/* import {
  SHOPIFY_DOMAIN,
  SHOPIFY_ACCESS_TOKEN,
  SHOPIFY_MULTIPASS,
} from "@constants/index";
import multipassify from "multipassify"; */
import jwt from "jsonwebtoken";
import cookie from "cookie";

/* const createMultipassToken = (email) => {
  const user = {
    email,
    return_to: "https://inventa.shop/" || `https://${SHOPIFY_DOMAIN}/checkout`,
  };
  const multipass = new multipassify(SHOPIFY_MULTIPASS);
  const token = multipass.encode(user);
  return {
    token,
    url: `https://${SHOPIFY_DOMAIN}/account/login/multipass/${token}`,
  };
}; */

export default async function handler(req, res) {
  if (req.method === "POST") {
    const { url_pathname = "/", token, ip, email, error, ...rest } = req.body;
    const final_cookies = { ...rest };
    console.log("Request info: " + JSON.stringify(req.body));

    if (token) {
      try {
        if (error) throw error;
        const decoded = jwt.verify(token, process.env.JWT_SECRET_SIGN);
        console.log(email + " ip are equals: " + decoded.ip == ip);
        if (decoded && decoded.email == email) {
          final_cookies.user = { ...decoded };
        } else {
          throw "INVALID_TOKEN";
        }
      } catch (error) {
        console.log("Error validating user: " + error);
        final_cookies.user = false;
      }
    } else {
      console.log("NO_TOKEN");
      final_cookies.user = false;
    }

    console.log("User cookie: " + JSON.stringify(final_cookies));
    const c = cookie.serialize("user-inventa", JSON.stringify(final_cookies), {
      httpOnly: true,
      secure: process.env.NODE_ENV === "production",
      maxAge: 60 * 60 * 24 * 7, // 1 week
      sameSite: "strict",
      path: "/",
    });
    res.setHeader("Set-Cookie", c);
    return res.redirect(
      302,
      process.env.LEGACY_SITE +
        "/apps/user-session/session?callback=" +
        url_pathname
    );
  } else if (req.method === "GET") {
    try {
      res.redirect(302, process.env.LEGACY_SITE + "/apps/user-session/app");
    } catch (error) {
      console.log(error);
      res.status(400).json({});
    }
  } else {
    res.status(400).json({});
  }
}
