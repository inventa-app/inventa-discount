import cookie from "cookie";

export default async function handler(req, res) {
  const c = cookie.serialize("user-inventa", "", {
    maxAge: -1,
    path: "/",
  });
  res.setHeader("Set-Cookie", c);
  return res.redirect(302, process.env.LEGACY_SITE + "/account/logout");
}
