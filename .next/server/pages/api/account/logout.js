"use strict";
(() => {
var exports = {};
exports.id = 606;
exports.ids = [606];
exports.modules = {

/***/ 910:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ handler)
/* harmony export */ });
/* harmony import */ var cookie__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8883);
/* harmony import */ var cookie__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(cookie__WEBPACK_IMPORTED_MODULE_0__);

async function handler(req, res) {
  const c = cookie__WEBPACK_IMPORTED_MODULE_0___default().serialize("user-inventa", "", {
    maxAge: -1,
    path: "/"
  });
  res.setHeader("Set-Cookie", c);
  return res.redirect(302, process.env.LEGACY_SITE + "/account/logout");
}

/***/ }),

/***/ 8883:
/***/ ((module) => {

module.exports = require("cookie");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(910));
module.exports = __webpack_exports__;

})();