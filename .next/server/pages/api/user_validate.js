"use strict";
(() => {
var exports = {};
exports.id = 821;
exports.ids = [821];
exports.modules = {

/***/ 2285:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ handler)
});

;// CONCATENATED MODULE: external "jsonwebtoken"
const external_jsonwebtoken_namespaceObject = require("jsonwebtoken");
var external_jsonwebtoken_default = /*#__PURE__*/__webpack_require__.n(external_jsonwebtoken_namespaceObject);
// EXTERNAL MODULE: external "cookie"
var external_cookie_ = __webpack_require__(8883);
var external_cookie_default = /*#__PURE__*/__webpack_require__.n(external_cookie_);
;// CONCATENATED MODULE: ./pages/api/user_validate.js
const _excluded = ["url_pathname", "token", "ip", "email", "error"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/* import {
  SHOPIFY_DOMAIN,
  SHOPIFY_ACCESS_TOKEN,
  SHOPIFY_MULTIPASS,
} from "@constants/index";
import multipassify from "multipassify"; */


/* const createMultipassToken = (email) => {
  const user = {
    email,
    return_to: "https://inventa.shop/" || `https://${SHOPIFY_DOMAIN}/checkout`,
  };
  const multipass = new multipassify(SHOPIFY_MULTIPASS);
  const token = multipass.encode(user);
  return {
    token,
    url: `https://${SHOPIFY_DOMAIN}/account/login/multipass/${token}`,
  };
}; */

async function handler(req, res) {
  if (req.method === "POST") {
    const _req$body = req.body,
          {
      url_pathname = "/",
      token,
      ip,
      email,
      error
    } = _req$body,
          rest = _objectWithoutProperties(_req$body, _excluded);

    const final_cookies = _objectSpread({}, rest);

    console.log("Request info: " + JSON.stringify(req.body));

    if (token) {
      try {
        if (error) throw error;
        const decoded = external_jsonwebtoken_default().verify(token, process.env.JWT_SECRET_SIGN);
        console.log(email + " ip are equals: " + decoded.ip == ip);

        if (decoded && decoded.email == email) {
          final_cookies.user = _objectSpread({}, decoded);
        } else {
          throw "INVALID_TOKEN";
        }
      } catch (error) {
        console.log("Error validating user: " + error);
        final_cookies.user = false;
      }
    } else {
      console.log("NO_TOKEN");
      final_cookies.user = false;
    }

    console.log("User cookie: " + JSON.stringify(final_cookies));
    const c = external_cookie_default().serialize("user-inventa", JSON.stringify(final_cookies), {
      httpOnly: true,
      secure: true,
      maxAge: 60 * 60 * 24 * 7,
      // 1 week
      sameSite: "strict",
      path: "/"
    });
    res.setHeader("Set-Cookie", c);
    return res.redirect(302, process.env.LEGACY_SITE + "/apps/user-session/session?callback=" + url_pathname);
  } else if (req.method === "GET") {
    try {
      res.redirect(302, process.env.LEGACY_SITE + "/apps/user-session/app");
    } catch (error) {
      console.log(error);
      res.status(400).json({});
    }
  } else {
    res.status(400).json({});
  }
}

/***/ }),

/***/ 8883:
/***/ ((module) => {

module.exports = require("cookie");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(2285));
module.exports = __webpack_exports__;

})();