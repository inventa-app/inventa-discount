"use strict";
(() => {
var exports = {};
exports.id = 888;
exports.ids = [888];
exports.modules = {

/***/ 6761:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {



var _react = __webpack_require__(9297);

var _Spinner = _interopRequireDefault(__webpack_require__(7348));

var _utils = __webpack_require__(3393);

var _UserContext = __webpack_require__(6404);

var _autoprefixer = __webpack_require__(8248);

var _shopifyRequests = __webpack_require__(6867);

var _jsxRuntime = __webpack_require__(5282);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const Login = ({
  changeForm
}) => {
  const {
    userLogin,
    sendLoginCode
  } = (0, _react.useContext)(_UserContext.UserContext);
  const {
    0: loading,
    1: setLoading
  } = (0, _react.useState)(false);
  const {
    0: loginCreds,
    1: setLoginCreds
  } = (0, _react.useState)({
    user: "",
    password: "",
    codeUser: "",
    error: "",
    code: ""
  });
  const {
    0: codeForm,
    1: setCodeForm
  } = (0, _react.useState)(false);
  const loginButton = {};
  const codeButton = {};
  const sentCodeButton = {};
  (0, _react.useEffect)(() => {
    if (loginCreds.error && loginCreds.error.form === "code") setCodeForm(false);
  }, [loginCreds]);
  if (!(0, _utils.validEmail)(loginCreds.user) || loginCreds.password.length < 1) loginButton.disabled = true;
  if (!(0, _utils.validEmail)(loginCreds.codeUser) && !(0, _utils.validPhone)(loginCreds.codeUser)) codeButton.disabled = true;
  if (loginCreds.code.length !== 4) sentCodeButton.disabled = true;

  const handleChange = (value, name) => setLoginCreds(_objectSpread(_objectSpread({}, loginCreds), {}, {
    error: "",
    [name]: value
  }));

  const handleLogin = e => {
    e.preventDefault();

    if (!loading && (0, _utils.validEmail)(loginCreds.user) && loginCreds.password.length > 0) {
      setLoading("login");
      userLogin(loginCreds.user, loginCreds.password).then(({
        ok
      }) => {
        if (!ok) {
          handleChange({
            form: "login",
            message: "E-mail ou senha incorretos."
          }, "error");
          setLoading(false);
        }
      });
    }
  };

  const loginChallenge = ({
    ok,
    triggerFunction,
    err,
    attempts
  }) => {
    if (attempts === 0) handleChange("", "code");
    setLoading(false);

    if (ok) {
      if (attempts > 0) handleChange({
        form: "sent-code",
        message: "Código inválido."
      }, "error");
      if (typeof triggerFunction === "function") setCodeForm({
        ok: true,
        try_f: triggerFunction
      });
    } else {
      handleChange({
        form: "code",
        message: "Código incorreto. Solicite um novo código."
      }, "error");
    }
  };

  const handleStartLoginCode = e => {
    e.preventDefault();

    if ((0, _utils.validEmail)(loginCreds.codeUser) || (0, _utils.validPhone)(loginCreds.codeUser)) {
      setLoading("code");
      sendLoginCode(loginCreds.codeUser, loginChallenge);
    }
  };

  const handleTryCode = e => {
    e.preventDefault();

    if (loginCreds.code && loginCreds.code.length === 4) {
      setLoading("sent-code");
      codeForm.try_f(loginCreds.code);
    }
  };

  if (codeForm) return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: "py-6 px-12 w-full",
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("h4", {
      className: "text-2xl font-bold font-poppins mb-4",
      children: "C\xF3digo de verifica\xE7\xE3o"
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("form", {
      onSubmit: handleTryCode,
      action: "/",
      children: [loginCreds.error && loginCreds.error.form === "sent-code" && /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
        className: "text-formErrorColor bg-formErrorColor/[0.07] mb-3 px-5 py-3 font-systemUi",
        children: loginCreds.error.message
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("input", {
        className: "w-full text-center text-4xl font-systemUi py-3 tracking-[20px] border-slate-200 border",
        onChange: e => handleChange(e.target.value, "code"),
        placeholder: "0000",
        value: loginCreds.code,
        maxLength: "4",
        name: "customer[code]",
        type: "text",
        onKeyDown: e => {
          if (isNaN(e.key) && e.key != "Backspace" && e.key != "Enter" && e.key != "Delete") e.preventDefault();
        }
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("button", _objectSpread(_objectSpread({}, sentCodeButton), {}, {
        type: "submit",
        className: "text-center transition duration-700 disabled:text-black/30 mt-3 text-lg font-bold font-poppins py-6 rounded w-full bg-inventaYellow",
        children: loading === "sent-code" ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_Spinner.default, {
          background: "black",
          fill: "gray"
        }) : "Validar código"
      }))]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("p", {
      className: "text-center my-5 text-xl",
      children: ["ou", " ", /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        className: "font-bold cursor-pointer",
        onClick: () => setCodeForm(false),
        children: "Utilize senha para entrar"
      })]
    })]
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: "py-6 px-12 w-full",
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("h4", {
      className: "text-2xl font-bold font-poppins mb-4",
      children: "Entrar como Lojista"
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("form", {
      action: process.env.LEGACY_SITE + "/account/login",
      method: "POST",
      onSubmit: handleLogin,
      children: [loginCreds.error && loginCreds.error.form === "login" && /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
        className: "text-formErrorColor bg-formErrorColor/[0.07] mb-3 px-5 py-3 font-systemUi",
        children: loginCreds.error.message
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: "relative w-full mb-3",
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("input", {
          name: "customer[email]",
          type: "email",
          value: loginCreds.user,
          required: "required",
          autoFocus: true,
          className: "p-4 w-full border-slate-200 border",
          onChange: e => handleChange(e.target.value, "user")
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("label", {
          className: "absolute text-xs top-0.5 left-3",
          htmlFor: "customer[email]",
          children: "E-mail"
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: "relative w-full",
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("input", {
          name: "customer[password]",
          required: "required",
          autoComplete: "current-password",
          type: "password",
          className: "p-4 w-full border-slate-200 border",
          onChange: e => handleChange(e.target.value, "password")
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("label", {
          className: "absolute text-xs top-0.5 left-3",
          htmlFor: "customer[password]",
          children: "Senha"
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("small", {
        className: "underline hover:text-primaryYellow cursor-pointer",
        onClick: () => changeForm("ForgotPassword"),
        children: "Esqueci a senha"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("button", _objectSpread(_objectSpread({
        type: "submit",
        className: "text-center transition duration-700 disabled:text-black/30 mt-3 text-lg font-bold font-poppins py-4 rounded w-full bg-inventaYellow"
      }, loginButton), {}, {
        children: loading == "login" ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_Spinner.default, {
          background: "black",
          fill: "gray"
        }) : "Entrar com senha"
      }))]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
      className: "my-4 text-center text-lg",
      children: "ou"
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("form", {
      onSubmit: handleStartLoginCode,
      action: "/",
      children: [loginCreds.error && loginCreds.error.form === "code" && /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
        className: "text-formErrorColor bg-formErrorColor/[0.07] mb-3 px-5 py-3 font-systemUi",
        children: loginCreds.error.message
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: "relative w-full",
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("input", {
          value: loginCreds.codeUser,
          name: "customer[code-email]",
          required: "required",
          className: "p-4 w-full border-slate-200 border mb-3",
          onChange: e => handleChange(e.target.value, "codeUser")
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("label", {
          className: "absolute text-xs top-0.5 left-3",
          htmlFor: "customer[code-email]",
          children: "Telefone (com DDD) ou Email"
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("button", _objectSpread(_objectSpread({
        className: "text-center transition duration-700 disabled:text-white/30 text-white text-lg font-bold font-poppins py-4 rounded w-full bg-black"
      }, codeButton), {}, {
        children: loading == "code" ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_Spinner.default, {
          background: "black",
          fill: "gray"
        }) : "Entrar com código"
      }))]
    })]
  });
};

const ForgotPassword = ({
  changeForm
}) => {
  const {
    0: email,
    1: setEmail
  } = (0, _react.useState)();
  const {
    0: loading,
    1: setLoading
  } = (0, _react.useState)(false);
  const {
    0: message,
    1: setMessage
  } = (0, _react.useState)();
  const resetButton = {};
  if (!(0, _utils.validEmail)(email)) resetButton.disabled = true;

  const handleSubmit = e => {
    e.preventDefault();

    if ((0, _utils.validEmail)(email) && !loading) {
      setMessage(false);
      setLoading(true);

      try {
        (0, _shopifyRequests.ResetPassword)(email).then(res => {
          setMessage({
            message: "Enviamos um e-mail com instruções para redefinir a senha.",
            color: "formSuccessColor"
          });
          setLoading(false);
        }).catch(err => {
          setMessage({
            message: "Algo deu errado, tente mais tarde por favor",
            color: "formErrorColor"
          });
          setLoading(false);
        });
      } catch (error) {
        setMessage({
          message: "Algo deu errado, tente mais tarde por favor",
          color: "formErrorColor"
        });
        setLoading(false);
      }

      setEmail("");
    }
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: "py-6 px-12 w-full",
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("h4", {
      className: "text-2xl font-bold font-poppins mb-4",
      children: "Esqueci a senha"
    }), message && /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
      className: `bg-${message.color}/[0.07] text-${message.color} mb-3 px-5 py-3 font-systemUi`,
      children: message.message
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("form", {
      action: process.env.LEGACY_SITE + "/account/recover",
      method: "POST",
      onSubmit: handleSubmit,
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("input", {
        type: "hidden",
        name: "form_type",
        value: "recover_customer_password"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("input", {
        type: "hidden",
        name: "utf8",
        value: "\u2713"
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: "relative w-full",
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("input", {
          value: email,
          name: "email",
          type: "email",
          required: "required",
          className: "p-4 w-full border-slate-200 border mb-3",
          onChange: e => setEmail(e.target.value)
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("label", {
          className: "absolute text-xs top-0.5 left-3",
          htmlFor: "email",
          children: "E-mail"
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("button", _objectSpread(_objectSpread({
        type: "submit",
        className: "text-center transition duration-700 disabled:text-black/30 mt-3 text-lg font-bold font-poppins py-4 rounded w-full bg-inventaYellow"
      }, resetButton), {}, {
        children: loading ? /*#__PURE__*/(0, _jsxRuntime.jsx)(_Spinner.default, {
          background: "black",
          fill: "gray"
        }) : "Recuperar"
      }))]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("p", {
      className: "mt-5",
      children: ["Lembrou a sua senha?", " ", /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        className: "underline cursor-pointer",
        onClick: () => changeForm("Login"),
        children: "Voltar para o login"
      })]
    })]
  });
};

module.exports = {
  Login,
  ForgotPassword
};

/***/ }),

/***/ 7348:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);



const Spinner = ({
  background = "gray",
  fill = "blue"
}) => {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
    className: "flex justify-center",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("svg", {
      role: "status",
      className: `h-8 text-${background}-200 animate-spin dark:text-${background}-600 fill-${fill}-600`,
      viewBox: "0 0 100 101",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("path", {
        d: "M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z",
        fill: background
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("path", {
        d: "M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z",
        fill: fill
      })]
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Spinner);

/***/ }),

/***/ 2331:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _app)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/MetaTags/index.js




const MetaTags = ({
  pageData = {}
}) => /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
  children: [/*#__PURE__*/jsx_runtime_.jsx("meta", {
    name: "theme-color",
    content: "#ddaa2e"
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    name: "description",
    content: pageData.description
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    property: "product:price:currency",
    content: "BRL"
  }), /*#__PURE__*/jsx_runtime_.jsx("link", {
    rel: "shortcut icon",
    type: "image/png",
    href: "/favicon_96x96.png"
  }), /*#__PURE__*/jsx_runtime_.jsx("link", {
    rel: "icon",
    href: "/favicon.ico"
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    property: "og:type",
    content: pageData.type
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    property: "og:title",
    content: pageData.title
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    property: "og:image",
    content: pageData.image
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    property: "og:image:secure_url",
    content: pageData.image
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    property: "og:description",
    content: pageData.description
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    property: "og:url",
    content: pageData.url
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    property: "og:site_name",
    content: "Inventa"
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    name: "twitter:card",
    content: "summary"
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    name: "twitter:title",
    content: pageData.title
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    name: "twitter:description",
    content: pageData.description
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    name: "twitter:image",
    content: pageData.image
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    charSet: "UTF-8"
  }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
    name: "viewport",
    content: "width=device-width, initial-scale=1.0"
  })]
});

/* harmony default export */ const components_MetaTags = (MetaTags);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./services/contexts/UserContext.js
var UserContext = __webpack_require__(6404);
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(5675);
// EXTERNAL MODULE: ./services/utils.js
var utils = __webpack_require__(3393);
;// CONCATENATED MODULE: ./components/WhatsAppButton/index.js






const WhatsAppButton = () => {
  const {
    0: expanded,
    1: setExpanded
  } = (0,external_react_.useState)(false);
  (0,external_react_.useEffect)(() => {
    (0,utils.listenEvent)("whatsapp:open", detail => {
      if (!expanded) setExpanded(detail);
    });
    (0,utils.listenEvent)("whatsapp:close", () => {
      if (expanded) setExpanded(false);
    });
  }, []);

  const handleCloseExpanded = () => {
    setExpanded(false);
  };

  const link_default = "https://wa.me/5511912709419?text=Olá, pode ajudar-me?";
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: "flex flex-col fixed z-10 bottom-16 right-4",
    children: [expanded && /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex mb-2",
      children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
        className: "material-icons absolute left-0 top-0 z-10 cursor-pointer",
        onClick: handleCloseExpanded,
        children: "close"
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "relative py-2 px-3",
        children: [/*#__PURE__*/jsx_runtime_.jsx("a", {
          className: "flex items-center justify-center rounded-full border-4 border-white",
          href: expanded.wpp_link,
          children: /*#__PURE__*/jsx_runtime_.jsx(next_image.default, {
            src: expanded.agent_icon,
            alt: "Laura",
            width: 64,
            height: 64
          })
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "absolute bottom-0 right-0 w-0 h-0 border-8 border-b-white border-r-white border-t-transparent border-l-transparent"
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
        className: "flex flex-col justify-center px-2 bg-white rounded-xl rounded-bl-none shadow-sm font-lato font-bold text-base text-secondaryDark",
        href: expanded.wpp_link,
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("p", {
          children: ["Ol\xE1, sou ", expanded.agent_name]
        }), /*#__PURE__*/jsx_runtime_.jsx("p", {
          children: expanded.agent_message
        })]
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
      className: "relative self-end max-w-fit py-4 px-5 bg-whatsApp flex items-center justify-start rounded-full rounded-br-none text-white",
      href: link_default,
      target: "_blank",
      children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
        className: "absolute p-2 bg-notification rounded-full top-0 left-0"
      }), /*#__PURE__*/jsx_runtime_.jsx("span", {
        className: "material-icons text-4xl",
        children: "whatsapp"
      }), expanded && /*#__PURE__*/jsx_runtime_.jsx("p", {
        className: "px-2 whitespace-nowrap text-lg",
        children: "Continue no WhatsApp"
      })]
    })]
  });
};

/* harmony default export */ const components_WhatsAppButton = (WhatsAppButton);
;// CONCATENATED MODULE: ./components/CollapseItem/index.js





function CollapseItem({
  children,
  title,
  type,
  titleLink
}) {
  const {
    clientWidth
  } = (0,external_react_.useContext)(UserContext.UserContext);
  const {
    0: show,
    1: setShow
  } = (0,external_react_.useState)(false);
  (0,external_react_.useEffect)(() => {
    if (clientWidth > 1024 && type !== "faq") {
      setShow(true);
    } else {
      setShow(false);
    }

    if (type == "description") {
      setShow(true);
    }
  }, [clientWidth]);

  const toggle = () => {
    if (type === "footer" && clientWidth > 1024) {
      return;
    }

    setShow(prev => !prev);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    children: [type !== "notification" && /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: `flex items-center justify-between ${type === "description" ? "py-2 px-4 border rounded my-4" : ""} ${type !== "footer" ? "cursor-pointer" : ""}`,
      onClick: toggle,
      children: [/*#__PURE__*/jsx_runtime_.jsx("p", {
        className: `font-bold font-roboto text-black my-3 ${type === "faq" ? "my-2 font-noto text-secondaryDark text-xl leading-10" : ""} ${type === "filters" ? "font-lato font-black text-2xl text-secondaryDark" : ""}
          ${type === "footer" ? "lg:text-base 2xl:text-lg" : ""}
          `,
        children: type === "footer" && titleLink && clientWidth > 1024 ? /*#__PURE__*/jsx_runtime_.jsx("a", {
          href: titleLink,
          target: "_blank",
          children: title
        }) : /*#__PURE__*/jsx_runtime_.jsx("span", {
          children: title
        })
      }), type === "description" && /*#__PURE__*/jsx_runtime_.jsx("span", {
        className: "material-icons-outlined",
        children: "expand_more"
      }), type === "faq" && !show && /*#__PURE__*/jsx_runtime_.jsx("span", {
        className: "px-3 py-2 material-icons text-2xl bg-basicMedium rounded-full",
        children: "add"
      }), type === "faq" && show && /*#__PURE__*/jsx_runtime_.jsx("span", {
        className: "px-3 py-2 material-icons text-2xl text-white bg-secondaryDark rounded-full",
        children: "close"
      }), type === "filters" && /*#__PURE__*/jsx_runtime_.jsx("span", {
        className: "material-icons-outlined ml-10",
        children: "expand_more"
      })]
    }), type === "notification" && /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex items-center justify-center",
      onClick: toggle,
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: `${show ? "" : "truncate"}`,
        children: children
      }), clientWidth <= 480 ? /*#__PURE__*/jsx_runtime_.jsx("span", {
        className: "material-icons-outlined",
        children: show ? "expand_less" : "expand_more"
      }) : null]
    }), show && type !== "notification" && /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: `leading-loose font-lato`,
      children: children
    })]
  });
}

/* harmony default export */ const components_CollapseItem = (CollapseItem);
;// CONCATENATED MODULE: ./components/layouts/InventaDefault/innerComponents/Footer/index.js




const Footer = () => {
  return /*#__PURE__*/jsx_runtime_.jsx("footer", {
    className: "bg-basicLight",
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex items-center justify-center flex-col p-10 lg:py-16 lg:px-32 xl:px-40",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex justify-between flex-col lg:flex-row lg:flex-wrap w-full",
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "leading-loose mr-10",
          children: [/*#__PURE__*/jsx_runtime_.jsx("a", {
            href: "https://inventa.shop/",
            className: "text-black font-extralight lg:text-5xl text-3xl tracking-widest",
            children: "INVENTA"
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("ul", {
            children: [/*#__PURE__*/jsx_runtime_.jsx("li", {
              className: "text-grayText text-sm",
              children: "@ 2022"
            }), /*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://inventa.shop/pages/politica-de-privacidade",
                className: "hover:text-primaryYellow text-grayText text-sm",
                children: "Pol\xEDtica de privacidades"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://inventa.shop/pages/termos-e-condicoes-lojistas",
                className: "hover:text-primaryYellow text-grayText text-sm",
                children: "Termos e condi\xE7\xF5es"
              })
            })]
          })]
        }), /*#__PURE__*/jsx_runtime_.jsx(components_CollapseItem, {
          title: "Produtos",
          type: "footer",
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("ul", {
            children: [/*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://inventa.shop/pages/fornecedor",
                className: "hover:text-primaryYellow text-grayText text-sm",
                children: "Fornecedor"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://inventa.shop/#beneficios-inventa",
                className: "hover:text-primaryYellow text-grayText text-sm",
                children: "Varejista"
              })
            })]
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(components_CollapseItem, {
          title: "Inventa",
          type: "footer",
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("ul", {
            children: [/*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://inventa.shop/pages/sobre-inventa",
                className: "hover:text-primaryYellow text-grayText text-sm",
                children: "Sobre"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://inventa.shop/",
                className: "hover:text-primaryYellow text-grayText text-sm",
                children: "Miss\xE3o"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://inventa.shop/",
                className: "hover:text-primaryYellow text-grayText text-sm",
                children: "Oportunidades"
              })
            })]
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(components_CollapseItem, {
          title: "Suporte",
          type: "footer",
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("ul", {
            children: [/*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://inventa.shop/",
                className: "hover:text-primaryYellow text-grayText text-sm",
                children: "FAQ"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://inventa.shop/pages/fornecedor#faq",
                className: "hover:text-primaryYellow text-grayText text-sm",
                children: "Ajuda Fornecedor"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://inventa.shop/#faq",
                className: "hover:text-primaryYellow text-grayText text-sm",
                children: "Ajuda Varejista"
              })
            })]
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(components_CollapseItem, {
          title: "Acompanhe",
          type: "footer",
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("ul", {
            children: [/*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://www.linkedin.com/company/inventashop/",
                className: "hover:text-primaryYellow text-grayText text-sm",
                children: "LinkedIn"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://www.instagram.com/inventa.shop",
                className: "hover:text-primaryYellow text-grayText text-sm",
                children: "Instagram"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://www.facebook.com/inventa.shop",
                className: "hover:text-primaryYellow text-grayText text-sm",
                children: "Facebook"
              })
            })]
          })
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "flex mt-10 w-full",
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("p", {
          className: "text-xs text-grayText",
          children: ["Inventa App Ltda - CNPJ: 41.356.168/0001-11 | Inscri\xE7\xE3o Estadual: 131.409.133.112 | R Jose Maria Lisboa, 880, 01423-912 |", " ", /*#__PURE__*/jsx_runtime_.jsx("a", {
            href: "mailto:contato@inventa.shop",
            className: "hover:text-primaryYellow",
            children: "contato@inventa.shop"
          })]
        })
      })]
    })
  });
};

/* harmony default export */ const innerComponents_Footer = (Footer);
;// CONCATENATED MODULE: ./constants/index.js
const SHOPIFY_DOMAIN = process.env.SHOPIFY_DOMAIN;
const SHOPIFY_ACCESS_TOKEN = process.env.SHOPIFY_ACCESS_TOKEN;
const SHOPIFY_MULTIPASS = process.env.SHOPIFY_MULTIPASS;
const JWT_SECRET_SIGN = process.env.JWT_SECRET_SIGN;
const constants_LEGACY_SITE = process.env.LEGACY_SITE;
const APPSYNC_URL = process.env.APPSYNC_URL;
const APPSYNC_API_KEY = process.env.APPSYNC_API_KEY;
;// CONCATENATED MODULE: ./components/Aside/index.js



const Aside_Aside = ({
  children,
  display,
  position,
  timing,
  duration,
  className
}) => {
  const positionStyles = {
    right: "right-0",
    left: "left-0",
    top: "top-0 left-0"
  };
  const sizeStyles = {
    right: {
      true: "w-screen",
      false: "w-0 px-0",
      default: "h-screen"
    },
    left: {
      true: "w-screen",
      false: "w-0 px-0",
      default: "h-screen"
    },
    top: {
      true: "h-screen",
      false: "h-0 py-0",
      default: "w-screen"
    }
  };

  if (!positionStyles[position]) {
    position = "right";
  }

  (0,external_react_.useEffect)(() => {
    document.body.style.overflowY = `${display === false ? "scroll" : "hidden"}`; //document.body.style.overflowX = `${display === false ? "scroll" : "hidden"}`;

    return () => {
      document.body.style.overflowY = "auto";
    };
  }, [display]);
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    children: /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: `bg-white lg:w-0 lg:h:0 overflow-hidden absolute inset-y-0 p-7 z-20
            ${sizeStyles[position || "right"].default || ""}
            ${sizeStyles[position || "right"][display] || ""} 
            ${positionStyles[position] || ""}
            ${timing || "ease"}
            ${duration || "duration-500"}
            ${className || ""}
            transition-all`,
      children: children
    })
  });
};

/* harmony default export */ const components_Aside = (Aside_Aside);
;// CONCATENATED MODULE: ./components/layouts/InventaDefault/innerComponents/Header/SearchbarMobile/index.js








const Searchbar = () => {
  const {
    0: showSearch,
    1: setShowSearch
  } = useState(false);
  let searchHistory = ["Glory by nature", "Chocolate", "Em promoção", "Chocolate", "Em promoção"];
  let recentHistory = [{
    name: "Chocolate brancho recheado 150g",
    price: "50,00",
    brand: "We Nutz",
    image: "https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg"
  }, {
    name: "Chocolate brancho recheado 150g",
    price: "50,00",
    brand: "We Nutz",
    image: "https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg"
  }, {
    name: "Chocolate brancho recheado 150g",
    price: "50,00",
    brand: "We Nutz",
    image: "https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg"
  }, {
    name: "Chocolate brancho recheado 150g",
    price: "50,00",
    brand: "We Nutz",
    image: "https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg"
  }];
  let categories = [{
    name: "Mercearia",
    image: "https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg"
  }, {
    name: "Cervejas",
    image: "https://images.tcdn.com.br/files/859628/themes/13/img/settings/we.jpg"
  }, {
    name: "Chocolates",
    image: "https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg"
  }];

  const displaySearch = () => {
    setShowSearch(!showSearch);
  };

  return /*#__PURE__*/_jsxs(_Fragment, {
    children: [/*#__PURE__*/_jsx("span", {
      className: "material-icons-outlined flex lg:hidden",
      onClick: displaySearch,
      children: "search"
    }), /*#__PURE__*/_jsxs(Aside, {
      display: showSearch,
      position: "right",
      timing: "ease-in-out",
      duration: "duration-500",
      children: [/*#__PURE__*/_jsxs("div", {
        className: "flex",
        children: [/*#__PURE__*/_jsxs("form", {
          action: LEGACY_SITE + "/search",
          method: "GET",
          role: "search",
          className: `w-full flex justify-end`,
          children: [/*#__PURE__*/_jsx("span", {
            className: `material-icons-outlined text-basicDark lg:hidden -mr-6 z-10 items-center text-base flex`,
            children: "search"
          }), /*#__PURE__*/_jsx("input", {
            type: "search",
            name: "search",
            placeholder: "Procurar por marca, produto ou categoria",
            className: `border-2 border-basicMedium rounded-lg pl-8 pr-2 text-sm focus:outline-none text-black h-full placeholder:font-noto placeholder:text-secondaryLight placeholder:text-xs py-1.5 w-full`
          })]
        }), /*#__PURE__*/_jsx("button", {
          onClick: displaySearch,
          className: "text-black text-xs font-noto ml-8",
          children: "Cancelar"
        })]
      }), /*#__PURE__*/_jsx("p", {
        className: "text-noto font-light text-lg mt-4 mb-2",
        children: "Buscado recentemente"
      }), /*#__PURE__*/_jsx("div", {
        className: "flex flex-wrap",
        children: searchHistory.map((item, index) => /*#__PURE__*/_jsxs("p", {
          className: "text-xs font-lato mr-4 my-1",
          children: [/*#__PURE__*/_jsx("span", {
            className: "text-secondaryLight text-xs",
            children: "x"
          }), item]
        }, `${index}-${item}`))
      }), /*#__PURE__*/_jsx("p", {
        className: "text-noto font-light text-lg mt-4 mb-2",
        children: "Mais buscados"
      }), /*#__PURE__*/_jsx("div", {
        className: "flex flex-wrap",
        children: categories.map((item, index) => /*#__PURE__*/_jsxs("div", {
          className: "flex rounded-3xl bg-basicMedium p-2 m-1",
          children: [/*#__PURE__*/_jsx(Image, {
            src: item.image,
            alt: item.title,
            width: 18,
            height: 18,
            className: "rounded-full"
          }), /*#__PURE__*/_jsx("p", {
            className: "text-xs font-noto mx-2 text-inventaGrayText",
            children: item.name
          })]
        }, `${index}-${item.name}`))
      }), /*#__PURE__*/_jsx("p", {
        className: "text-noto font-light text-lg mt-4 mb-2",
        children: "Visto recentemente"
      }), /*#__PURE__*/_jsx("div", {
        className: "flex flex-col overflow-auto h-2/6",
        children: recentHistory.map((item, index) => /*#__PURE__*/_jsxs("div", {
          className: "flex p-1 m-1",
          children: [/*#__PURE__*/_jsx(Image, {
            src: item.image,
            alt: item.title,
            width: 56,
            height: 56,
            className: "border rounded-md border-inventaGrayText"
          }), /*#__PURE__*/_jsxs("div", {
            children: [/*#__PURE__*/_jsx("p", {
              className: "px-2 text-sm font-lato font-bold",
              children: item.name
            }), /*#__PURE__*/_jsx("p", {
              className: "px-2 text-xs font-lato",
              children: `R\$ ${item.price} ${item.brand}`
            })]
          })]
        }, `${index}-${item.name}`))
      }), /*#__PURE__*/_jsx("div", {
        className: "w-full bg-basicMedium h-0.5 block my-4 mx-auto"
      }), /*#__PURE__*/_jsx("p", {
        className: "text-sm font-lato font-bold my-3 text-center underline hover:cursor-pointer",
        children: "Ganhe 20% off na primera compra"
      })]
    })]
  });
};

/* harmony default export */ const SearchbarMobile = ((/* unused pure expression or super */ null && (Searchbar)));
;// CONCATENATED MODULE: ./components/CreditPopUp/data.js
const FirstIcon = "/general_images/CreditPopUp/firstIcon.svg";
const SecondIcon = "/general_images/CreditPopUp/secondIcon.svg";
const ThirdIcon = "/general_images/CreditPopUp/thirdIcon.png";
const FourthIcon = "/general_images/CreditPopUp/fourthIcon.svg";
const data = [{
  icon: FirstIcon,
  title: "À vista",
  description: "Via boleto ou cartão de crédito, você paga logo após finalizá-la. Após a confirmação do pagamento, o nosso time de logística vai correr para entregar tudo rapidinho."
}, {
  icon: SecondIcon,
  title: "Parcelado no cartão de crédito",
  description: "Você também pode parcelar suas compras em até 12x no cartão de crédito, sem juros!"
}, {
  icon: ThirdIcon,
  title: "Recebíveis da maquininha de cartão",
  description: "TruePay é um meio de pagamento que libera o seu saldo das suas maquininhas, sem nenhuma taxa. Isso libera mais capital para comprar e prazo para pagar."
}, {
  icon: FourthIcon,
  title: "Parcelado no boleto",
  description: "Você poderá parcelar todo o valor aprovado para o seu perfil em até 3x no prazo de até 90 dias. Geramos e enviamos os boletos por e-mail assim que o envio for confirmado pelos fabricantes.",
  description2: "Assim, você recebe e vende os produtos para seus clientes e só depois precisa pagar a Inventa. Legal, né? E melhor ainda: pagando as contas em dia, você ganha mais limite e prazo para levar mais produtos para sua clientela!"
}];
/* harmony default export */ const CreditPopUp_data = (data);
;// CONCATENATED MODULE: ./components/CreditPopUp/index.js







const CreditPopUp = ({
  toggle
}) => {
  const {
    clientWidth
  } = (0,external_react_.useContext)(UserContext.UserContext);
  const breakpointMd = clientWidth >= 768 ? true : false;
  const ref = (0,external_react_.useRef)(null);

  const handleTogglePopUp = () => {
    toggle();
  };

  (0,external_react_.useEffect)(() => {
    const handleClickOutside = e => {
      if (ref.current && !ref.current.contains(e.target)) {
        toggle();
      }
    };

    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref, toggle]);
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: "flex flex-row-reverse items-start justify-center bg-black bg-opacity-70 w-screen h-screen fixed top-0 z-50 py-6 overflow-y-auto",
    children: [breakpointMd && /*#__PURE__*/jsx_runtime_.jsx("span", {
      className: "material-icons cursor-pointer text-4xl text-white",
      onClick: handleTogglePopUp,
      children: "close"
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "relative flex flex-col bg-white rounded-md pt-8 px-12 w-11/12 md:max-w-4xl h-fit max-h-screen overflow-y-auto text-secondaryDark",
      ref: ref,
      children: [!breakpointMd && /*#__PURE__*/jsx_runtime_.jsx("span", {
        className: "material-icons absolute cursor-pointer text-4xl top-2 right-2",
        onClick: handleTogglePopUp,
        children: "close"
      }), /*#__PURE__*/jsx_runtime_.jsx("p", {
        className: "font-bold text-2xl",
        children: "Como eu posso pagar minhas compras na Inventa?"
      }), /*#__PURE__*/jsx_runtime_.jsx("p", {
        className: "mt-2 mb-6",
        children: "Voc\xEA pode pagar seus pedidos de quatro maneiras:"
      }), CreditPopUp_data.map((item, index) => /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "md:ml-6 mb-6 flex flex-col md:flex-row items-start",
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "mr-4",
          children: /*#__PURE__*/jsx_runtime_.jsx(next_image.default, {
            src: item.icon,
            alt: "Image for " + item.title,
            width: 50,
            height: 50
          })
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "w-full md:w-11/12",
          children: [/*#__PURE__*/jsx_runtime_.jsx("p", {
            className: "font-bold",
            children: item.title
          }), /*#__PURE__*/jsx_runtime_.jsx("p", {
            children: item.description
          }), item.description2 && /*#__PURE__*/jsx_runtime_.jsx("p", {
            className: "mt-2",
            children: item.description2
          })]
        })]
      }, index)), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "my-6 w-full h-px bg-inventaBorderGray"
      })]
    })]
  });
};

/* harmony default export */ const components_CreditPopUp = (CreditPopUp);
;// CONCATENATED MODULE: ./components/Badge/index.js



const Badge = ({
  children,
  size,
  value = 0,
  className = ""
}) => {
  const sizeClasses = {
    sm: "px-1 py-0.5 text-xs",
    md: "px-1.5 py-1 text-sm",
    lg: "px-3 py-2 text-md"
  };
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: `relative flex align-middle ${className}`,
    children: [/*#__PURE__*/jsx_runtime_.jsx("p", {
      className: `bg-primaryYellow text-white rounded-xl absolute -right-1 -top-2 font-bold font-lato leading-none ${sizeClasses[size]}`,
      children: value
    }), children]
  });
};

/* harmony default export */ const components_Badge = (Badge);
;// CONCATENATED MODULE: ./components/FloatingMenu/index.js




const FloatingMenu = ({
  children,
  arrow,
  mark,
  hover,
  position,
  clickable
}) => {
  const {
    0: display,
    1: setDisplay
  } = (0,external_react_.useState)(false);
  const ref = (0,external_react_.useRef)(null);
  const arrowStyle = {
    right: "right-4",
    left: "left-4",
    center: "left-1/2 -translate-x-1/2"
  };
  const positionStyle = {
    right: "md:left-full md:-translate-y-10 -translaye-x-1",
    left: "md:left-0",
    bottom: "md:top-full md:-translate-y-1",
    top: ""
  };
  const markDirection = {
    up: "expand_less",
    right: "navigate_next",
    left: "navigate_before",
    down: "expand_more"
  };

  const handleClick = () => {
    setDisplay(!display);
  };

  const handleHoverEnter = () => {
    if (hover) {
      setDisplay(true);
    }
  };

  const handleHoverLeave = () => {
    if (hover) {
      setDisplay(false);
    }
  };

  (0,external_react_.useEffect)(() => {
    const handleClickOutside = event => {
      if (ref.current && !ref.current.contains(event.target)) {
        setDisplay(false);
      }
    };

    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("click", handleClickOutside, true);
    };
  }, []);
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: "z-10",
    ref: ref,
    onMouseEnter: handleHoverEnter,
    onMouseLeave: handleHoverLeave,
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex justify-between items-center hover:cursor-pointer relative",
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        onClick: handleClick,
        children: clickable
      }), mark && /*#__PURE__*/jsx_runtime_.jsx("span", {
        className: "material-icons-outlined text-base absolute right-0 top-1/2 -translate-y-1/2 text-basicDark",
        onClick: handleHoverEnter,
        children: markDirection[mark]
      })]
    }), display && /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: `flex flex-col border-t-inventaBordersColor border bg-white floating-menu absolute rounded text-black shadow-lg z-20 ${positionStyle[position]}`,
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "relative",
        children: arrow && display && /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: `absolute -top-4  h-0 w-0 border-8 border-transparent border-b-white
                                    ${arrowStyle[arrow] || "left-1"}
                                `
        })
      }), children]
    })]
  });
};

/* harmony default export */ const components_FloatingMenu = (FloatingMenu);
;// CONCATENATED MODULE: ./components/Container/index.js


const Container = ({
  children,
  tailwindClass
}) => {
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: `max-w-[1440px] mx-auto ${tailwindClass ? tailwindClass : ""}`,
    children: children
  });
};

/* harmony default export */ const components_Container = (Container);
// EXTERNAL MODULE: ./components/Redirect/index.js
var Redirect = __webpack_require__(6645);
;// CONCATENATED MODULE: ./components/HeaderMenu/index.js






const HeaderMenu = ({
  menuItems
}) => {
  const url =  false ? 0 : "";
  if (menuItems && menuItems.menu) return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "border-b-gray-300 border-b-[0.0625rem] bg-white",
    children: /*#__PURE__*/jsx_runtime_.jsx(components_Container, {
      children: /*#__PURE__*/jsx_runtime_.jsx("ul", {
        className: "flex md:justify-center w-full floating-menu_wrapper overflow-x-auto pr-3",
        children: menuItems.menu.map((item, index) => /*#__PURE__*/jsx_runtime_.jsx("li", {
          className: "whitespace-nowrap",
          children: item.submenu ? /*#__PURE__*/jsx_runtime_.jsx(components_FloatingMenu, {
            hover: true,
            mark: "down",
            position: "bottom",
            className: "mt-[2.532rem]",
            clickable: /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
              href: `${item.link}`,
              className: `font-lato font-bold w-full lg:text-base text-sm pl-3 pr-6 lg:pl-2 py-4 flex text-center hover:text-primaryYellow ${url.includes(item.link) ? "text-secondaryDark" : "text-basicDark"} `,
              children: item.label
            }),
            children: /*#__PURE__*/jsx_runtime_.jsx("ul", {
              children: item.submenu.map((subItem, index) => /*#__PURE__*/jsx_runtime_.jsx("li", {
                children: subItem.submenu ? /*#__PURE__*/jsx_runtime_.jsx(components_FloatingMenu, {
                  hover: true,
                  mark: "right",
                  position: "right",
                  clickable: /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
                    href: subItem.link,
                    className: "h-full w-full pl-4 pr-6 py-2 hover:text-primaryYellow hover:cursor-pointer font-lato font-bold text-basicDark text-sm flex",
                    children: subItem.label
                  }, `${index}-${subItem.label}`),
                  children: subItem.submenu.map((deepItem, index) => /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
                    href: deepItem.link,
                    className: "h-full w-full px-4 py-2 hover:text-primaryYellow hover:cursor-pointer font-lato font-bold text-basicDark text-sm flex",
                    children: deepItem.label
                  }, `${index}-${deepItem.label}`))
                }) : /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
                  href: subItem.link,
                  className: "h-full w-full px-4 py-2 hover:text-primaryYellow hover:cursor-pointer font-lato font-bold text-basicDark text-sm flex",
                  children: subItem.label
                }, `${index}-${subItem.label}`)
              }, `${index}-${subItem.label}`))
            })
          }) : /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
            href: `${item.link}`,
            className: `font-lato font-bold lg:text-base text-sm px-3 lg:px-4 py-4 block text-center hover:text-primaryYellow ${url.includes(item.link) ? "text-secondaryDark" : "text-basicDark"}`,
            children: item.label
          })
        }, `${index}-${item.label}`))
      })
    })
  });else return /*#__PURE__*/jsx_runtime_.jsx(jsx_runtime_.Fragment, {});
};

/* harmony default export */ const components_HeaderMenu = (HeaderMenu);
;// CONCATENATED MODULE: ./components/layouts/InventaDefault/innerComponents/Header/index.js
















const Header = ({
  userLogged,
  menuItems
}) => {
  const {
    0: show,
    1: setShow
  } = (0,external_react_.useState)(false);
  const {
    clientWidth,
    cartCount
  } = (0,external_react_.useContext)(UserContext.UserContext);
  const LEGACY_SITE = process.env.LEGACY_SITE;
  const {
    0: showSearch,
    1: setShowSearch
  } = (0,external_react_.useState)(false);
  let asideItems = [{
    label: "Home",
    link: "https://inventa.shop/"
  }, {
    label: "Com envios gratuitos",
    link: "#"
  }, {
    label: "Promoções",
    link: "#"
  }, {
    label: "Novos produtos",
    link: "#"
  }, {
    label: "Todas as marcas",
    link: "#"
  }, {
    label: "Todos os produtos",
    link: "#"
  }, {
    label: "Cosméticos",
    link: "#"
  }, {
    label: "Mercearia",
    link: "#"
  }, {
    label: "Casa e Decoração",
    link: "#"
  }];
  let profileItems = [{
    label: "Meus pedidos",
    link: "/account"
  }, {
    label: "Endereços",
    link: "/account/addresses"
  }, {
    label: "Crédito",
    link: "#"
  }, {
    label: "Sair",
    link: "/api/account/logout"
  }];
  const {
    0: showCreditPopUp,
    1: setShowCreditPopUp
  } = (0,external_react_.useState)(false);

  const handleToggleCreditPopUp = () => {
    setShowCreditPopUp(!showCreditPopUp);
  };

  const handleProfileItemClick = label => {
    if (label === "Crédito") {
      handleToggleCreditPopUp();
    }
  };

  const toggleSearch = () => {
    setShowSearch(!showSearch);
  };

  (0,external_react_.useEffect)(() => {
    (0,utils.listenEvent)("asidebar:open", () => {
      if (!show) setShow(true);
    });
    (0,utils.listenEvent)("asidebar:close", () => {
      setShow(false);
    });
    (0,utils.listenEvent)("credit-popup:open", () => {
      setShowCreditPopUp(true);
    });
  }, []);
  const {
    0: cartChange,
    1: setCartChange
  } = (0,external_react_.useState)(cartCount());
  (0,external_react_.useEffect)(() => {
    if (typeof cartChange !== "boolean" && cartCount() !== cartChange) {
      setCartChange(true);
      setInterval(() => {
        setCartChange(cartCount());
      }, 3000);
    }
  });
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("header", {
      className: "relative z-50",
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "bg-secondaryDark",
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(components_Container, {
          tailwindClass: `w-full h-[4.3125rem] flex flex-row relative items-center justify-between px-1 md:px-10 py-4 text-black lg:text-white`,
          children: [/*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
            href: "https://inventa.shop/"
            /* className="hover:text-primaryYellow text-[15px] font-light" */
            ,
            children: /*#__PURE__*/jsx_runtime_.jsx("span", {
              className: "text-white ml-5 md:ml-0 md:flex tracking-widest py-1 md:text-4xl text-xl font-logo-inventa hover:text-primaryYellow",
              children: "INVENTA"
            })
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: "flex flex-row items-center space-x-4 xl:space-x-18 xxl:space-x-12 xl:pr-5 mr-3 md:mr-0",
            children: [!userLogged && /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: "flex-col hidden lg:flex text-lg font-lato leading-6 mx-5",
              children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
                className: "flex flex-row items-center",
                children: [/*#__PURE__*/jsx_runtime_.jsx("a", {
                  href: "/account/login",
                  onClick: e => {
                    e.preventDefault();
                    (0,utils.triggerEvent)("modal_open:login");
                  },
                  className: "hover:text-primaryYellow text-[15px] font-light",
                  children: /*#__PURE__*/jsx_runtime_.jsx("span", {
                    children: "Entrar"
                  })
                }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("span", {
                  className: "text-base font-light",
                  children: [" ", "\xA0/\xA0", " "]
                }), /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
                  href: "https://inventa.shop/",
                  className: "hover:text-primaryYellow text-[15px] font-light",
                  children: /*#__PURE__*/jsx_runtime_.jsx("span", {
                    children: "Criar conta"
                  })
                })]
              })
            }), userLogged && /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
              className: "flex-col hidden lg:flex text-sm mx-5",
              children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("p", {
                className: "ml-3 mb-0.5 font-system font-light",
                children: ["Ol\xE1", " ", userLogged && typeof userLogged === "string" ? userLogged.substring(0, 17) : ""]
              }), /*#__PURE__*/jsx_runtime_.jsx(components_FloatingMenu, {
                arrow: "right",
                clickable: /*#__PURE__*/jsx_runtime_.jsx("div", {
                  className: "bg-inventaGrayText px-3 py-0.5 rounded-2xl space-x-2 flex flex-row",
                  children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("p", {
                    className: "text-white font-system",
                    children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
                      className: "mr-3 text-[16px]",
                      children: "Meus Pedidos"
                    }), /*#__PURE__*/jsx_runtime_.jsx("span", {
                      className: "material-icons-outlined text-[20px] font-bold absolute right-0 mt-0 top-1/2 -translate-y-1/2 text-white",
                      children: "expand_more"
                    })]
                  })
                }),
                children: profileItems.map((item, index) => {
                  if (item.label == "Sair") return /*#__PURE__*/jsx_runtime_.jsx("a", {
                    className: "py-1 px-5 hover:bg-basicMedium hover:text-primary hover:cursor-pointer last:rounded-b first:pt-2 last:pb-2 first:rounded-t font-lato font-bold text-secondaryDark text-sm",
                    href: item.link,
                    children: item.label
                  }, `${index}-${item.label}`);else if (item.label === "Crédito") return /*#__PURE__*/jsx_runtime_.jsx("a", {
                    className: "py-1 px-5 hover:bg-basicMedium hover:text-primary hover:cursor-pointer last:rounded-b first:pt-2 last:pb-2 first:rounded-t font-lato font-bold text-secondaryDark text-sm",
                    href: item.link,
                    onClick: () => handleProfileItemClick(item.label),
                    children: item.label
                  }, `${index}-${item.label}`);else return /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
                    href: item.link,
                    className: "py-1 px-5 hover:bg-basicMedium hover:text-primary hover:cursor-pointer last:rounded-b first:pt-2 last:pb-2 first:rounded-t font-lato font-bold text-secondaryDark text-sm",
                    children: item.label
                  }, `${index}-${item.label}`);
                })
              })]
            }), showSearch ? /*#__PURE__*/jsx_runtime_.jsx("span", {
              className: "material-icons-outlined lg:hidden",
              onClick: () => toggleSearch(),
              children: "close"
            }) : /*#__PURE__*/jsx_runtime_.jsx(jsx_runtime_.Fragment, {}), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Redirect/* default */.Z, {
              href: "https://inventa.shop",
              className: "flex cursor-pointer",
              children: [/*#__PURE__*/jsx_runtime_.jsx(components_Badge, {
                size: clientWidth >= 1024 ? "md" : "md",
                value: cartCount(),
                className: typeof cartChange === "boolean" && cartChange ? " motion-safe:animate-bounce" : "",
                children: /*#__PURE__*/jsx_runtime_.jsx("span", {
                  className: "material-icons-outlined lock-icon text-white",
                  children: "shopping_cart"
                })
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("span", {
                className: "hidden text-white md:inline-block pl-4 mt-2 text-base font-lato",
                children: [" ", "Carrinho"]
              })]
            })]
          })]
        })
      }), /*#__PURE__*/jsx_runtime_.jsx(components_HeaderMenu, {
        menuItems: menuItems
      }), show && /*#__PURE__*/(0,jsx_runtime_.jsxs)(components_Aside, {
        display: show,
        position: "left",
        timing: "ease-in-out",
        duration: "duration-500",
        className: "p-7 space-y-6",
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "flex justify-between items-center ",
          children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: "flex items-center space-x-2",
            children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
              className: "material-icons-outlined",
              children: "person_outline"
            }), userLogged ? /*#__PURE__*/(0,jsx_runtime_.jsxs)("p", {
              children: ["Ol\xE1", " ", userLogged && typeof userLogged === "string" ? userLogged.substring(0, 17) : ""]
            }) : /*#__PURE__*/jsx_runtime_.jsx("p", {
              className: "text-lg",
              children: "Cadastre-se"
            })]
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            children: /*#__PURE__*/jsx_runtime_.jsx("button", {
              className: "text-2xl font-thin focus:outline-none",
              onClick: () => {
                (0,utils.triggerEvent)("asidebar:close");
              },
              children: "x"
            })
          })]
        }), /*#__PURE__*/jsx_runtime_.jsx("ul", {
          className: "space-y-2",
          children: asideItems.map((item, index) => /*#__PURE__*/jsx_runtime_.jsx("li", {
            children: /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
              href: item.link,
              children: item.label
            })
          }, `${index}-${item.label}`))
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "border-t pt-6",
          children: userLogged ? /*#__PURE__*/jsx_runtime_.jsx("a", {
            href: "/api/account/logout",
            children: "Sair"
          }) : /*#__PURE__*/(0,jsx_runtime_.jsxs)("ul", {
            className: "space-y-2",
            children: [/*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "/account/login",
                onClick: e => {
                  e.preventDefault();
                  (0,utils.triggerEvent)("modal_open:login");
                },
                children: "Login"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
                href: "https://inventa.shop/",
                children: "Cadastrar-se"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("li", {
              children: /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
                href: "/pages/fornecedor",
                children: "Ser um Fornecedor"
              })
            })]
          })
        })]
      })]
    }), showCreditPopUp && /*#__PURE__*/jsx_runtime_.jsx(components_CreditPopUp, {
      toggle: handleToggleCreditPopUp
    })]
  });
};

/* harmony default export */ const innerComponents_Header = (Header);
;// CONCATENATED MODULE: ./components/Notification/index.js







const Notification = ({
  notification
}) => {
  const {
    clientWidth
  } = (0,external_react_.useContext)(UserContext.UserContext);
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "text-center px-2 py-3 bg-[#FFE2AA]",
    children: /*#__PURE__*/jsx_runtime_.jsx(components_Container, {
      children: notification ? /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "notification-banner",
        dangerouslySetInnerHTML: {
          __html: notification
        }
      }) : clientWidth >= 768 ? /*#__PURE__*/(0,jsx_runtime_.jsxs)("p", {
        className: "text-sm",
        children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "font-bold whitespace-nowrap",
          children: "Frete gr\xE1tis"
        }), " ", "para todo Brasil,", " ", /*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "font-bold whitespace-nowrap",
          children: "20% de desconto"
        }), " ", "na primeira compra e", " ", /*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "font-bold whitespace-nowrap",
          children: "parcelamento no cart\xE3o em 12x sem juros!"
        })]
      }) : /*#__PURE__*/jsx_runtime_.jsx(components_CollapseItem, {
        type: "notification",
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("p", {
          className: "text-sm",
          children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
            className: "font-bold whitespace-nowrap",
            children: "Frete gr\xE1tis"
          }), ", na primeira compra e", " ", /*#__PURE__*/jsx_runtime_.jsx("span", {
            className: "font-bold whitespace-nowrap",
            children: "12 parcelas sem juros!"
          })]
        })
      })
    })
  });
};

/* harmony default export */ const components_Notification = (Notification);
;// CONCATENATED MODULE: ./components/BottomNavbar/BottomNavbar/index.js





const BottomNavbar = ({
  auth
}) => {
  return /*#__PURE__*/jsx_runtime_.jsx("nav", {
    className: "fixed bottom-0 inset-x-0 bg-white shadow-md h-14 border-t px-4 flex items-center z-10",
    children: auth ? /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex items-center justify-between w-full text-basicDark px-4 md:px-8",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex justify-center items-center flex-col",
        children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "material-icons-outlined",
          children: "card_giftcard"
        }), /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
          href: "/account",
          className: "text-[0.5rem] hover:text-primaryYellow",
          children: "Meus Pedidos"
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex justify-center items-center flex-col",
        children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "material-icons-outlined",
          children: "local_shipping"
        }), /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
          href: "/account/addresses",
          className: "text-[0.5rem] hover:text-primaryYellow",
          children: "Endere\xE7os"
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex justify-center items-center flex-col",
        children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "material-icons-outlined",
          children: "monetization_on"
        }), /*#__PURE__*/jsx_runtime_.jsx("a", {
          href: "#",
          onClick: e => {
            e.preventDefault();
            (0,utils.triggerEvent)("credit-popup:open");
          },
          className: "text-[0.5rem] hover:text-primaryYellow",
          children: "Cr\xE9dito"
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
        href: "https://inventa.shop/",
        className: "flex justify-center items-center flex-col text-[0.5rem] hover:text-primaryYellow",
        onClick: e => {
          e.preventDefault();
          (0,utils.triggerEvent)("asidebar:open");
        },
        children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "material-icons-outlined",
          children: "more_horiz"
        }), "Mais"]
      })]
    }) : /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex items-center justify-center space-x-4 w-full h-full",
      children: [/*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
        href: "https://inventa.shop/",
        className: "bg-primaryYellow text-center border-primaryYellow text-black rounded text-xs font-roboto font-medium px-8 py-3.5 w-full whitespace-nowrap",
        children: "Cadastre-se"
      }), /*#__PURE__*/jsx_runtime_.jsx("button", {
        onClick: () => {
          (0,utils.triggerEvent)("modal_open:login");
        },
        className: "bg-white border border-black rounded text-dark text-xs font-roboto font-medium px-8 py-3 w-full",
        children: "Entrar"
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex justify-center items-center flex-col text-basicDark w-52 text-center my-4",
        children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "material-icons-outlined",
          children: "live_help"
        }), /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
          href: "/#faq",
          className: "text-[0.5rem] hover:text-primaryYellow",
          children: "FAQ Lojista"
        })]
      })]
    })
  });
};

/* harmony default export */ const BottomNavbar_BottomNavbar = (BottomNavbar);
;// CONCATENATED MODULE: ./components/layouts/InventaDefault/index.js












const Layout = ({
  children,
  menuItems,
  user,
  notification,
  footerItems
}) => {
  const auth = user && user.user;
  const {
    clientWidth
  } = (0,external_react_.useContext)(UserContext.UserContext);
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "fixed top-0 left-0 w-full z-50 mb-40",
      children: [/*#__PURE__*/jsx_runtime_.jsx(components_Notification, {
        notification: notification
      }), /*#__PURE__*/jsx_runtime_.jsx(innerComponents_Header, {
        userLogged: auth && user.first_name ? user.first_name : false,
        menuItems: menuItems
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx("main", {
      className: "mt-28",
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(components_Container, {
        children: [children, " ", clientWidth <= 768 ? /*#__PURE__*/jsx_runtime_.jsx(BottomNavbar_BottomNavbar, {
          auth: auth
        }) : null]
      })
    }), /*#__PURE__*/jsx_runtime_.jsx(innerComponents_Footer, {
      footerItems: footerItems
    }), /*#__PURE__*/jsx_runtime_.jsx(components_WhatsAppButton, {})]
  });
};

/* harmony default export */ const InventaDefault = (Layout);
;// CONCATENATED MODULE: ./components/ModalWrapper/index.js




const ModalWrapper = ({
  children,
  onClose,
  closeColor = "black"
}) => {
  const modalWrapperRef = (0,external_react_.useRef)(null);

  const handleClick = e => {
    if (!modalWrapperRef.current.innerHTML.includes(e.target.innerHTML) && typeof onClose == "function") onClose();
  };

  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    onClick: handleClick,
    className: "z-50 fixed flex justify-center items-center top-0 left-0 w-screen h-screen fade-in bg-neutral-700/50",
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      ref: modalWrapperRef,
      className: "bg-white relative w-full h-full md:w-3/4 md:h-fit md:min-w-[760px] md:min-h-[250px] md:max-h-[600px] xxl:max-h-[700px] md:max-w-[900px] overflow-y-auto",
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: `absolute z-10 px-2 right-0 text-3xl md:text-black text-${closeColor}`,
        onClick: () => {
          if (typeof onClose == "function") onClose();
        },
        children: /*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "material-icons-outlined cursor-pointer",
          children: "close"
        })
      }), children]
    })
  });
};

/* harmony default export */ const components_ModalWrapper = (ModalWrapper);
// EXTERNAL MODULE: ./components/Modals/Login/Forms/index.js
var Forms = __webpack_require__(6761);
var Forms_default = /*#__PURE__*/__webpack_require__.n(Forms);
;// CONCATENATED MODULE: ./components/Modals/Login/index.js








const banner_images = {
  "popup-mobile": "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/popup-NMKP-cropped.png",
  "popup-desktop": "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/popup-NMKP-cropped.png",
  "background-bg": "#a8cbde"
};

const Login = ({
  closeFunction
}) => {
  const {
    clientWidth
  } = (0,external_react_.useContext)(UserContext.UserContext);
  const {
    0: form,
    1: setForm
  } = (0,external_react_.useState)("Login");
  const Form = (Forms_default())[form];
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: "md:flex h-[700px]",
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "h-[420px] md:h-full md:w-[400px] md:max-w-[400px] bg-testAbBackground relative",
      children: [/*#__PURE__*/jsx_runtime_.jsx("h1", {
        className: "p-5 md:mt-10 pl-10 mb-10 text-4xl font-noto text-white",
        children: "O atacado online exclusivo para lojistas"
      }), /*#__PURE__*/jsx_runtime_.jsx(next_image.default, {
        className: "custom-image w-auto md-w-100",
        alt: "Banner image",
        src: clientWidth >= 768 ? banner_images["popup-desktop"] : banner_images["popup-mobile"],
        width: clientWidth >= 768 ? 400 : clientWidth,
        height: clientWidth >= 768 ? 507 : parseInt(clientWidth * 0.63),
        quality: 90,
        loading: "eager"
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex-1",
      children: [Form && /*#__PURE__*/jsx_runtime_.jsx(Form, {
        changeForm: setForm
      }), /*#__PURE__*/jsx_runtime_.jsx(ToRegisterButton, {})]
    })]
  });
};

/* harmony default export */ const Modals_Login = (Login);

function ToRegisterButton() {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: "px-12 w-full pb-6",
    children: [/*#__PURE__*/jsx_runtime_.jsx("h4", {
      className: "text-xl font-bold font-poppins mb-4 pt-6 border-t border-solid border-inventaBorderGray",
      children: "Primeira vez na Inventa?"
    }), /*#__PURE__*/jsx_runtime_.jsx(Redirect/* default */.Z, {
      href: "/",
      children: /*#__PURE__*/jsx_runtime_.jsx("p", {
        className: "border-black border text-center mt-3 text-lg font-bold font-poppins py-4 rounded",
        children: "Cadastre-se como Lojista"
      })
    })]
  });
}
;// CONCATENATED MODULE: ./services/contexts/ModalContext.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








const ModalContext = /*#__PURE__*/(0,external_react_.createContext)();
const ModalProvider = ({
  children,
  userAuth
}) => {
  const {
    0: modal,
    1: setModal
  } = (0,external_react_.useState)();

  const closeModal = () => {
    setModal(undefined);
  };

  const openModal = ({
    name,
    component,
    props = {},
    closeColor = "black"
  }) => {
    if (!component) throw {
      error: "No component assigned to modal"
    };
    setModal({
      name,
      component,
      props,
      closeColor
    });
  };

  (0,external_react_.useEffect)(() => {
    (0,utils.listenEvent)("modal_open:login", () => openModal({
      name: "login",
      component: Modals_Login,
      closeColor: "white"
    }));
  }, []);
  const value = {};
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(ModalContext.Provider, {
    value: value,
    children: [children, modal && modal.component ? /*#__PURE__*/jsx_runtime_.jsx(components_ModalWrapper, {
      closeColor: modal.closeColor,
      onClose: closeModal,
      name: modal.name,
      children: /*#__PURE__*/jsx_runtime_.jsx(modal.component, _objectSpread({
        closeFunction: closeModal
      }, modal.props))
    }) : /*#__PURE__*/jsx_runtime_.jsx(jsx_runtime_.Fragment, {})]
  });
};
;// CONCATENATED MODULE: external "next/head"
const head_namespaceObject = require("next/head");
var head_default = /*#__PURE__*/__webpack_require__.n(head_namespaceObject);
;// CONCATENATED MODULE: ./pages/_app.js
function _app_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _app_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { _app_ownKeys(Object(source), true).forEach(function (key) { _app_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { _app_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _app_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }











function MyApp({
  Component,
  pageProps
}) {
  const {
    user,
    notification,
    pageData = {},
    menuItems,
    footerItems
  } = pageProps;
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)((head_default()), {
      children: [/*#__PURE__*/jsx_runtime_.jsx("title", {
        children: pageData.title_main
      }), /*#__PURE__*/jsx_runtime_.jsx(components_MetaTags, {
        pageData: pageData
      }), !(user !== null && user !== void 0 && user.user) && /*#__PURE__*/jsx_runtime_.jsx("script", {
        defer: true,
        src: "/legacy-login.js"
      }), /*#__PURE__*/jsx_runtime_.jsx("script", {
        defer: true,
        src: "/legacy-redirect.js"
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(UserContext.UserProvider, {
      user_data: user,
      children: /*#__PURE__*/jsx_runtime_.jsx(ModalProvider, {
        children: /*#__PURE__*/jsx_runtime_.jsx(InventaDefault, {
          menuItems: menuItems,
          user: user,
          notification: notification,
          footerItems: footerItems,
          children: /*#__PURE__*/jsx_runtime_.jsx(Component, _app_objectSpread({}, pageProps))
        })
      })
    })]
  });
}

/* harmony default export */ const _app = (MyApp);

/***/ }),

/***/ 4899:
/***/ ((module) => {

module.exports = require("amazon-cognito-identity-js");

/***/ }),

/***/ 8248:
/***/ ((module) => {

module.exports = require("autoprefixer");

/***/ }),

/***/ 7435:
/***/ ((module) => {

module.exports = require("graphql-request");

/***/ }),

/***/ 822:
/***/ ((module) => {

module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 6695:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 556:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/to-base-64.js");

/***/ }),

/***/ 9297:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 5282:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [675,580], () => (__webpack_exec__(2331)));
module.exports = __webpack_exports__;

})();