"use strict";
exports.id = 580;
exports.ids = [580];
exports.modules = {

/***/ 6645:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__);
const _excluded = ["href", "toLegacy", "children"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }




const Redirect = _ref => {
  let {
    href = "/",
    toLegacy,
    children
  } = _ref,
      rest = _objectWithoutProperties(_ref, _excluded);

  const host = process.env.LEGACY_SITE;
  const {
    0: props,
    1: setProps
  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(_objectSpread({
    href: toLegacy ? host + href : href
  }, rest));
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    if (toLegacy) {
      setProps(_objectSpread(_objectSpread({}, props), {}, {
        onMouseDown: e => {
          if (e.button === 1) {
            e.preventDefault();

            if (href != "#") {
              const cartUpdates = window.localStorage.getItem("cart_user-updates");
              window.localStorage.removeItem("cart_user-updates");

              if (cartUpdates && typeof window.legacyRedirect === "function") {
                const data = {};
                data["cart_user-updates"] = cartUpdates;
                legacyRedirect(href, encodeURIComponent(JSON.stringify(data)), process.env.LEGACY_SITE, true);
              } else window.open(process.env.LEGACY_SITE + href, "_blank");
            }
          }
        },
        onClick: e => {
          const controlClick = e.ctrlKey ? true : false;
          e.preventDefault();

          if (href != "#") {
            const cartUpdates = window.localStorage.getItem("cart_user-updates");
            window.localStorage.removeItem("cart_user-updates");

            if (cartUpdates && typeof window.legacyRedirect === "function") {
              const data = {};
              data["cart_user-updates"] = cartUpdates;
              legacyRedirect(href, encodeURIComponent(JSON.stringify(data)), process.env.LEGACY_SITE, controlClick);
            } else {
              if (controlClick) window.open(process.env.LEGACY_SITE + href, "_blank");else window.location.href = process.env.LEGACY_SITE + href;
            }
          }
        }
      }));
    }
  }, []);
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("a", _objectSpread(_objectSpread({}, props), {}, {
    children: children
  }));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Redirect);

/***/ }),

/***/ 6404:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserContext": () => (/* binding */ UserContext),
/* harmony export */   "UserProvider": () => (/* binding */ UserProvider)
/* harmony export */ });
/* harmony import */ var _services_shopify_requests__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6867);
/* harmony import */ var _services_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3393);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4899);
/* harmony import */ var amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const UserContext = /*#__PURE__*/(0,react__WEBPACK_IMPORTED_MODULE_1__.createContext)();
const UserProvider = ({
  children,
  user_data
}) => {
  const {
    0: userCat,
    1: setUserCat
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
  const {
    0: clientWidth,
    1: setClientWidth
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
  const {
    0: user
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(user_data);
  const {
    0: cartInfo,
    1: setCartInfo
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(user_data && user_data.cart_items ? JSON.parse(user_data.cart_items) : []);

  const getCartInfo = () => cartInfo;

  const cartCount = () => cartInfo.reduce((previousValue, item) => previousValue + item.qty, 0);

  const updateQty = ({
    id,
    qty
  }) => {
    const newArray = refreshUpdatesStorage({
      id,
      qty
    });
    setCartInfo(newArray);
  };

  const refreshUpdatesStorage = ({
    id,
    qty
  }) => {
    const cartStorage = window.localStorage.getItem("cart_user-updates");

    if (cartStorage) {
      let found = false;
      const parsed = JSON.parse(cartStorage);
      parsed.map(item => {
        if (item.id == id) {
          found = true;
          item.qty = qty;
        }
      });
      if (!found) parsed.push({
        id,
        qty
      });
      window.localStorage.setItem("cart_user-updates", JSON.stringify(parsed));
      return parsed;
    } else {
      window.localStorage.setItem("cart_user-updates", JSON.stringify([{
        id,
        qty
      }]));
      return [{
        id,
        qty
      }];
    }
  };

  const cartInit = () => {
    (0,_services_utils__WEBPACK_IMPORTED_MODULE_4__.listenEvent)("cart:update-product", ({
      id,
      qty
    }) => {
      updateQty({
        id,
        qty
      });
    });
    (0,_services_utils__WEBPACK_IMPORTED_MODULE_4__.listenEvent)("cart:remove-product", ({
      id
    }) => {
      updateQty({
        id,
        qty: 0
      });
    });
  };

  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    setClientWidth(window.innerWidth);

    window.onresize = () => {
      setClientWidth(window.innerWidth);
    };

    const cart = user_data && user_data.cart_items ? JSON.parse(user_data.cart_items) : [];

    if (Array.isArray(cart)) {
      setCartInfo(cart);
      cartInit();
      window.localStorage.setItem("cart_user-updates", JSON.stringify(cart));
    }
  }, []);

  const getUserCat = () => userCat.token;

  const userLogin = async (email, password) => {
    if (email, password) {
      const {
        customerAccessTokenCreate: {
          customerUserErrors,
          customerAccessToken
        }
      } = await (0,_services_shopify_requests__WEBPACK_IMPORTED_MODULE_0__.LoginUser)({
        email,
        password
      });

      if (Array.isArray(customerUserErrors) && customerUserErrors.length) {
        console.warn("Login error", _objectSpread({}, customerUserErrors));
        return {
          ok: false,
          error: "invalid_creds"
        };
      }

      if (customerAccessToken) {
        const {
          accessToken,
          expiresAt
        } = customerAccessToken; //setUserCat({ token: accessToken, expiresAt });
        //To change

        if (typeof window.legacyLogin === "function") window.legacyLogin(email, password, process.env.LEGACY_SITE);
        return {
          ok: true
        };
      }
    } else return {
      ok: false,
      error: "missing_creds"
    };
  };

  const sendLoginCode = (email, callback) => {
    let attempts = -1;
    const prod = !window.location.host.includes("dev");
    const poolData = prod ? {
      // Prod
      UserPoolId: "us-east-1_Gn9H5xdeq",
      ClientId: "c99caq12iv5462qitbr66avud"
    } : {
      // Dev
      UserPoolId: "us-east-2_WqeLGvUIN",
      ClientId: "6hqh9mntss3k7jcu8hd07quas"
    };
    const newUserPool = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_2__.CognitoUserPool(poolData);
    const authenticationDetails = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_2__.AuthenticationDetails({
      Username: email
    });
    const user = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_2__.CognitoUser({
      Username: email,
      Pool: newUserPool
    });
    user.setAuthenticationFlowType("CUSTOM_AUTH");
    user.initiateAuth(authenticationDetails, customAuth());

    function customAuth() {
      return {
        onSuccess: result => {
          const jwtToken = result.idToken.jwtToken;
          const corsUrl = "https://let-cors-anywhere.herokuapp.com/";
          const inventaUrl = prod ? "https://auth.inventa.tec.br/login/shopify" : "https://auth.inventa.dev.br/login/shopify";
          fetch(`${corsUrl}${inventaUrl}`, {
            method: "GET",
            headers: {
              origin: "shopify",
              Authorization: jwtToken
            }
          }).then(res => {
            return res.json();
          }).then(res => {
            const redirectUrl = res.result;
            window.location.replace(redirectUrl);
          });
        },
        onFailure: err => {
          if (typeof callback == "function") callback({
            ok: false,
            err
          });
        },
        customChallenge: challengeParameters => {
          attempts++;

          if (typeof callback == "function") {
            callback({
              ok: true,
              triggerFunction: function (code) {
                if (code) user.sendCustomChallengeAnswer(code, customAuth());
              },
              attempts
            });
          }
        }
      };
    }
  };

  const value = {
    user,
    getUserCat,
    sendLoginCode,
    userLogin,
    clientWidth,
    cartInfo,
    getCartInfo,
    cartCount
  };
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(UserContext.Provider, {
    value: value,
    children: children
  });
};

/***/ }),

/***/ 6867:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "AddLine": () => (/* binding */ AddLine),
  "CreateCart": () => (/* binding */ CreateCart),
  "CreateCheckout": () => (/* binding */ CreateCheckout),
  "EditLine": () => (/* binding */ EditLine),
  "GetCart": () => (/* binding */ GetCart),
  "GetProducts": () => (/* binding */ GetProducts),
  "LinkCustomerToCheckout": () => (/* binding */ LinkCustomerToCheckout),
  "LoginUser": () => (/* binding */ LoginUser),
  "RecoverCart": () => (/* binding */ RecoverCart),
  "ResetPassword": () => (/* binding */ ResetPassword)
});

// EXTERNAL MODULE: external "graphql-request"
var external_graphql_request_ = __webpack_require__(7435);
;// CONCATENATED MODULE: ./services/shopify-requests/queries.js

const customer_login = ({
  email,
  password
}) => external_graphql_request_.gql`
    mutation {
        customerAccessTokenCreate(input: {email: "${email}", password: "${password}"}) {
            customerAccessToken {
                accessToken,
                expiresAt
            },
            customerUserErrors {
                code
            }
        }
    }
`;
const customer_reset_password = email => external_graphql_request_.gql`
  mutation {
    customerRecover(email: "${email}") {
      customerUserErrors{
        code
        field
        message
      }
    }
  }
`;
const cart_create = () => external_graphql_request_.gql`
  mutation createCart($cartInput: CartInput) {
    cartCreate(input: $cartInput) {
      cart {
        id
        createdAt
        updatedAt
        lines(first: 10) {
          edges {
            node {
              id
              quantity
              merchandise {
                ... on ProductVariant {
                  id
                }
              }
            }
          }
        }
        attributes {
          key
          value
        }
        estimatedCost {
          totalAmount {
            amount
            currencyCode
          }
          subtotalAmount {
            amount
            currencyCode
          }
          totalTaxAmount {
            amount
            currencyCode
          }
          totalDutyAmount {
            amount
            currencyCode
          }
        }
      }
    }
  }
`;
const cart_recover = ({
  cart_id
}) => external_graphql_request_.gql`
cart(id:"${cart_id}") {
    id
    createdAt
    updatedAt
    lines(first:10) {
      edges {
        node {
          id
          quantity
          merchandise {
            ... on ProductVariant {
              id
            }
          }
          attributes {
            key
            value
          }
        }
      }
    }
    attributes {
      key
      value
    }
    estimatedCost {
      totalAmount {
        amount
        currencyCode
      }
      subtotalAmount {
        amount
        currencyCode
      }
      totalTaxAmount {
        amount
        currencyCode
      }
      totalDutyAmount {
        amount
        currencyCode
      }
    }
    buyerIdentity {
      email
      phone
      customer {
        id
      }
      countryCode
    }
  }
}
`;
const cart_modify = () => external_graphql_request_.gql`
  mutation cartLinesUpdate($cartId: ID!, $lines: [CartLineUpdateInput!]!) {
    cartLinesUpdate(cartId: $cartId, lines: $lines) {
      cart {
        id
        lines(first: 10) {
          edges {
            node {
              id
              quantity
              merchandise {
                ... on ProductVariant {
                  id
                }
              }
            }
          }
        }
        estimatedCost {
          totalAmount {
            amount
            currencyCode
          }
          subtotalAmount {
            amount
            currencyCode
          }
          totalTaxAmount {
            amount
            currencyCode
          }
          totalDutyAmount {
            amount
            currencyCode
          }
        }
      }
    }
  }
`;
const cart_addLine = () => external_graphql_request_.gql`
  mutation cartLinesAdd($lines: [CartLineInput!]!, $cartId: ID!) {
    cartLinesAdd(lines: $lines, cartId: $cartId) {
      cart {
        id
        lines(first: 10) {
          edges {
            node {
              id
              quantity
              merchandise {
                ... on ProductVariant {
                  id
                }
              }
            }
          }
        }
        estimatedCost {
          totalAmount {
            amount
            currencyCode
          }
          subtotalAmount {
            amount
            currencyCode
          }
          totalTaxAmount {
            amount
            currencyCode
          }
          totalDutyAmount {
            amount
            currencyCode
          }
        }
      }
      userErrors {
        code
        field
        message
      }
    }
  }
`;
const products = ({
  quantity
}) => external_graphql_request_.gql`
{
    products(first: ${quantity}) {
        edges {
            node {
                id
                title
                description
                handle
                tags
                images(first: 1) { 
                    edges {
                        node {
                            altText
                            transformedSrc(maxWidth: 200, maxHeight: 200)
                        }
                    }
                }
                variants(first: 100) {
                    edges {
                        node {
                            id
                            title
                            quantityAvailable
                            priceV2 {
                                amount
                                currencyCode
                            }
                        }
                    }
                }
            }
        }
    }
}
`;
const cart_get = ({
  cartId
}) => external_graphql_request_.gql`
  query checkoutURL {
    cart(id: "${cartId}") {
        id
        createdAt
        updatedAt
        lines(first:10) {
          edges {
            node {
              id
              quantity
              merchandise {
                ... on ProductVariant {
                  id
                }
              }
              attributes {
                key
                value
              }
            }
          }
        }
    }
  }
`;
const checkout_create = () => external_graphql_request_.gql`
  mutation checkoutCreate($input: CheckoutCreateInput!) {
    checkoutCreate(input: $input) {
      checkout {
        id
        webUrl
        lineItems(first: 5) {
          edges {
            node {
              title
              quantity
            }
          }
        }
      }
    }
  }
`;
const checkout_linkCustomer = () => external_graphql_request_.gql`
  mutation associateCustomerWithCheckout(
    $checkoutId: ID!
    $customerAccessToken: String!
  ) {
    checkoutCustomerAssociateV2(
      checkoutId: $checkoutId
      customerAccessToken: $customerAccessToken
    ) {
      checkout {
        id
        webUrl
      }
      checkoutUserErrors {
        code
        field
        message
      }
      customer {
        id
      }
    }
  }
`;
const checkout_redirectLink = ({
  cartId
}) => gql`
query {
    node(id:"${cartId}" ) {
      ... on Checkout {
        id
        webUrl
      }
    }
  }
`;
;// CONCATENATED MODULE: ./services/shopify-requests/index.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//Here we make every request via Graphql to Shopify
//The functions are declared in this file, the queries are in the queries.js file.


const client = new external_graphql_request_.GraphQLClient(`https://${process.env.SHOPIFY_DOMAIN}/api/2021-10/graphql.json`, {
  headers: {
    "Content-Type": "application/json",
    "x-shopify-storefront-access-token": process.env.SHOPIFY_ACCESS_TOKEN
  }
});
const LoginUser = async variables => await client.request(customer_login(_objectSpread({}, variables)));
const ResetPassword = async email => await client.request(customer_reset_password(email));
const RecoverCart = async cart_id => await client.request(cart_recover({
  cart_id
}));
const CreateCart = async ({
  id,
  quantity
}) => await client.request(cart_create(), {
  cartInput: {
    lines: [{
      quantity: quantity,
      merchandiseId: id
    }]
  }
});
const EditLine = async ({
  cartId,
  line_items
}) => await client.request(cart_modify(), {
  cartId,
  lines: line_items
});
const AddLine = async ({
  cartId,
  items
}) => await client.request(cart_addLine(), {
  lines: items,
  cartId
});
const GetProducts = async quantity => await client.request(products({
  quantity
}));
const GetCart = async cartId => await client.request(cart_get({
  cartId
}));
const CreateCheckout = async line_items => await client.request(checkout_create(), {
  input: {
    lineItems: line_items
  }
});
const LinkCustomerToCheckout = async ({
  checkoutId,
  customerAccessToken
}) => await client.request(checkout_linkCustomer(), {
  checkoutId,
  customerAccessToken
});

/***/ }),

/***/ 3393:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "triggerEvent": () => (/* binding */ triggerEvent),
/* harmony export */   "listenEvent": () => (/* binding */ listenEvent),
/* harmony export */   "saveUserAccessToken": () => (/* binding */ saveUserAccessToken),
/* harmony export */   "getUserAccessToken": () => (/* binding */ getUserAccessToken),
/* harmony export */   "findTag": () => (/* binding */ findTag),
/* harmony export */   "formatPrice": () => (/* binding */ formatPrice),
/* harmony export */   "getAuthToken": () => (/* binding */ getAuthToken),
/* harmony export */   "shopifyImagesLoader": () => (/* binding */ shopifyImagesLoader),
/* harmony export */   "validEmail": () => (/* binding */ validEmail),
/* harmony export */   "validPhone": () => (/* binding */ validPhone),
/* harmony export */   "productInfoForUser": () => (/* binding */ productInfoForUser),
/* harmony export */   "getStaticInformation": () => (/* binding */ getStaticInformation),
/* harmony export */   "getStaticFooterInformation": () => (/* binding */ getStaticFooterInformation)
/* harmony export */ });
const _excluded = ["variants"],
      _excluded2 = ["original_price"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

const triggerEvent = (name, detail = {}) => {
  if (!name) throw {
    error: 'Missing "name" property'
  };else {
    const event = new CustomEvent(name, {
      detail
    });
    window.dispatchEvent(event);
  }
};
const listenEvent = (name, callback) => {
  if (name) {
    window.addEventListener(name, e => {
      if (typeof callback == "function") callback(e.detail);
    });
  } else {
    throw {
      error: 'Missing required "name" property'
    };
  }
};
const saveUserAccessToken = ({
  token,
  expiresAt
}) => {
  if (token && expiresAt) window.localStorage.setItem("user_cat", JSON.stringify({
    token,
    expiresAt
  }));else throw {
    error: "Missing required properties"
  };
};
const getUserAccessToken = () => {
  const user_cat = window.localStorage.getItem("user_cat");

  if (user_cat) {
    try {
      const {
        token,
        expiresAt
      } = JSON.parse(user_cat);
      if (token && new Date().getTime() < new Date(expiresAt).getTime()) return {
        token,
        expiresAt
      };else {
        window.localStorage.removeItem("user_cat");
        return null;
      }
    } catch (error) {
      window.localStorage.removeItem("user_cat");
      return null;
    }
  } else return null;
};
const findTag = (tags, target) => {
  if (Array.isArray(tags)) {
    const result = tags.find(tag => {
      if (tag.toLowerCase().includes(target.toLowerCase())) return true;else return false;
    });
    if (result) return result.split(target);else return null;
  } else return null;
};
const formatPrice = number => {
  return new Intl.NumberFormat("pt-BR", {
    style: "currency",
    currency: "BRL"
  }).format(number);
};
/* import jwt from "jsonwebtoken";
import { JWT_SECRET_SIGN } from "@constants/index"; */

const getAuthToken = cookies => {
  if (cookies && cookies["user-inventa"]) return cookies["user-inventa"];else return null;
};
const shopifyImagesLoader = ({
  src,
  width,
  height
}) => {
  if (!width || !height) throw {
    error: "Provide Width && Height"
  };
  const a = src.split("/");
  const file = a[a.length - 1];
  const fileName = file.slice(0, file.lastIndexOf("."));
  const rest = file.slice(file.lastIndexOf("."), file.length);
  return src.replace(file, fileName + "_" + width + "x" + height + rest);
};
const validEmail = value => value && value.match(/^[\w-\.\+\u00C0-\u017F]+@([\w-]+\.)+[\w-]{2,6}$/g);
const validPhone = value => value && value.match(/^\d{10,11}\b/g);
const productInfoForUser = (product, user_data = {}) => {
  const {
    user
  } = user_data ? user_data : {};

  if (!user) {
    const {
      variants
    } = product,
          rest = _objectWithoutProperties(product, _excluded);

    const variants_noPrice = variants.map(variant => {
      const {
        variant_price
      } = variant;

      const {
        original_price
      } = variant_price,
            vp = _objectWithoutProperties(variant_price, _excluded2);

      const newVariant_price = _objectSpread({}, vp);

      return _objectSpread(_objectSpread({}, variant), {}, {
        variant_price: newVariant_price
      });
    });
    return _objectSpread({
      variants: variants_noPrice
    }, rest);
  } else return product;
};
let static_data;
const getStaticInformation = async () => {
  if (!static_data) {
    const data = await fetch("https://inventa-mktp-assets.s3.amazonaws.com/main-menu.json");
    static_data = await data.json();
  }

  return static_data;
};
let footer_static;
const getStaticFooterInformation = async () => {
  if (!footer_static) {
    const data = await fetch("https://inventa-mktp-assets.s3.amazonaws.com/footer-menu.json");
    footer_static = await data.json();
  }

  return footer_static;
};

/***/ })

};
;