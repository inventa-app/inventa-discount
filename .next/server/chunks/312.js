exports.id = 312;
exports.ids = [312];
exports.modules = {

/***/ 4730:
/***/ ((__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) => {

"use strict";

// UNUSED EXPORTS: default

// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(5675);
// EXTERNAL MODULE: external "@splidejs/react-splide"
var react_splide_ = __webpack_require__(4402);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/HidePriceButton/index.js



const HidePriceButton_HidePrice = () => {
  return /*#__PURE__*/_jsx("button", {
    className: "w-full text-center bg-primaryYellow text-white font-medium font-roboto text-base rounded py-4 md:py-5 lg:py-6 mt-1",
    onClick: () => triggerEvent("modal_open:login"),
    children: "Ver Pre\xE7o"
  });
};

/* harmony default export */ const HidePriceButton = ((/* unused pure expression or super */ null && (HidePriceButton_HidePrice)));
// EXTERNAL MODULE: ./components/QuantityComponent/index.js
var QuantityComponent = __webpack_require__(5293);
;// CONCATENATED MODULE: ./components/ProductCard/index.js







const ProductCard_ProductCard = ({
  userLogged,
  title,
  minimal_order,
  image,
  price,
  minimal_quant,
  discount
}) => {
  return /*#__PURE__*/_jsxs("li", {
    className: "w-40",
    children: [/*#__PURE__*/_jsx(Image, {
      src: image,
      alt: title,
      width: 170,
      height: 170,
      className: "border-gray-100"
    }), userLogged ? /*#__PURE__*/_jsxs("h2", {
      className: "text-inventaGrayText text-xs font-normal",
      children: [title, " | Min ", formatPrice(minimal_order)]
    }) : /*#__PURE__*/_jsx("h2", {
      className: "text-inventaGrayText text-xs font-normal",
      children: title
    }), /*#__PURE__*/_jsxs("span", {
      className: "flex",
      children: [userLogged ? /*#__PURE__*/_jsx("h3", {
        className: "flex text-lg",
        children: formatPrice(price)
      }) : /*#__PURE__*/_jsxs("h3", {
        className: "text-lg whitespace-nowrap",
        children: ["R$", /*#__PURE__*/_jsx("span", {
          className: "relative top-1 material-icons-outlined",
          children: "lock"
        })]
      }), discount > 0 && /*#__PURE__*/_jsxs("p", {
        className: "text-xs text-red-500 ml-2 mt-0.5",
        children: [discount, "% off"]
      })]
    }), /*#__PURE__*/_jsxs("p", {
      className: "text-xs font-light font-lato text-black ",
      children: ["Quantidade m\xEDnima ", minimal_quant, " und"]
    }), userLogged ? /*#__PURE__*/_jsx(QuantityComp, {
      quantity: {
        min_quantity: 0
      },
      stock_props: {
        sell_without_stock: false
      }
    }) : /*#__PURE__*/_jsx(HidePrice, {})]
  });
};

/* harmony default export */ const components_ProductCard = ((/* unused pure expression or super */ null && (ProductCard_ProductCard)));
;// CONCATENATED MODULE: ./components/Carousel/breakpoints.js
const breakpoints_breakpointsTypes = {
  "brands": {
    1200: {
      perPage: 7
    },
    1024: {
      perPage: 6,
      gap: "2rem"
    },
    768: {
      perPage: 4,
      gap: "2rem"
    },
    600: {
      perPage: 3
    },
    425: {
      perPage: 3,
      gap: "1rem"
    },
    375: {
      perPage: 3,
      gap: "2rem"
    },
    360: {
      perPage: 3,
      gap: "3.5rem"
    }
  },
  "products": {
    1200: {
      perPage: 6
    },
    1024: {
      perPage: 4.5,
      gap: "1rem"
    },
    768: {
      perPage: 4,
      gap: "4rem"
    },
    600: {
      perPage: 3
    },
    425: {
      perPage: 2.5,
      gap: "5rem"
    },
    375: {
      perPage: 3,
      gap: "8rem"
    },
    360: {
      perPage: 2,
      gap: "0.5rem"
    }
  },
  "highlights": {
    1440: {
      perPage: 4
    },
    1200: {
      perPage: 4,
      gap: "1rem"
    },
    1024: {
      perPage: 3,
      gap: "1rem"
    },
    1000: {
      perPage: 2.5
    },
    768: {
      perPage: 2
    },
    425: {
      perPage: 2
    },
    375: {
      perPage: 3
    },
    360: {
      perPage: 2,
      gap: "0.5rem"
    }
  }
};
;// CONCATENATED MODULE: ./components/Carousel/index.js










const Carousel = ({
  userLogged,
  type,
  items,
  title
}) => {
  const arrowsStyle = {
    arrows: `splide__arrows ${style["arrows"]}`,
    arrow: "splide__arrow",
    prev: `splide__arrow--prev ${style["splide__arrow--prev"]}`,
    next: `splide__arrow--next ${style["splide__arrow--next"]}`
  };
  return /*#__PURE__*/_jsxs("div", {
    className: "w-[95%] pl-2 relative left-2",
    children: [/*#__PURE__*/_jsx("h3", {
      className: "w-[75%] text-lg font-lato text-inventaGrayText font-medium\tml-2 mb-6 md:text-[2rem] md:leading-9",
      children: title
    }), /*#__PURE__*/_jsx(Splide, {
      options: {
        pagination: false,
        rewind: true,
        gap: type == "brands" ? "2rem" : "1rem",
        arrowPath: "M7.98 10L32 10L32 6L7.98 6V-9.53674e-07L0 8L7.98 16V10V10Z",
        classes: arrowsStyle,
        perPage: type == "brands" ? 8 : 7,
        breakpoints: breakpointsTypes[type]
      },
      children: items.map((item, i) => type == "brands" ? /*#__PURE__*/_jsx(SplideSlide, {
        children: /*#__PURE__*/_jsxs("div", {
          className: "w-fit h-full text-center mb-12",
          children: [/*#__PURE__*/_jsx("a", {
            className: "flex w-32 h-32 m-auto p-4  border border-slate-300",
            href: item.link,
            children: /*#__PURE__*/_jsx(Image, {
              src: item.image,
              alt: item.name,
              width: 100,
              height: 100
            })
          }), /*#__PURE__*/_jsxs("div", {
            className: "mb-5 text-center w-32",
            children: [/*#__PURE__*/_jsx("a", {
              href: item.link,
              children: /*#__PURE__*/_jsx("p", {
                className: "mt-2 text-xs",
                children: item.name
              })
            }), /*#__PURE__*/_jsxs("p", {
              className: "text-basicDark text-xs font-light w-fit m-auto",
              children: ["M\xEDn. ", formatPrice(item.price)]
            })]
          })]
        })
      }, item + i) : /*#__PURE__*/_jsx(SplideSlide, {
        children: /*#__PURE__*/_jsx("div", {
          className: "mb-12 w-fit",
          children: /*#__PURE__*/_jsx("ul", {
            children: /*#__PURE__*/_jsx(ProductCard, {
              userLogged: userLogged,
              title: item.title,
              minimal_order: item.minimal_order,
              image: item.image,
              price: item.price,
              minimal_quant: item.minimal_quant,
              discount: item.discount
            }, item.id)
          })
        })
      }, item + i))
    })]
  });
};

/* harmony default export */ const components_Carousel = ((/* unused pure expression or super */ null && (Carousel)));

/***/ }),

/***/ 2758:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* unused harmony exports brands, products */
const ProductImage = "/general_images/product_image.png";
const brands = [{
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/smell-of-fragrance_100x.png",
  name: "Smell of Fragrance",
  link: "https://inventa.shop/collections/vendors?q=Smell+of+Fragrance",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/megazinebr_100x.png",
  name: "MegazineBR",
  link: "https://inventa.shop/collections/vendors?q=MegazineBR",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/ricosti_100x.png",
  name: "Ricosti",
  link: "https://inventa.shop/collections/vendors?q=Ricosti",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/bioz-green_100x.png",
  name: "Bioz Green",
  link: "https://inventa.shop/collections/vendors?q=Bioz+Green",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/olea-saboaria_100x.png",
  name: "Olea Saboaria",
  link: "https://inventa.shop/collections/vendors?q=Olea+Saboaria",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/natural-science_100x.png",
  name: "Natural Science",
  link: "https://inventa.shop/collections/vendors?q=Natural+Science",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/oz-candy_100x.png",
  name: "Oz Candy",
  link: "https://inventa.shop/collections/vendors?q=Oz+Candy",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/raizes-do-campo_100x.png",
  name: "Raízes do Campo",
  link: "https://inventa.shop/collections/vendors?q=Ra%C3%ADzes+do+Campo",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/luzz-cacau_100x.png",
  name: "Luzz Cacau",
  link: "https://inventa.shop/collections/vendors?q=Luzz+Cacau",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/royalwax_100x.png",
  name: "RoyalWax",
  link: "https://inventa.shop/collections/vendors?q=RoyalWax",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/smell-of-fragrance_100x.png",
  name: "Smell of Fragrance",
  link: "https://inventa.shop/collections/vendors?q=Smell+of+Fragrance",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/megazinebr_100x.png",
  name: "MegazineBR",
  link: "https://inventa.shop/collections/vendors?q=MegazineBR",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/ricosti_100x.png",
  name: "Ricosti",
  link: "https://inventa.shop/collections/vendors?q=Ricosti",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/bioz-green_100x.png",
  name: "Bioz Green",
  link: "https://inventa.shop/collections/vendors?q=Bioz+Green",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/olea-saboaria_100x.png",
  name: "Olea Saboaria",
  link: "https://inventa.shop/collections/vendors?q=Olea+Saboaria",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/natural-science_100x.png",
  name: "Natural Science",
  link: "https://inventa.shop/collections/vendors?q=Natural+Science",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/oz-candy_100x.png",
  name: "Oz Candy",
  link: "https://inventa.shop/collections/vendors?q=Oz+Candy",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/raizes-do-campo_100x.png",
  name: "Raízes do Campo",
  link: "https://inventa.shop/collections/vendors?q=Ra%C3%ADzes+do+Campo",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/luzz-cacau_100x.png",
  name: "Luzz Cacau",
  link: "https://inventa.shop/collections/vendors?q=Luzz+Cacau",
  price: 300
}, {
  image: "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/royalwax_100x.png",
  name: "RoyalWax",
  link: "https://inventa.shop/collections/vendors?q=RoyalWax",
  price: 300
}];
const products = [{
  id: 1,
  title: "We Green 150g",
  image: ProductImage,
  minimal_order: 300,
  price: 30,
  discount: 20,
  minimal_quant: 5
}, {
  id: 2,
  title: "We Green 150g",
  image: ProductImage,
  minimal_order: 300,
  price: 30,
  discount: 0,
  minimal_quant: 1
}, {
  id: 3,
  title: "We Green 150g",
  image: ProductImage,
  minimal_order: 300,
  price: 30,
  discount: 20,
  minimal_quant: 1
}, {
  id: 4,
  title: "We Green 150g",
  image: ProductImage,
  minimal_order: 300,
  price: 30,
  discount: 0,
  minimal_quant: 1
}, {
  id: 5,
  title: "We Green 150g",
  image: ProductImage,
  minimal_order: 300,
  price: 30,
  discount: 0,
  minimal_quant: 1
}, {
  id: 6,
  title: "We Green 150g",
  image: ProductImage,
  minimal_order: 300,
  price: 30,
  discount: 20,
  minimal_quant: 1
}, {
  id: 7,
  title: "We Green 150g",
  image: ProductImage,
  minimal_order: 300,
  price: 30,
  discount: 40,
  minimal_quant: 1
}, {
  id: 8,
  title: "We Green 150g",
  image: ProductImage,
  minimal_order: 300,
  price: 30,
  discount: 20,
  minimal_quant: 1
}, {
  id: 9,
  title: "We Green 150g",
  image: ProductImage,
  minimal_order: 300,
  price: 30,
  discount: 20,
  minimal_quant: 1
}, {
  id: 10,
  title: "We Green 150g",
  image: ProductImage,
  minimal_order: 300,
  price: 30,
  discount: 0,
  minimal_quant: 1
}];

/***/ }),

/***/ 7331:
/***/ ((__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);



const Dropdown = ({
  children,
  closeModal
}) => {
  return /*#__PURE__*/_jsx("div", {
    children: /*#__PURE__*/_jsxs("div", {
      className: "bg-white py-10 border w-screen h-fit max-h-full overflow-y-auto fixed bottom-0 inset-x-0 rounded-t-2xl p-6 transform transition duration-200 ease-in-out z-[999]",
      children: [/*#__PURE__*/_jsx("div", {
        className: "float-right",
        children: /*#__PURE__*/_jsx("button", {
          className: "focus:outline-none",
          onClick: closeModal,
          children: /*#__PURE__*/_jsx("span", {
            className: "material-icons-outlined",
            children: "close"
          })
        })
      }), children]
    })
  });
};

/* unused harmony default export */ var __WEBPACK_DEFAULT_EXPORT__ = ((/* unused pure expression or super */ null && (Dropdown)));

/***/ }),

/***/ 9105:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Desktop)
});

// EXTERNAL MODULE: ./components/Redirect/index.js
var components_Redirect = __webpack_require__(6645);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/Breadcrumb/index.js





const Breadcrumb = ({
  steps = []
}) => {
  return /*#__PURE__*/_jsx("nav", {
    className: "mx-4 pt-4 sm:pt-0",
    children: /*#__PURE__*/_jsx("ul", {
      className: "flex flex-wrap w-full items-center gap-y-0.5 gap-x-3 text-basicDark lg:text-secondaryDark text-xs md:text-sm lg:text-lg font-lato font-normal",
      children: steps.map(({
        link,
        label
      }, index) => /*#__PURE__*/_jsx("li", {
        className: steps.length - 1 !== index ? "cursor-pointer hover:text-primaryYellow" : "",
        children: steps.length - 1 !== index ? /*#__PURE__*/_jsxs(_Fragment, {
          children: [/*#__PURE__*/_jsxs(Redirect, {
            href: link,
            children: [label, "\xA0"]
          }), /*#__PURE__*/_jsx("span", {
            children: "\xA0\xA0/"
          })]
        }) : /*#__PURE__*/_jsx("p", {
          children: label
        })
      }, index + "li-list"))
    })
  });
};

/* harmony default export */ const components_Breadcrumb = ((/* unused pure expression or super */ null && (Breadcrumb)));
;// CONCATENATED MODULE: ./components/SocialIcons/index.js




const SocialIcons = () => {
  return /*#__PURE__*/_jsxs("div", {
    className: "flex mt-1",
    children: [/*#__PURE__*/_jsx("div", {
      className: styles.icon + ' ' + styles.instagram
    }), /*#__PURE__*/_jsx("div", {
      className: styles.icon + ' ' + styles.facebook
    }), /*#__PURE__*/_jsx("div", {
      className: styles.icon + ' ' + styles.polygon
    })]
  });
};

/* harmony default export */ const components_SocialIcons = ((/* unused pure expression or super */ null && (SocialIcons)));
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(5675);
;// CONCATENATED MODULE: ./components/Landing/Header/Desktop/index.js






const FornecedorHeader = ({
  fornecedor
}) => {
  console.log(fornecedor);
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    style: {
      marginBottom: '-100px'
    },
    children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
      style: {
        backgroundImage: `url(${fornecedor.data.getNmkpBrandByBrandHandle.banner ? fornecedor.data.getNmkpBrandByBrandHandle.banner : 'https://cdn.shopify.com/s/files/1/0507/3226/9775/files/banner-brand-background.png?v=1633535674'})`,
        backgroundRepeat: "no-repeat",
        width: "100%",
        height: "300px",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        borderBottom: "6px solid #ddaa2e"
      }
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex",
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        style: {
          position: "relative",
          top: -100,
          height: "270px",
          flex: "0 0 auto",
          width: "270px",
          margin: "20px"
        },
        children: /*#__PURE__*/jsx_runtime_.jsx("div", {
          style: {
            width: "270px",
            height: "270px",
            backgroundColor: "white",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            boxShadow: "0px 5px 25px rgb(0 0 0 / 15%)"
          },
          children: /*#__PURE__*/jsx_runtime_.jsx("img", {
            src: fornecedor.data.getNmkpBrandByBrandHandle.logo
          })
        })
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        children: [/*#__PURE__*/jsx_runtime_.jsx("h2", {
          className: "text-4xl font-medium text-[#666]",
          children: fornecedor.data.getNmkpBrandByBrandHandle.brand_name
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("span", {
          className: "font-light text-base font-lato text-secondaryDark",
          children: ["M\xEDnimo R", fornecedor.data.getNmkpBrandByBrandHandle.min]
        }), /*#__PURE__*/jsx_runtime_.jsx("p", {
          className: "text-xl font-lato font-[300] text-secondaryDark",
          children: fornecedor.data.getNmkpBrandByBrandHandle.description
        })]
      })]
    })]
  });
};

/* harmony default export */ const Desktop = (FornecedorHeader);

/***/ }),

/***/ 5293:
/***/ ((__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__);




const Quantity = ({
  mobile,
  initialQuantity = 0,
  quantity: {
    min_quantity = 0,
    max_quantity = false,
    step_quantity = 1
  },
  stock_props: {
    sell_without_stock = true,
    stock
  },
  callback,
  keyIndex
}) => {
  const {
    0: keyValue,
    1: setKeyValue
  } = useState(keyIndex);
  const {
    0: init,
    1: setInit
  } = useState(initialQuantity);
  const {
    0: value,
    1: setValue
  } = useState(initialQuantity);
  let input = useRef();
  useEffect(() => {
    if (init !== initialQuantity || keyValue !== keyIndex) {
      setValue(initialQuantity);
      setInit(initialQuantity);
      setKeyValue(keyIndex);
      input.current.value = initialQuantity;
    }
  });

  const increment = () => {
    updateValue(value + step_quantity);
  };

  const decrement = () => {
    if (value > 0) {
      if (value - step_quantity < min_quantity) updateValue(0);else updateValue(value - step_quantity);
    }
  };

  const handleValue = e => {
    const val = +e.target.value;
    setValue(val);
  };

  const handleValidation = e => {
    if (e.key && e.key == "Enter" || !e.key) {
      updateValue(value);
    }

    if (e.key) {
      let validKey = false;

      switch (e.key) {
        case "Backspace":
        case "ArrowRight":
        case "ArrowLeft":
        case "Delete":
        case "Tab":
          validKey = true;
          break;
      }

      if (isNaN(e.key) && !validKey) {
        e.preventDefault();
      }
    }
  };

  const updateValue = quantity => {
    let newValue = quantity;

    if (step_quantity && quantity % step_quantity !== 0) {
      newValue = +(quantity + step_quantity - quantity % step_quantity);
    }

    if (max_quantity && newValue > max_quantity) {
      newValue = max_quantity;
    }

    if (min_quantity && newValue > 0 && newValue < min_quantity) {
      newValue = min_quantity;
    }

    if (!sell_without_stock && !isNaN(stock) && newValue > stock) {
      newValue = stock;
    }

    input.current.value = newValue;
    setValue(newValue);
    if (typeof callback === "function") callback(newValue);
  };

  useEffect(() => {
    input.current.value = value;
  }, []);
  return /*#__PURE__*/_jsxs("div", {
    className: `quantity border border-black flex items-center justify-between ${mobile ? "px-2 py-1 w-full" : "px-4 py-2 md:w-1/2 w-full"} rounded `,
    children: [/*#__PURE__*/_jsx("button", {
      className: "text-5xl  text-gray-500 ",
      onClick: decrement,
      disabled: value <= 0,
      children: "-"
    }), /*#__PURE__*/_jsx("input", {
      ref: input,
      type: "text",
      name: "quantity",
      className: "w-1/2 text-center text-2xl focus:outline-none",
      onBlur: handleValidation,
      onKeyDown: handleValidation,
      onChange: handleValue
    }), /*#__PURE__*/_jsx("button", {
      className: "text-5xl  text-gray-500 ",
      onClick: increment,
      disabled: max_quantity !== null && value >= max_quantity,
      children: "+"
    })]
  });
};

/* unused harmony default export */ var __WEBPACK_DEFAULT_EXPORT__ = ((/* unused pure expression or super */ null && (Quantity)));

/***/ }),

/***/ 2095:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "i0": () => (/* binding */ maisPura),
/* harmony export */   "bs": () => (/* binding */ bs),
/* harmony export */   "bC": () => (/* binding */ positiva),
/* harmony export */   "pT": () => (/* binding */ weNutz)
/* harmony export */ });
const maisPura = {
  "data": {
    "getNmkpBrandByBrandHandle": {
      "products_brand_page": {
        "products": [{
          "handle": "mais-pura-pipoca-artesanal-mais-pura-sabor-caramelo-coco-75g",
          "product_id": "39059a75-a041-4782-a006-97c9a44da452",
          "brand_handle": "mais-pura",
          "title": "Pipoca Artesanal Mais Pura Sabor Caramelo & Coco 75g",
          "body_html": "<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">A pipoca Caramelo &amp; Coco é feita com óleo de coco, caramelo e pedacinhos da fruta.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">SOBRE OS INGREDIENTES</strong></span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Milho de pipoca não transgênico:</strong> é um alimento fonte de vitaminas do complexo B e fibras, além disso o milho contém polifenóis, que são poderosos antioxidantes.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Coco:</strong> Rico em ácido láurico, ajuda a fortalecer o sistema imune devido sua propriedade antimicrobiana, em sua polpa também estão presentes os flavonóides que tem ação antibacteriana, antiviral e anti-inflamatória. Além de conter fibras e gorduras que melhoram a sensação de saciedade.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">O SEGREDO DOS NOSSOS PRODUTOS</strong></span></p>\n<ul data-mce-fragment=\"1\">\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não contém glúten</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Integral</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não Transgênico</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Vegano</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem lactose</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem ingredientes alergênicos</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem conservantes</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem Gordura Trans</span></li>\n</ul>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">INFORMAÇÕES NUTRICIONAIS</strong></span></p>\n<p data-mce-fragment=\"1\"><img alt=\"\" src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipocaCaramelo__Coco.jpg\" data-mce-fragment=\"1\" width=\"264\" height=\"236\" data-mce-src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipocaCaramelo__Coco.jpg\"></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">* Valores Diários de Referência com base em uma dieta de 2000 kcal ou 8400 kj. Seus valores diários podem ser maiores ou menores dependendo de suas necessidades energéticas. ** VD não estabelecido.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Ingredientes:</span></strong><span data-mce-fragment=\"1\"> milho para pipoca não transgênico integral, açúcar demerara, açúcar mascavo, glicose de milho, óleo de coco, coco ralado e emulsificante lecitina de girassol. Sem glúten, sem lactose, integral, sem gordura trans, sem conservantes, sem ingredientes de origem animal, não transgênico e sem ingredientes alergênicos.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">*Após aberto cosumir imediatamente/ Conservar em local fresco.</span></p>",
          "min_quantity": 36.0,
          "max_quantity": null,
          "step_quantity": 36.0,
          "ship_all_states": true,
          "vendor_minimum": 450.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/carameloecoco75g.jpg?v=1638921281"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/carameloecoco75g2.jpg?v=1638921281"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "mais-pura-pipoca-artesanal-mais-pura-sabor-tomate-defumado-25g",
          "product_id": "9b4b9c2b-30cb-429f-95af-b2e9837953e3",
          "brand_handle": "mais-pura",
          "title": "Pipoca Artesanal Mais Pura Sabor Tomate Defumado 25g",
          "body_html": "<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">O tempero de tomate mais crocante, delicioso e defumado.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">SOBRE OS INGREDIENTES</strong></span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Milho de pipoca não transgênico:</strong>  possui vitaminas E, A e B1, é rico em sais minerais como o fósforo, cálcio e potássio.Além disso, o milho contém antioxidantes luteína e zeaxantina que melhoram a saúde intestinal. Devido ao seu alto teor de fibras ajuda a regular o funcionamento do intestino aumentado a sensação de saciedade.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">O SEGREDO DOS NOSSOS PRODUTOS </strong></span><span data-mce-fragment=\"1\"></span></p>\n<ul data-mce-fragment=\"1\">\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não contém glúten</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Integral</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não Transgênico</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Vegano</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem lactose</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem ingredientes alergênicos</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem conservantes</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem Gordura Trans</span></li>\n</ul>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">INFORMAÇÃO NUTRICIONAL</strong></span></p>\n<p data-mce-fragment=\"1\"><img alt=\"\" src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipoca_Tomate_Defumado.jpg\" width=\"304\" height=\"272\" data-mce-fragment=\"1\" data-mce-src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipoca_Tomate_Defumado.jpg\"></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">* Valores Diários de Referência com base em uma dieta de 2000 kcal ou 8400 kj. Seus valores diários podem ser maiores ou menores dependendo de suas necessidades energéticas. ** VD não estabelecido.<br data-mce-fragment=\"1\"><br data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Ingredientes:</strong> Milho para pipoca não transgênico integral, óleo de girassol, maltodextrina, tomate em pó, aroma natural de tomate, cebola, salsa, alho, sal marinho, extrato de levedura, coloral, ácido cítrico, coentro em pó e pimenta do reino. NÃO CONTÉM GLÚTEN. Sem glúten, sem lactose, integral, sem gordura trans, sem conservantes, sem ingredientes de origem animal, não transgênico.</span></p>",
          "min_quantity": 36.0,
          "max_quantity": null,
          "step_quantity": 36.0,
          "ship_all_states": true,
          "vendor_minimum": 450.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/tomatedefumado25g.jpg?v=1638921283"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/tomatedefumado25g3.jpg?v=1638921283"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/tomatedefumado25g2.png?v=1638921283"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "mais-pura-pipoca-artesanal-mais-pura-sabor-cacau-original-150g",
          "product_id": "223382f5-9907-430c-a185-ef769a10b01b",
          "brand_handle": "mais-pura",
          "title": "Pipoca Artesanal Mais Pura Sabor Cacau Original 150g",
          "body_html": "<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">A pipoca Cacau Original é livre de lactose, como todos os nossos produtos, tem muito sabor e é rica em nutrientes do próprio cacau.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">SOBRE OS INGREDIENTES</strong></span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Milho de pipoca não transgênico:</strong> é um alimento fonte de vitaminas do complexo B e fibras, além disso o milho contém polifenóis, que são poderosos antioxidantes.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Cacau: </strong>Rico em polifenóis e flavonoides, tem poder antioxidante combatendo o envelhecimento precoce e pode auxiliar como anti-inflamatório o que ajuda a diminuir o colesterol.Também fornece triptofano, importante para produção de serotonina, proporcionando sensação de bem-estar, prazer melhorando o humor. </span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">O SEGREDO DOS NOSSOS PRODUTOS</strong></span></p>\n<ul data-mce-fragment=\"1\">\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não contém glúten</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Integral</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não Transgênico</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Vegano</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem lactose</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem ingredientes alergênicos</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem conservantes</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem Gordura Trans</span></li>\n</ul>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">INFORMAÇÕES NUTRICIONAIS</strong> </span></p>\n<p data-mce-fragment=\"1\"><img alt=\"\" src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipoca_Cacau_Original.jpg\" data-mce-fragment=\"1\" width=\"306\" height=\"274\" data-mce-src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipoca_Cacau_Original.jpg\"></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">* Valores Diários de Referência com base em uma dieta de 2000 kcal ou 8400 kj. Seus valores diários podem ser maiores ou menores dependendo de suas necessidades energéticas. ** VD não estabelecido. </span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Ingredientes:</strong></span><span data-mce-fragment=\"1\"> milho para pipoca não transgênico integral, açúcar demerara, açúcar mascavo, glicose de milho, cacau em pó, óleo de girassol, emulsificante lecitina de girassol. Sem glúten, sem lactose, integral, sem gordura trans, sem conservantes, sem ingredientes de origem animal, não transgênico e sem ingredientes alergênicos.</span></p>",
          "min_quantity": 24.0,
          "max_quantity": null,
          "step_quantity": 24.0,
          "ship_all_states": true,
          "vendor_minimum": 450.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/cacauoriginal150g.jpg?v=1638921272"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/cacauoriginal150g2.jpg?v=1638921272"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/cacauoriginal150g3.png?v=1638921272"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "mais-pura-pipoca-artesanal-mais-pura-sabor-caramelo-e-flor-de-sal-75g",
          "product_id": "993987b2-7e60-49d8-94dd-61fb1cf52952",
          "brand_handle": "mais-pura",
          "title": "Pipoca Artesanal Mais Pura Sabor Caramelo e Flor de Sal 75g",
          "body_html": "<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Docemente salgado: o contraste perfeito entre o açúcar caramelizado e o sabor suave da flor-de-sal.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">SOBRE OS INGREDIENTES </strong></span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Milho de pipoca não transgênico: </strong>é um alimento fonte de vitaminas do complexo B e fibras, além disso o milho contém polifenóis, que são poderosos antioxidantes.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Açúcar mascavo:</strong> não passa por processo de refinamento, o que mantém as suas propriedades nutricionais. É rico em cálcio, magnésio, fósforo e potássio;</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Flor de sal:</strong> não passa por nenhum processo posterior à sua retirada do mar, mantendo seus nutrientes, tais como o ferro, zinco, iodo, magnésio, flúor, sódio, potássio, cálcio e cobre.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">O SEGREDO DOS NOSSO PRODUTOS</strong></span></p>\n<ul data-mce-fragment=\"1\">\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não contém glúten</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Integral</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não Transgênico</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Vegano</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem lactose</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem ingredientes alergênicos</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem conservantes</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem Gordura Trans</span></li>\n</ul>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">INFORMAÇÕES NUTRICIONAIS</strong></span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\"><img alt=\"\" src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipocaCaramelo__Flor_de_Sal.jpg\" data-mce-fragment=\"1\" width=\"308\" height=\"276\" data-mce-src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipocaCaramelo__Flor_de_Sal.jpg\"></strong></span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">* Valores Diários de Referência com base em uma dieta de 2000 kcal ou 8400 kj. Seus valores diários podem ser maiores ou menores dependendo de suas necessidades energéticas. ** VD não estabelecido.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Ingredientes: </strong>Milho para pipoca não transgênico integral, açúcar mascavo, glicose de milho, açúcar cristal, óleo de girassol, flor de sal, emulsificante lecitina de girassol.  Sem glúten, sem lactose, integral, sem gordura trans, sem conservantes, sem ingredientes de origem animal, não transgênico.</span></p>",
          "min_quantity": 36.0,
          "max_quantity": null,
          "step_quantity": 36.0,
          "ship_all_states": true,
          "vendor_minimum": 450.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/carameloeflordesal75g.png?v=1638921272"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/carameloeflordesal75g2.jpg?v=1638921272"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/carameloeflordesal2_3a924e17-8b87-433e-8bfe-af48e58baf9a.png?v=1638921272"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "mais-pura-pipoca-artesanal-mais-pura-sabor-tomate-defumado-50g",
          "product_id": "b12405cf-8356-4016-8d28-5fd0b1f28f13",
          "brand_handle": "mais-pura",
          "title": "Pipoca Artesanal Mais Pura Sabor Tomate Defumado 50g",
          "body_html": "<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">O tempero de tomate mais crocante, delicioso e defumado.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">SOBRE OS INGREDIENTES</strong></span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Milho de pipoca não transgênico:</strong>  possui vitaminas E, A e B1, é rico em sais minerais como o fósforo, cálcio e potássio.Além disso, o milho contém antioxidantes luteína e zeaxantina que melhoram a saúde intestinal. Devido ao seu alto teor de fibras ajuda a regular o funcionamento do intestino aumentado a sensação de saciedade.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">O SEGREDO DOS NOSSOS PRODUTOS </strong></span><span data-mce-fragment=\"1\"></span></p>\n<ul data-mce-fragment=\"1\">\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não contém glúten</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Integral</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não Transgênico</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Vegano</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem lactose</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem ingredientes alergênicos</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem conservantes</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem Gordura Trans</span></li>\n</ul>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">INFORMAÇÃO NUTRICIONAL</strong></span></p>\n<p data-mce-fragment=\"1\"><img alt=\"\" src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipoca_Tomate_Defumado.jpg\" width=\"289\" height=\"259\" data-mce-fragment=\"1\" data-mce-src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipoca_Tomate_Defumado.jpg\"></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">* Valores Diários de Referência com base em uma dieta de 2000 kcal ou 8400 kj. Seus valores diários podem ser maiores ou menores dependendo de suas necessidades energéticas. ** VD não estabelecido.<br data-mce-fragment=\"1\"><br data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Ingredientes:</strong> Milho para pipoca não transgênico integral, óleo de girassol, maltodextrina, tomate em pó, aroma natural de tomate, cebola, salsa, alho, sal marinho, extrato de levedura, coloral, ácido cítrico, coentro em pó e pimenta do reino. NÃO CONTÉM GLÚTEN. Sem glúten, sem lactose, integral, sem gordura trans, sem conservantes, sem ingredientes de origem animal, não transgênico.</span></p>",
          "min_quantity": 24.0,
          "max_quantity": null,
          "step_quantity": 24.0,
          "ship_all_states": true,
          "vendor_minimum": 450.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/tomatedefumado50g.jpg?v=1638921260"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/tomatedefumado50g3.jpg?v=1638921260"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/tomatedefumado50g2.png?v=1638921260"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "mais-pura-pipoca-artesanal-mais-pura-sabor-banoffee",
          "product_id": "eabef15a-2271-4cf0-bd8e-72801c4e790a",
          "brand_handle": "mais-pura",
          "title": "Pipoca Artesanal Maïs Pura Sabor Banoffee 120g",
          "body_html": "<div data-mce-fragment=\"1\">\n<div data-mce-fragment=\"1\">\n<span data-mce-fragment=\"1\">Surpreenda-se com essa novidade! </span><span data-mce-fragment=\"1\">A pipoca Maïs amada do Brasil com gostinho de Banoffee.</span>\n</div>\n<div data-mce-fragment=\"1\"> </div>\n<div data-mce-fragment=\"1\">\n<span data-mce-fragment=\"1\">PIPOCA, </span><span data-mce-fragment=\"1\">CARAMELO, </span><span data-mce-fragment=\"1\">BANANA, TOQUE DE CANELA </span><span data-mce-fragment=\"1\">&amp; HUUUUM.</span>\n</div>\n</div>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">SOBRE OS INGREDIENTES </strong></span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Milho de pipoca não transgênico: </strong>é um alimento fonte de vitaminas do complexo B e fibras, além disso o milho contém polifenóis, que são poderosos antioxidantes.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Açúcar mascavo:</strong> não passa por processo de refinamento, o que mantém as suas propriedades nutricionais. É rico em cálcio, magnésio, fósforo e potássio;</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">O SEGREDO DOS NOSSO PRODUTOS</strong></span></p>\n<ul data-mce-fragment=\"1\">\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não contém glúten</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Integral</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não Transgênico</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Vegano</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem lactose</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem ingredientes alergênicos</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem conservantes</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem Gordura Trans</span></li>\n</ul>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">INFORMAÇÕES NUTRICIONAIS</strong></span></p>\n<p data-mce-fragment=\"1\"><img alt=\"\" src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelanutricionailbanoffee.png\" data-mce-fragment=\"1\" width=\"330\" height=\"296\" data-mce-src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelanutricionailbanoffee.png\"></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">* Valores Diários de Referência com base em uma dieta de 2000 kcal ou 8400 kj. Seus valores diários podem ser maiores ou menores dependendo de suas necessidades energéticas. ** VD não estabelecido.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Ingredientes:</strong> Milho para pipoca não transgênico integral, açúcar demerara, glicose de milho, açúcar mascavo, óleo de girassol, canela em pó, banana em pó (polpa de banana e fibra alimentar), emulsificante lecitina de girassol, aromatizante natural. </span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem glúten, sem lactose, integral, sem gordura trans, sem conservantes, sem ingredientes de origem animal, não transgênico.</span></p>",
          "min_quantity": 30.0,
          "max_quantity": null,
          "step_quantity": 30.0,
          "ship_all_states": true,
          "vendor_minimum": 400.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/banoffee.png?v=1644944633"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "mais-pura-pipoca-artesanal-mais-pura-sabor-cacau-zero-65g",
          "product_id": "4e32751e-00a6-4546-9b94-45ea1b88921d",
          "brand_handle": "mais-pura",
          "title": "Pipoca Artesanal Mais Pura Sabor Cacau Zero 65g",
          "body_html": "<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Para os que não vivem sem a pipoca mais amada do Brasil, apresentamos o novo sabor: Maïs Pura Cacau Zero.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">MAÏS PURA Cacau Zero: pipoca artesanal sabor cacau, sem adição de açúcar e deliciosamente irresistível!</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">SOBRE OS INGREDIENTES </strong></span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Milho de pipoca não transgênico:</strong> é um alimento fonte de vitaminas do complexo B e fibras, além disso o milho contém polifenóis, que são poderosos antioxidantes.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Cacau: </strong>rico em polifenóis e flavonoides, tem poder antioxidante combatendo o envelhecimento precoce e pode auxiliar como anti-inflamatório o que ajuda a diminuir o colesterol. Também fornece triptofano, importante para produção de serotonina, proporcionando sensação de bem-estar, prazer melhorando o humor. </span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Glicosídeo de esteviol</strong>: a Stévia é uma planta nativa da América do Sul. A folha de stévia possui o adoçante glicosídeo de esteviol. Esse adoçante possui alto grau de pureza.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Isomalte: </strong>é um tipo de adoçante derivado do açúcar. Entre os seus benefícios, se destacam: menor valor calórico, ação anticariogênica e baixo índice glicêmico (não aumentar os níveis de glicose e insulina no sangue).</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Polidextrose: </strong>tem propriedades prebióticas. Suas principais características são os efeitos benéficos à saúde intestinal, além de saciedade, modulação da função imunológica e auxílio no controle de glicose e colesterol.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><img alt=\"\" src=\"https://images.tcdn.com.br/img/editor/up/654930/icones.png\" data-mce-fragment=\"1\" width=\"388\" height=\"78\" data-mce-src=\"https://images.tcdn.com.br/img/editor/up/654930/icones.png\"></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">O SEGREDO DOS NOSSOS PRODUTOS</strong></span></p>\n<ul data-mce-fragment=\"1\">\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não contém glúten</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Integral</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não Transgênico</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Vegano</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem lactose</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem ingredientes alergênicos</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem conservantes</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem Gordura Trans</span></li>\n</ul>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">INFORMAÇÕES NUTRICIONAIS</strong> </span></p>\n<p data-mce-fragment=\"1\"><img alt=\"\" src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipocacacau_zero.jpg\" data-mce-fragment=\"1\" width=\"265\" height=\"238\" data-mce-src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipocacacau_zero.jpg\"></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">* Valores Diários de Referência com base em uma dieta de 2000 kcal ou 8400 kj. Seus valores diários podem ser maiores ou menores dependendo de suas necessidades energéticas. ** VD não estabelecido. </span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Ingredientes</strong></span><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">:</strong> milho para pipoca não transgênico integral, polidextrose (fibra alimentar), cacau em pó, óleo de girassol, sal, isomalte, emulsificante lecitina de girassol, aromatizante edulcorante glicosídeo de esteviól (stévia)</span></p>",
          "min_quantity": 48.0,
          "max_quantity": null,
          "step_quantity": 48.0,
          "ship_all_states": true,
          "vendor_minimum": 450.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/cacauzero65g.png?v=1638921292"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/cacauzero65g2.png?v=1638921292"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "mais-pura-pipoca-artesanal-mais-pura-sabor-caramelo-coco-150g",
          "product_id": "b8e1673d-da2e-4200-ad56-3bae0030a161",
          "brand_handle": "mais-pura",
          "title": "Pipoca Artesanal Mais Pura Sabor Caramelo & Coco 150g",
          "body_html": "<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">A pipoca Caramelo &amp; Coco é feita com óleo de coco, caramelo e pedacinhos da fruta.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">SOBRE OS INGREDIENTES</strong></span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Milho de pipoca não transgênico:</strong> é um alimento fonte de vitaminas do complexo B e fibras, além disso o milho contém polifenóis, que são poderosos antioxidantes.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Coco:</strong> Rico em ácido láurico, ajuda a fortalecer o sistema imune devido sua propriedade antimicrobiana, em sua polpa também estão presentes os flavonóides que tem ação antibacteriana, antiviral e anti-inflamatória. Além de conter fibras e gorduras que melhoram a sensação de saciedade.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">O SEGREDO DOS NOSSOS PRODUTOS</strong></span></p>\n<ul data-mce-fragment=\"1\">\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não contém glúten</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Integral</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não Transgênico</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Vegano</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem lactose</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem ingredientes alergênicos</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem conservantes</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem Gordura Trans</span></li>\n</ul>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">INFORMAÇÕES NUTRICIONAIS</strong></span></p>\n<p data-mce-fragment=\"1\"><img alt=\"\" src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipocaCaramelo__Coco.jpg\" data-mce-fragment=\"1\" width=\"289\" height=\"259\" data-mce-src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipocaCaramelo__Coco.jpg\"></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">* Valores Diários de Referência com base em uma dieta de 2000 kcal ou 8400 kj. Seus valores diários podem ser maiores ou menores dependendo de suas necessidades energéticas. ** VD não estabelecido.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Ingredientes:</span></strong><span data-mce-fragment=\"1\"> milho para pipoca não transgênico integral, açúcar demerara, açúcar mascavo, glicose de milho, óleo de coco, coco ralado e emulsificante lecitina de girassol. Sem glúten, sem lactose, integral, sem gordura trans, sem conservantes, sem ingredientes de origem animal, não transgênico e sem ingredientes alergênicos.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">*Após aberto cosumir imediatamente/ Conservar em local fresco.</span></p>",
          "min_quantity": 24.0,
          "max_quantity": null,
          "step_quantity": 24.0,
          "ship_all_states": true,
          "vendor_minimum": 450.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/carameloecoco150g1.png?v=1638921268"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/carameloecoco150g2.png?v=1638921268"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "mais-pura-pipoca-artesanal-mais-pura-sabor-cacau-original-75g",
          "product_id": "ddf3a19c-9cc7-4dcf-a58a-04483824b3cb",
          "brand_handle": "mais-pura",
          "title": "Pipoca Artesanal Mais Pura Sabor Cacau Original 75g",
          "body_html": "<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">A pipoca Cacau Original é livre de lactose, como todos os nossos produtos, tem muito sabor e é rica em nutrientes do próprio cacau.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">SOBRE OS INGREDIENTES</strong></span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Milho de pipoca não transgênico:</strong> é um alimento fonte de vitaminas do complexo B e fibras, além disso o milho contém polifenóis, que são poderosos antioxidantes.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Cacau: </strong>Rico em polifenóis e flavonoides, tem poder antioxidante combatendo o envelhecimento precoce e pode auxiliar como anti-inflamatório o que ajuda a diminuir o colesterol.Também fornece triptofano, importante para produção de serotonina, proporcionando sensação de bem-estar, prazer melhorando o humor. </span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">O SEGREDO DOS NOSSOS PRODUTOS</strong></span></p>\n<ul data-mce-fragment=\"1\">\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não contém glúten</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Integral</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não Transgênico</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Vegano</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem lactose</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem ingredientes alergênicos</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem conservantes</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem Gordura Trans</span></li>\n</ul>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">INFORMAÇÕES NUTRICIONAIS</strong> </span></p>\n<p data-mce-fragment=\"1\"><img alt=\"\" src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipoca_Cacau_Original.jpg\" data-mce-fragment=\"1\" width=\"264\" height=\"237\" data-mce-src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipoca_Cacau_Original.jpg\"></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">* Valores Diários de Referência com base em uma dieta de 2000 kcal ou 8400 kj. Seus valores diários podem ser maiores ou menores dependendo de suas necessidades energéticas. ** VD não estabelecido. </span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Ingredientes:</strong></span><span data-mce-fragment=\"1\"> milho para pipoca não transgênico integral, açúcar demerara, açúcar mascavo, glicose de milho, cacau em pó, óleo de girassol, emulsificante lecitina de girassol. Sem glúten, sem lactose, integral, sem gordura trans, sem conservantes, sem ingredientes de origem animal, não transgênico e sem ingredientes alergênicos.</span></p>",
          "min_quantity": 36.0,
          "max_quantity": null,
          "step_quantity": 36.0,
          "ship_all_states": true,
          "vendor_minimum": 450.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/cacauoriginal75g.jpg?v=1638921284"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/cacauoriginal75g2.jpg?v=1638921284"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/cacauoriginal150g3_051b1eaa-4281-4ce9-9ead-ce2f1657b538.png?v=1638921284"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "mais-pura-pipoca-artesanal-mais-pura-sabor-lemon-pepper-50g",
          "product_id": "431f6730-0c6f-4921-af66-6dc08e15ff5f",
          "brand_handle": "mais-pura",
          "title": "Pipoca Artesanal Mais Pura Sabor Lemon Pepper 50g",
          "body_html": "<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">A combinação perfeita entre o frescor do limão e a ardência delicada da pimenta-do-reino. </span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">SOBRE OS INGREDIENTES</strong></span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Milho de pipoca não transgênico:</strong>  possui vitaminas E, A e B1, é rico em sais minerais como o fósforo, cálcio e potássio.  </span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Além disso, o milho contém antioxidantes luteína e zeaxantina que melhoram a saúde intestinal. </span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Devido ao seu alto teor de fibras ajuda a regular o funcionamento do intestino aumentado a sensação de saciedade.</span></p>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">O SEGREDO DOS NOSSOS PRODUTOS</strong></span></p>\n<ul data-mce-fragment=\"1\">\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não contém glúten</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Integral</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não Transgênico</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Vegano</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem lactose</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem ingredientes alergênicos</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem conservantes</span></li>\n<li data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Sem Gordura Trans    </span></li>\n</ul>\n<p data-mce-fragment=\"1\"> </p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">INFORMAÇÕES NUTRICIONAIS</strong></span></p>\n<p data-mce-fragment=\"1\"><img height=\"228\" width=\"255\" data-mce-fragment=\"1\" src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipocaLemon_Pepper.jpg\" alt=\"\" data-mce-src=\"https://images.tcdn.com.br/img/editor/up/654930/tabelaPipocaLemon_Pepper.jpg\"></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">* Valores Diários de Referência com base em uma dieta de 2000 kcal ou 8400 kj. Seus valores diários podem ser maiores ou menores dependendo de suas necessidades energéticas. ** VD não estabelecido.</span></p>\n<p data-mce-fragment=\"1\"><span data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Ingredientes:</strong> Milho para pipoca não transgênico integral, óleo de girassol, lemon pepper, sal marinho. Sem glúten, sem lactose, integral, sem gordura trans, sem conservantes, sem ingredientes de origem animal, não transgênico.</span></p>",
          "min_quantity": 24.0,
          "max_quantity": null,
          "step_quantity": 24.0,
          "ship_all_states": true,
          "vendor_minimum": 450.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/lemonpepper50g.jpg?v=1638921257"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/lemonpepper50g1.jpg?v=1638921257"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/lemonpepper50g3.png?v=1638921257"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }],
        "next_token": "eyJ2ZXJzaW9uIjoyLCJ0b2tlbiI6IkFRSUNBSGg5OUIvN3BjWU41eE96NDZJMW5GeGM4WUNGeG1acmFOMUpqajZLWkFDQ25BRTZzU0wwMm9TZWRiakxVNlZIK0phY0FBQURoRENDQTRBR0NTcUdTSWIzRFFFSEJxQ0NBM0V3Z2dOdEFnRUFNSUlEWmdZSktvWklodmNOQVFjQk1CNEdDV0NHU0FGbEF3UUJMakFSQkF4ZHhZbjJYNzd6VXAzTXZiNENBUkNBZ2dNMzNIZXd0SFRab1lkeE5Rc2NDRE9WZVlvQ1JQOUhLdzA5ZHZJZGY1dWwrN3lMSUdIcUpMemF4M0hUREFpdnRyOStyemtsVWFxTTkzRllHb2ViS0R6dWN2S3VkYUZqczRnNmk3ODJnc0wrZC9GeWNyYzdwVU1CVzZ0bXlDcno3emcva283YVBFLzRtTUpuTnNIRGpUM1ZNZTFLZWJGOFFqQm1VRk9UMWs2YjBoTTA2ajZJTk5BdkVRR3BnKytBdlNWV09QVEFnenhLT3poYjljN1Q2alFsYkZwQmRzQTBRT29GemEvbTd4OXAyTHd2SEtxOVpIdUlYMC9rWi9lQWFYd2hSWXpRNTdnTlhycmt5MkVuL3FzSmtKQTFwL0hFUWRUZkd5dGdzYnNRWnRDMFVmOHNuZnNrYWV4dlE3T3IrRkpyWndNYUJkb28zZm0zVEM1UGhNbk91anRWUk9pODFnZEN4MVRnbUlkWnRHZnpDREROY2h3dTkrdDB1NVo5Y240MkgzMHh1MmVFa1NXZW9KWDE1Qi9zUXhqNHBHRG9ENzFTeWpodXZaRThoMEFwcU45WlJGdHlPa0JQZGtKcXRJcXlLNTBKTTJYcUovVURsdXYwNm9mZVg5bkRTQVpPNi8vazNXQVJLVzdzNnF5S2I3SXBudGVwcklFRDhydlhoUXhQWnBVbkE2VldZNDZYMXZ0eHhtQVBpbkdxQkxvV0kzb1M4OS8xQXgyOXZ4MUxGTUNncWZDdDgxdW9aNllnOWl1WWdTYWl4Q041U2tIRW5zbXRzQUxUNDZwdkw5RFhSQm5jQXNtWEt3MG9EUUN0aUp6cGErUUVXbG5pVEl0ZS93UTU0RFlJZTRBT0EvdVZwaE1FamJpcC9IVEFMMFp0WElYeWs2S0JaV1pXQXJZZjhLSVgxaWNtSkVqbnpUZHZ0M0YvdTZDajIzVzJVR1Rjb2l4Ym5UNUI5Nm5ad3czS3BvYTBnSWlRL0kzMm5aL1hNbUlkQWt2c3dScElGdmlyVXR4VHl5ejl0WEorTnZCRzRkSllrTGUyRHNaQUEzeklmZFdIdmRuVy91MjlZY3Y4bzFBNmhxeU1iNkltWmZJaE55T0h5NHZqeEZPcHoyYUJ6QjFGWkpXQXlSWFN3ejhmVVp1NWNTU1p1S2l4WHpKdVJsNzIrbmxPeElFS1RMZUpXRVBpbnJtdnc0eWdER2NYYWFtQTd1MGc3dUw0cW81bFltOTBNZDFOVFNWR2hSdnJQL0xXYkNYaU5waTZ5SzZla3FtRWo5WjBocEg1RnIydytmQ2k3ZzBhNjhoWkh0M2xrVmFHeTU0T3BxL28rZUNWVFhFc2ZHcFN6MUVnOEhBcU1ieXJhM3NkNW1rclVqcG1DWXdkU3F1ekZ3R1MzRFdWbFBpb3hXejN6bjlUbnQ2WnRLa21uaTBMbUd6cVQ2eFZQUjl2VHBwN1o5K1hzZz09In0="
      },
      "banner": "https://s3.amazonaws.com/lp.public.images/banner-mais-pura.png",
      "brand_handle": "mais-pura",
      "brand_id": "db6b115c-5462-4b6a-88fd-da10cf2c414c",
      "brand_name": "Mais Pura",
      "cnpj": "21475442000164",
      "min": 450,
      "days_to_ship": 2.0,
      "description": "O segredo dos nossos produtos \nfeitos com ingredientes especiais e de maneira artesanal, nossos\nsnacks são a opção rápida e saudável para qualquer momento do dia.\nA textura crocante e nos livramos dos corantes e conservantes pensando\nem você, que se preocupa com a saúde, mas não abre mão dos pequenos \nprazeres da vida.\n\nNão contém glúten, sem lactose, integral, não transgênico, sem ingredientes \nalergênicos, vegano.",
      "index": null,
      "limit": null,
      "logo": "https://fornecedor.inventa.app.br/supplier/image/e23c076ccc43333242bb42c7ed3f5582.jpeg?v=1642020338170",
      "products_quantity": 15.0,
      "vendor_id": "ea670788-cb6d-426d-b0f0-db325d7ba33b",
      "websites": [{
        "type": "Website",
        "url": "https://www.maispura.ind.br/"
      }]
    }
  }
};
const bs = {
  "data": {
    "getNmkpBrandByBrandHandle": {
      "products_brand_page": {
        "products": [{
          "handle": "bs-co-copia-de-kit-escova-de-dentes-the-humble-mini-pasta-fresh-mint-7g-the-humble",
          "product_id": "06dcd332-f46e-4da5-9949-194db45cf953",
          "brand_handle": "bs-co",
          "title": "Kit Escova de Dentes Adulto de Amido de Milho + Mini Pasta Fresh Mint 7g The Humble",
          "body_html": "<p>O Kit Escova de dentes The Humble + Mini pasta Fesh Mint 7g The Humble é ideal para a higiene bucal, com um sabor de menta gelada proporciona além da limpeza dos dentes, um hálito fresco e agradável.<br><br>• Produto Natural e Vegano<br>• Kit perfeito para iniciar sua higiene bucal com produtos mais naturais e veganos<br>• Escova sustentável de bambu e replantável <br>• Mini Pasta de dente super refrescante e natural<br>• Contém: 1 Escova de Bambu The Humble + Mini pasta Fresh Mint 7g The humble</p>\n<p>Projetos Humble <br>Seja Humble, dê sorrisos.</p>\n<p>A Humble Smile Foundation gerencia projetos de extensão em saúde bucal para ajudar a <br>manter sorrisos nos rostos das crianças que vivem nas áreas mais vulneráveis do mundo.</p>\n<p>- Biodegradável <br>- Sustentável <br>- Cruelty-free (Não testado em animais) <br>- Vegana <br>- Proveniente de plantação sustentável com selo FSC <br>- Cerdas DuPont</p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 500.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/Amido.jpg?v=1631912732"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "bs-co-fio-dental-organico-charcoal-50m",
          "product_id": "7a0ebdde-f2e7-4dc4-bced-cbb971735f88",
          "brand_handle": "bs-co",
          "title": "Fio Dental Orgânico Charcoal 50M",
          "body_html": "<p>O Fio Dental Vegano e Ecológico de Carvão da The Humble faz parte de uma rotina diária importante para alcançar aqueles locais de difícil acesso entre os dentes, para evitar cáries, doenças gengivais e o tártaro.</p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 500.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": "Fio Dental Orgânico Charcoal 50M",
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/FiodentalCarvao-Bioute_700x_85b85a63-f13c-486e-ad68-b49b7839f4fa.jpg?v=1626266635"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "bs-co-fio-dental-organico-fresh-the-humble-50m",
          "product_id": "1292e1b8-9889-4ad0-84a6-3c8a5abfbce4",
          "brand_handle": "bs-co",
          "title": "Fio Dental Orgânico Fresh The Humble 50M",
          "body_html": "<p><span data-mce-fragment=\"1\">O </span><strong data-mce-fragment=\"1\">Fio Dental Orgânico Fresh The Humble</strong><span data-mce-fragment=\"1\"> faz parte de uma rotina diária importante para alcançar aqueles locais de difícil acesso entre os dentes, para evitar cáries, doenças gengivais e o tártaro.</span><br data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Ele é forte e durável, com sabor de menta, para garantir que sua experiência com o fio dental seja a mais eficiente, agradável e eficaz possível.</span><br data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\"></strong></p>\n<p><strong data-mce-fragment=\"1\">MODO DE USO</strong><span data-mce-fragment=\"1\">:</span><br data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Use aproximadamente 30 cm do Fio Dental Vegano e Ecológico da The Humble, deixando um espaço de fio entre os dedos.</span><br data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Com cuidado, passe nas curvas dos dentes.</span><br data-mce-fragment=\"1\"><br data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">Benefícios</strong><span data-mce-fragment=\"1\">:</span><br data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Não testado em animais.</span><br data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Produto Vegano.</span><br data-mce-fragment=\"1\"><span data-mce-fragment=\"1\">Natural</span></p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 500.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/fio-dental-natural-e-organico-fresh-the-humble-bioute_650x_d66e53da-4fc8-4c10-ade0-585fde7194e0.png?v=1628874921"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "bs-co-fio-dental-organico-lemon-50m",
          "product_id": "b66eb129-1018-4d97-b31f-c2f8282bdccb",
          "brand_handle": "bs-co",
          "title": "Fio Dental Orgânico Lemon 50M",
          "body_html": "<p>O Fio Dental Vegano e Ecológico de Limão da The Humble faz parte de uma rotina diária importante para alcançar aqueles locais de difícil acesso entre os dentes, para evitar cáries, doenças gengivais e o tártaro.</p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 500.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": "Fio Dental Orgânico Lemon 50M",
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/FiodentalLemon-Bioute_700x_96fc295a-c659-464e-ac1c-e85fb17976f4.jpg?v=1626266637"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "bs-co-fio-dental-organico-cinnamon-50m",
          "product_id": "d3c509f9-3670-431e-8c13-21689a0c8eb3",
          "brand_handle": "bs-co",
          "title": "Fio Dental Orgânico Cinnamon 50M",
          "body_html": "<p>O Fio Dental Vegano e Ecológico de Canela da The Humble faz parte de uma rotina diária importante para alcançar aqueles locais de difícil acesso entre os dentes, para evitar cáries, doenças gengivais e o tártaro.</p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 500.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": "Fio Dental Orgânico Cinnamon 50M",
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/FiodentalCinnamon-Bioute_700x_0ab11feb-4110-40c3-a8cb-aa830b6fa835.jpg?v=1626266648"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "bs-co-roll-on-deodorant-active-citrus-e-vergamot-88ml",
          "product_id": "70ba14a8-1889-49ef-8dab-219a702225b7",
          "brand_handle": "bs-co",
          "title": "Roll-On Active Vegano 88ml  ",
          "body_html": "<p>O Desodorante Natural Roll-On Active Lafe's é feito para suportar até mesmo os estilos de vida mais ativos e estressantes, usando óleos essenciais botânicos naturais para combater as bactérias causadoras de odor sem o uso de produtos químicos nocivos. Nosso desodorante 100% natural foi reconhecido pela eficácia de até 24 horas. A Aloe Vera amacia e hidrata a pele e tem resultado bactericida que ajuda a manter a pele fresca e saudável o dia inteiro. A hamamélis é um adstringente natural e bactericida ajudando a manter a pele balanceada naturalmente. O óleo de coentro reduz o suor, age contra o odor e inibe o aparecimento de fungos, ajudando a curar infecções por fungos. <br>Com 72% de ingredientes orgânicos certificados, o Desodorante Roll-On Active Lafe's não tem parabenos, propilenoglicol, álcool ou cloridrato de alumínio, sem testes em animais e oferece uma proteção natural contra as bactérias que causam odor, com um aroma botânico refrescante. <br> <br>Não é antiperspirante e nem antitranspirante, portanto, não obstruirá os poros. Apenas oferece uma proteção natural. <br> <br>Suave na pele: feito para uma proteção duradoura por 24 horas, o Desodorante Roll-On Active Lafe's contém uma combinação de óleos essenciais para proporcionar uma proteção desodorante suave e eficaz. <br> <br>Modo de Uso: Aplicar por toda extensão das axilas e reaplicar se necessário. <br><br> Proteção 24 horas<br> Para pessoas ativas, um aroma agradável de óleos essenciais botânicos que combate bactérias causadoras de odor <br> Livre de glúten <br> Livre de Proplyenoglicol <br>Sem OGM - Organismo Geneticamente Modificado Composição: Aqua (Água), Potassium Alum (Sais Minerais), Cyamopsis Tetragonoloba* (Goma Guar), Aloe Barbadensis* (Aloe Vera) Leaf Extract, Hissopus Officinalis* (hissopo) Extract, Coriadrum Sativum (Coentro) Seed Oil, Hamamelis Virginiana (Bruxa Avelã) Extract, Eugenia Caryophyllus* (Clove Bud) Essential Oil, Citrus Reticulata, Citrus Arantium Sub. Bergamia (Bergamota), Jasmin Gladiforum (Jasmine), Vetiveria Zizanoid (Vetiver), Glucose &amp; Glucose Oxidase &amp; Lactoperoxidase.</p>\n<p>*Certificado Orgânico. <br><br></p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 500.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/AC.jpg?v=1626266690"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "bs-co-roll-on-deodorant-fresh-cedar-e-aloe-88ml",
          "product_id": "2ce75ef0-44cb-4e41-85d3-77ece155b584",
          "brand_handle": "bs-co",
          "title": "Roll-On Fresh Vegano 88ml ",
          "body_html": "<p>O Desodorante Natural Roll-On Fresh Lafe's deixa o cheiro como você tivesse acabado de sair do chuveiro com um aroma fresco! Este desodorante orgânico mantém-se fresco e limpo ao longo do dia. <br>Nossa mistura especial de ingredientes ativos e óleos essenciais combatem as bactérias causadoras do odor. <br>Esta fórmula eficaz de desodorante natural e roll-on tem uma fórmula forte o suficiente para suportar até mesmo os estilos de vida mais ativos e estressantes. A Lafes foi homenageada com vários prêmios, incluindo Natural Solutions \"Beauty with a Conscience Award\" dois anos seguidos. <br>A Aloe Vera amacia e hidrata a pele e tem resultado bactericida que ajuda a manter a pele fresca e saudável o dia inteiro. A hamamélis é um adstringente natural e bactericida que auxilia na manutenção natural da pele. O óleo de coentro reduz o suor, age contra o odor e inibe o aparecimento de fungos, ajudando a curar infecções por fungos. <br> <br>Com 72% de ingredientes orgânicos certificados, o Desodorante Roll-On Fresh Lafe's não tem parabenos, propilenoglicol, álcool ou alumínio, sem testes em animais e oferece uma proteção natural contra as bactérias que causam odor, e mantém o frescor depois do banho. <br> <br>Não é antiperspirante e nem antitranspirante, portanto, não obstruirá os poros. Apenas oferece uma proteção natural. <br> <br>Proteção contra odor: o Desodorante Roll-On Fresh Lafe's elimina as bactérias que causam o odor, com as propriedades bacterianas de sais minerais naturais, oferecendo um aroma que mantém o frescor do banho, ingredientes ativos, e óleos essenciais aromáticos. <br> <br>Suave na pele: feito para uma proteção duradoura por 24 horas, o Desodorante Roll-On Fresh Lafe's contém uma mistura especial de ingredientes ativos e óleos essenciais que combatem as bactérias. <br> <br>Modo de Uso: Aplicar por toda extensão das axilas e reaplicar se necessário. <br> <br> Proteção 24 horas <br> Mantem o frescor depois do banho, com uma mistura especial de ingredientes ativos e óleos essenciais que combatem outras bactérias <br> Livre de glúten <br> Livre de Proplyenoglicol <br>Sem OGM - Organismo Geneticamente Modificado <br> <br>Composição: Aqua (Água), Potassium Alum (Sais Minerais), Cyamopsis Tetragonoloba* (Goma Guar), Aloe Barbadensis* (Aloe Vera) Leaf Extract, Salvia Officinalis* (Sálvia) Extract, Coriadrum Sativum (Coentro) Seed Oil, Hamamelis Virginiana (Bruxa Avelã) Extract, Eugenia Caryophyllus* (Clove Bud) Essential Oil, Eletteria Cardamomum (Cardamomo), Citrus Arantifolia (Lima), Root Iris Florentina (Raiz de Orris), Cedrus Libani (Cedro), Vanilla Planifolia (Baunilha), Glucose &amp; Glucose Oxidase &amp; Lactoperoxidase.</p>\n<p>*Certificado Orgânico.<br></p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 500.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/FF.jpg?v=1626266710"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "bs-co-roll-on-deodorant-extra-strength-coriand-88ml",
          "product_id": "fa065cd4-1dc0-40a4-b751-35883419d87a",
          "brand_handle": "bs-co",
          "title": "Roll-On Extra Strength Vegano 88ml",
          "body_html": "<p>O Desodorante Natural Roll-on Extra Strenght Lafe's é feito com óleo essencial de melaleuca que combate as bactérias causadoras do odor sem o uso de produtos químicos nocivos. Os desodorantes naturais roll-on de Lafes foram homenageados com vários prêmios, incluindo o prêmio \"Soluções naturais com beleza\" com dois anos seguidos. <br> <br>A Aloe Vera amacia e hidrata a pele e tem resultado bactericida que ajuda a manter a pele fresca e saudável o dia inteiro. A hamamélis é um adstringente natural e bactericida ajudando a manter a pele balanceada naturalmente. O óleo de coentro reduz o suor, age contra o odor e inibe o aparecimento de fungos, ajudando a curar infecções por fungos. <br> <br>Com 72% de ingredientes orgânicos certificados, o <span data-mce-fragment=\"1\">Desodorante Natural Roll-on Extra Strenght Lafe's</span> não tem parabenos, propilenoglicol, álcool ou alumínio, sem testes em animais e oferece uma proteção natural contra as bactérias que causam odor, com um aroma de melaleuca. <br> <br>Não é antiperspirante e nem antitranspirante, portanto, não obstruirá os poros. Apenas oferece uma proteção natural. <br> <br>Proteção contra odor: o <span data-mce-fragment=\"1\">Desodorante Natural Roll-on Extra Strenght Lafe's </span>elimina as bactérias que causam o odor, com as propriedades bacterianas de sais minerais naturais, oferecendo uma suave fragrância de melaleuca. <br> <br>Suave na pele: feito para uma proteção duradoura por 24 horas, o <span data-mce-fragment=\"1\">Desodorante Natural Roll-on Extra Strenght Lafe's </span>contém uma combinação de óleos essenciais para proporcionar uma proteção desodorante suave e eficaz <br> <br>Modo de Uso: Aplicar por toda extensão das axilas e reaplicar se necessário. <br> <br> Proteção 24 horas <br> Uma mistura especial que utiliza as propriedades bacterianas do óleo essencial da melaleuca para combater a bactéria que causa odor. <br> Livre de glúten <br> Livre de Proplyenoglicol <br>Sem OGM - Organismo Geneticamente Modificado <br> <br>Composição: Aqua (Água), Potassium Alum (Sais Minerais), Cyamopsis Tetragonoloba* (Goma Guar), Aloe Barbadensis* (Aloe Vera) Leaf Extract, Melaluca Alternifolia* (Tea Tree Oil, óleo essencial de melaleuca) Extract, Calendula Officinalis (Calendula) Flower Extract, Coriadrum Sativum (Coentro) Seed Oil, Hamamelis Virginiana (Bruxa Avelã) Extract, Eugenia Caryophyllus* (Clove Bud) Essential Oil, Glucose &amp; Glucose Oxidase &amp; Lactoperoxidase.</p>\n<p>*Certificado Orgânico.<br></p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 500.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/Roll-OnExtraStrength88ml-VEG.jpg?v=1626266697"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "bs-co-roll-on-deodorant-unscented-witch-hazel-88ml",
          "product_id": "1635d807-9f7a-45fc-8a32-50da92935062",
          "brand_handle": "bs-co",
          "title": "Roll-On Unscented Vegano 88ml",
          "body_html": "<p>O Desodorante Natural Roll-On Unscented Witch Hazel sem perfume faz você manter-se fresco e limpo sem o uso de fragrâncias ou produtos químicos nocivos. Este desodorante natural é feito com ingredientes orgânicos que efetivamente combatem odor da maneira natural. A Lafes foi homenageada com vários prêmios, incluindo o prêmio \"Soluções naturais com beleza\", dois anos seguidos. <br>A Aloe Vera amacia e hidrata a pele e tem resultado bactericida que ajuda a manter a pele fresca e saudável o dia inteiro. A hamamélis é um adstringente natural e bactericida ajudando a manter a pele balanceada naturalmente. O óleo de coentro reduz o suor, age contra o odor e inibe o aparecimento de fungos, ajudando a curar infecções por fungos.</p>\n<p>A Lafe’s se esforça para criar produtos orgânicos que estão livres de toxinas e produtos químicos prejudiciais, porque o que se passa na pele penetra diretamente no corpo. <br> <br>Com 72% de ingredientes orgânicos certificados, o Desodorante Roll-On Unscented Lafe's não tem parabenos, propilenoglicol, álcool ou alumínio, sem testes em animais e oferece uma proteção natural contra as bactérias que causam odor, sem perfume. <br> <br>Não é antiperspirante e nem antitranspirante, portanto, não obstruirá os poros. Apenas oferece uma proteção natural. <br> <br>Proteção contra odor: o Desodorante Roll-On Unscented Lafe's elimina as bactérias que causam o odor, com as propriedades bacterianas de sais minerais naturais. <br> <br>Suave na pele: feito para uma proteção duradoura por 24 horas, o Desodorante Roll-On Unscented Lafe-s sem fragrância combate bactérias causadoras de odor. <br> <br>Modo de Uso: Aplicar por toda extensão das axilas e reaplicar se necessário. <br> <br> Proteção 24 horas <br> Um desodorante suave, eficaz e sem fragrância que combate bactérias causadoras de odor <br> Livre de glúten <br> Livre de Proplyenoglicol <br>Sem OGM - Organismo Geneticamente Modificado <br> <br>Composição: Aqua (Água), Potassium Alum (Sais Minerais), Cyamopsis Tetragonoloba* (Goma Guar), Aloe Barbadensis* (Aloe Vera) Leaf Extract, Coriadrum Sativum (Coentro) Seed Oil, Hamamelis Virginiana (Bruxa Avelã) Extract, Eugenia Caryophyllus* (Clove Bud) Essential Oil, Glucose &amp; Glucose Oxidase &amp; Lactoperoxidase. *Certificado Orgânico. <br> <br> 71ml <br></p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 500.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/sad.jpg?v=1626836153"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "bs-co-roll-on-deodorant-bliss-iris-e-rose-88ml",
          "product_id": "94bcb3df-88e8-4dcd-a471-d0dd9ac694c4",
          "brand_handle": "bs-co",
          "title": "Roll-On Bliss Vegano 88ml  ",
          "body_html": "<p>O Desodorante Natural Roll-On Bliss Lafe's tem o aroma fresco de rosas e íris que deixa você se sentir limpo, combate as bactérias causadoras de odor sem o uso de produtos químicos nocivos. Este desodorante natural é livre de glúten e propilenoglicol e é feito com ingredientes orgânicos certificados. Os desodorantes naturais e orgânicos roll-on de Lafes foram homenageados com vários prêmios, incluindo o prêmio de \"Solução Natural de Beleza com Consciência\" Natural Solutions, dois anos seguidos. A Aloe Vera amacia e hidrata a pele e tem resultado bactericida que ajuda a manter a pele fresca e saudável o dia inteiro. A hamamélis é um adstringente natural e bactericida ajudando a manter a pele balanceada naturalmente. O óleo de coentro reduz o suor, age contra o odor e inibe o aparecimento de fungos, ajudando a curar infecções por fungos. <br> <br>Com 72% de ingredientes orgânicos certificados, o <span data-mce-fragment=\"1\">Desodorante Natural Roll-On Bliss Lafe's </span>não tem parabenos, propilenoglicol, álcool ou alumínio, sem testes em animais e oferece uma proteção natural contra as bactérias que causam odor, com um aroma floral. <br> <br>Não é antiperspirante e nem antitranspirante, portanto, não obstruirá os poros. Apenas oferece uma proteção natural. <br> <br>Suave na pele: feito para uma proteção duradoura por 24 horas, o <span data-mce-fragment=\"1\">Desodorante Natural Roll-On Bliss Lafe's </span>contém uma combinação de óleos essenciais para proporcionar uma proteção desodorante suave e eficaz. <br> <br>Proteção contra odor: o <span data-mce-fragment=\"1\">Desodorante Natural Roll-On Bliss Lafe's </span>elimina as bactérias que causam o odor, com as propriedades bacterianas de sais minerais naturais, oferecendo uma suave fragrância floral. <br> <br>Modo de Uso: Aplicar por toda extensão das axilas e reaplicar se necessário. <br> <br> Proteção 24 horas <br> Um aroma floral semelhante ao aroma maravilhoso de rosas e íris <br> Livre de glúten <br> Livre de Proplyenoglicol <br>Sem OGM - Organismo Geneticamente Modificado <br> <br> <br>Composição: Aqua (Água), Potassium Alum (Sais Minerais), Cyamopsis Tetragonoloba* (Goma Guar), Aloe Barbadensis* (Aloe Vera) Leaf Extract, Coriadrum Sativum (Coentro) Seed Oil, Hamamelis Virginiana (Bruxa Avelã) Extract, Eugenia Caryophyllus* (Clove Bud) Essential Oil, Rosa Damascena Flower Oil, Glucose &amp; Glucose Oxidase &amp; Lactoperoxidase.*Certificado Orgânico. <br> <br>73ml <br></p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 500.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/BLISS.jpg?v=1626836149"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }],
        "next_token": "eyJ2ZXJzaW9uIjoyLCJ0b2tlbiI6IkFRSUNBSGg5OUIvN3BjWU41eE96NDZJMW5GeGM4WUNGeG1acmFOMUpqajZLWkFDQ25BRmk0a2hER2hvWWNPaDk1dDlBMjlZZEFBQURwRENDQTZBR0NTcUdTSWIzRFFFSEJxQ0NBNUV3Z2dPTkFnRUFNSUlEaGdZSktvWklodmNOQVFjQk1CNEdDV0NHU0FGbEF3UUJMakFSQkF3NExnaGlvVExvUWlKMks4a0NBUkNBZ2dOWE9ZOHFweTU3T2VXRTNYWWxTRXE0QmZMeDFPdDZZbmRjMzE5cXhWWVpadFhSbG9PYUpUQW1BQjU0NGhyM2ZKcDVHMnVmQnBxbUpadjEwNkN5blUzalh4dlRObHN6WHVuMlVaUm1QWWRoc09jYk9OdCswR1d5VGt6UFd4RkdycEtZWWduV1RsU1pBN21wVjBjT0dOMGpuSFB6eU9IdzNJclZXcFpHa3hkRTMrN2l6UUM2aDkwM1hKOUNJVUFNUTNRSVlqdWpGS2wvdWR4UERWK1psMDJZektuYjBCRVNaVWdpZ01GeTVHWkQvYnZaUDUybk1UNGVGVVJNeThPaXdYV0xPR1JlanFhb2JFRmhwcEhBeXFzMWxSelpVODJKRFFqOXF5Uk1oV1V5blRaOTBpeFNQRU1EYVpTbE8wMXNRMy9EYlQzWW5mV1ZGRzBtR0RhVXF1dVQwN0xyeHJDSDJURG1yWERlajQwdCtPWjVBOXFvbm12bElQM3VBbTdMcGxwSE85YmJJUGU1TEpxS3BaWDhRQ1FZcUI2akFKazZMWWg2RWZTWDRmN3Q4WWp4TkkrV3ExTFE1c3E5UWlQTFc3RnJ5Mmg5NDk2NjB6RjNpazV3VW9IZXRFMXgxTjRFTFNnbkxNN1VkM3A0RnBITXRZV2Q0cFJhK3NiM2RtcXpJaE16UkZhVGNqeCtEM0hwVm5wMmVpL2prVURrWENOYktSellOOXdGVkpRei9iUVN0UGhkZHluV3ZVZG1oelZRd25LUWJ2NDdQNmFmcXRqZjE2MncxN1doeFordEU5V0wrWGZZWkZES09teDlTTFVsaldYN0MycVVJbHpFZUtEaUFuL0p5bGEyTFc3Sys4RmpDV0wwczRkUlk5VHRGaXVVZUhONUlWY0JLZUpXZjlVbkVWc09GT0Y4VnhRTHJoTUg1L3hvY212QlpSRkNtcmR5NEkvTE96eWZxSHMvVEZLRW11NkgzYlpiV2dJQ3JkekhyT0MwdWUzTnBLVE1pWXhBaWxHMFBmNEZESHZ5KzRjR2RYVFRGWE9VcThzZUx6S0tJYjBpaGE5WHlKeUJvTThPMTY2ZnAwUitLby9uVkQ2WnpOYU1xTDR1aUlERm9Wb08vUUR0aldUVWJ1dnJjQVpzbEhYVjAwNUxSZHQwMUNDbk9FYkZmMWE0NnVCb1NWWVk2U2kzaC9QZWZJWVJETUlDU3Vrc0Q4UTFCaTl3UE1BcWM4QjV3ZkxHSTVyTmpJS3NnTHBmSzJjL0owMXkvUGpJdXI1L2pjSHZGcGQ2RXI0bXNzRjNUSzlObHNXV2prZUxtZk00SVhJOXdxQVVwR0hYL0hDUmZTbFNTaUFuQXVNWkZDNDRNVG85M1V1TnM3UHFjYUFXQVIwcE1YTW9keGdVMTFaVDJ5aDRmUVU5OE9LT2lUcm44cFRCcysvY29jV3gyS04wQ0FyYUJ6QzNVNzlCekp5QUtBSzIvU05BMUdjc1dERDUweGlsdGdXS1l2VmJVZ1ZqVWpOVCJ9"
      },
      "banner": "https://fornecedor.inventa.app.br/supplier/image/03cad43cfee143450e52244cc85f27c1.png?v=1647377839583",
      "brand_handle": "bs-co",
      "brand_id": "cfb85809-d2b9-4488-bf1e-0596c52e9d4c",
      "brand_name": "BS&CO",
      "cnpj": "80078934000185",
      "min": 100,
      "days_to_ship": 5.0,
      "description": "Nosso negócio é movido pela conscientização. Ao invés de transformar a\nnatureza, deixamos que ela mesma nos transforme. Somos incansáveis na\nbusca por ingredientes naturais, inovadores e de qualidade. Tudo para\natender as necessidades do corpo e do planeta, que solicitam cada vez mais\ncuidado e respeito.",
      "index": null,
      "limit": null,
      "logo": "https://fornecedor.inventa.app.br/supplier/image/9b8b9ca903b365d4b04d26203325a761.jpeg?v=1634298043117",
      "products_quantity": 88.0,
      "vendor_id": "6337d405-8846-4faa-a12d-b998bbba88ad",
      "websites": [{
        "type": "Website",
        "url": "https://bioute.com.br/"
      }]
    }
  }
};
const positiva = {
  "data": {
    "getNmkpBrandByBrandHandle": {
      "products_brand_page": {
        "products": [{
          "handle": "positiv-a-cx-c-24-frasco-auxiliar-multiuso-geral-500ml-positiva",
          "product_id": "31de0da0-5308-4fcf-93cd-acaf092df01c",
          "brand_handle": "positiv-a",
          "title": "Frasco Auxiliar Multiuso Geral 500ml - 24 unid.",
          "body_html": "<p>Caixa com 24 unidades.</p>\n<p>Preço de varejo sugerido (unidade): R$ 4.99</p>\n<p><span>Frasco feito com plástico 100% reciclado coletado do litoral; Livre de BPA; Reciclável, flexível e durável; Acompanha tampa fliptop. &gt;&gt;&gt; IMPORTANTE! Este é um frasco auxiliar vazio, que serve para diluir o MULTIUSO CONCENTRADO 1L ou galão 5L. A diluição GERAL pode usada na limpeza de pisos, azulejos, sofás, paredes, portas, mármores, granitos, eletrodomésticos e todo o quarto do bebê.</span></p>\n<p><span>Sou um frasco 100% reciclado e reciclável, feito de plástico que já esteve por aí, poluindo o litoral. Agora ganhei uma nova vida e posso ser reutilizado muitas vezes! :)<br><br>Siga a diluição sugerida e dilua o MULTIUSO CONCENTRADO com água conforme instruções no rótulo do produto. Para diluição GERAL use 2 tampas de MULTIUSO CONCENTRADO e complete o frasco com água até a medida indicada. Reutilize os frascos é mais econômico para você e gera menos impacto para meio ambiente!<br><br><strong>Composição</strong><br>Frasco: Polietileno (PE) reciclado pós-consumo.<br><br>Tampa: Polipropileno (PP)<br><br><strong>Descarte</strong><br>Reciclagem.</span></p>",
          "min_quantity": 1.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 600.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/FRASCOGERAL.jpg?v=1647003228"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/FRASCOGERAL2.jpg?v=1647003228"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "positiv-a-cx-c-06-oleo-essencial-agroflorestal-de-capim-limao-10-ml",
          "product_id": "3bbec6cf-b76f-42a4-b68f-6b825e630df5",
          "brand_handle": "positiv-a",
          "title": "Óleo Essencial Agroflorestal de Capim Limão 10ml - 6 unid.",
          "body_html": "<p>Caixa com 6 unidades.</p>\n<p>Preço de varejo sugerido (unidade): R$ 59.99</p>\n<p><span>Bom para má digestão, flatulências, TPM e cólicas menstruais (não ingerir); Calmante e sedativo; Equilibra oleosidade; Recomendado para tratamento de micose; Redutor de febre; Relaxante muscular.</span></p>\n<p><span>Este produto é puro e vivo, fruto da natureza - e, por isso, pode haver alguma variação de cheiro entre os lotes. Mas a gente garante que é a mesma planta, com o mesmo nome botânico e as mesmas propriedades :) #SomosNatureza<br><br>Sobre Óleos Essenciais e Capim-Limão (Elionurus latiflorus)<br>Os óleos essenciais são substâncias naturais hiperconcentradas, extraídas de todas as partes das plantas - às vezes das flores, das folhas, dos caules, das hastes, das cascas ou das raízes. São riquíssimos em benefícios medicinais e aromaterapeuticos - cada qual com suas funções específicas.<br><br><strong>Dicas de uso</strong><br>No aromatizador elétrico Positiv.a: misture de 5 a 10 gotas em água, a depender do tamanho do cômodo, e ligue o aparelho.<br><br>Na Limpeza: dilua 15 gotas de Óleo Essencial de Capim Limão em 100mL de álcool e borrife no ambiente, adicione ainda Óleos de Eucalipto e Tea Tree (Melaleuca) para uma ação desinfetante. Não aplicar em madeira.<br><br><strong>Composição</strong><br>Elionurus latiflorus oil<br><br><strong>Curiosidades</strong><br>O Óleo Essencial de Capim Limão Brasileiro é oriundo da região serrana de Santa Catarina. Possui propriedades sedativas do sistema nervoso periférico, ação analgésica local e relaxante muscular. É indicado para tratamento de dores na coluna e hérnia de disco.<br><br>Óleos essenciais são super concentrados e devem ser usados com cuidado. Jamais ingerir ou usar diretamente sobre a pele sem indicação. Para crianças, gestantes e lactantes, use somente com orientação de um médico ou terapeuta profissional.<br><br>Parte da planta utilizada para extração: folhas.<br><br><strong>Diferencial</strong><br>Os óleos essencias Positiv.a são extremamente puros, produzidos de forma orgânica e são frutos de sistema agroflorestal. Ou seja, eles nascem em um processo de cuidado com a terra e, ao contrário das monoculturas, uma agrofloresta confere ampla diversidade de espécies, misturando agricultura com floresta, daí seu nome. Essa prática de plantio reproduz o funcionamento da Natureza.<br><br>Embalagem de vidro, com tampa de metal e rótulo serigrafado, vem em saquinho 100% algodão e serigrafado.<br><br><strong>Descarte</strong><br>O saquinho de algodão pode ser reutilizado para guardar miudezas como brincos, colares, botões etc.<br>O vidrinho vai para o lixo reciclável, junto com os vidros (cesto verde).<br>A bula vai para o lixo reciclável, junto com os papéis (cesto azul).<br><br><strong>Saiba mais sobre Agrofloresta</strong><br>Um sistema agroflorestal propõe, por meio da confluência entre plantas agrícolas e arbóreas, um sistema muito abundante. Falamos em abundância, pois os consórcios entre diversos tipos de plantas, de diferentes estratos (baixo, médio, alto e emergente), aumentam o grau de diversidade e densidade da plantação. Entre elas desenvolve-se um mutualismo: troca de fluidos, água, nutrientes e, dessa forma, elas se protegem de pragas e atraem organismos vivos indispensáveis para a terra. Gera-se um novo ecossistema, com solo mais saudável e rico em matéria orgânica, dispensando o uso de agrotóxicos e insumos externos, melhorando a qualidade dos alimentos produzidos e do ar a sua volta. O sistema agroflorestal é um ciclo completo, que favorece o bem-estar das pessoas, recupera as florestas, produz alimentos, além de contribuir para a melhoria do ecossistema.</span></p>",
          "min_quantity": 1.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 600.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/oleocapim10ml.jpg?v=1647012525"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/agroflorestaldecapimlimao.png?v=1647012525"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "positiv-a-cx-c-06-oleo-essencial-agroflorestal-de-citronela-10-ml",
          "product_id": "a7a0c19c-aec9-4a22-9a23-8c4a87281399",
          "brand_handle": "positiv-a",
          "title": "Óleo Essencial Agroflorestal de Citronela 10ml - 6 unid.",
          "body_html": "<p>Caixa com 6 unidades.</p>\n<p>Preço de varejo sugerido (unidade): R$ 44.99</p>\n<p><span>Antifadiga; Anti-inflamatório; Antimicrobiano; Antisséptico; Bactericida; Bom para gripe, resfriado e tosse; Calmante; Febrífugo; Fungicida; Repelente a insetos; Produção orgânica e agroflorestal; feita no Brasil.</span></p>\n<p><span>Este produto é puro e vivo, fruto da natureza - e, por isso, pode haver alguma variação de cheiro entre os lotes. Mas a gente garante que é a mesma planta, com o mesmo nome botânico e as mesmas propriedades :) #SomosNatureza<br><br>Sobre Óleos Essenciais e Citronela (Cymbopogon winterianus)<br>Os óleos essenciais são substâncias naturais hiperconcentradas, extraídas de todas as partes das plantas - às vezes das flores, das folhas, dos caules, das hastes, das cascas ou das raízes. São riquíssimos em benefícios medicinais e aromaterapêuticos - cada qual com suas funções específicas.<br><br><strong>Dicas de uso</strong><br>Ação repelente em ambientes internos: dilua 5 gotas do Óleo Essencial de Citronela em 1 colher de chá de água e aplique no Aromatizador elétrico Positiv.a.<br><br>Para ação bactericida e remover odores indesejados de ambientes: dilua 10 gotas de Óleo Essencial de Citronela em 200mL de álcool e aplique com borrifador ou pano úmido pelas superfícies desejadas. Bom para remover odores de urina animal. Não aplicar em madeira.<br><br><strong>Composição</strong><br>Cymbopogon winterianus oil<br><br><strong>Curiosidades</strong><br>O Óleo Essencial de Citronela é muito conhecido por sua propriedade repelente. Com ele podem ser feitos produtos como velas, sachês, loções hidratantes, repelentes e incensos. É utilizado para tratar insegurança e sensações de impureza espiritual.<br><br>Óleos essenciais são super concentrados e devem ser usados com cuidado. Jamais ingerir ou usar diretamente sobre a pele sem indicação. Para crianças, gestantes e lactantes, use somente com orientação de um médico ou terapeuta profissional.<br><br>Parte da planta utilizada para extração: folhas.<br><br><strong>Diferencial</strong><br>Os óleos essencias Positiv.a são extremamente puros, produzidos de forma orgânica e são frutos de sistema agroflorestal. Ou seja, eles nascem em um processo de cuidado com a terra e, ao contrário das monoculturas, uma agrofloresta confere ampla diversidade de espécies, misturando agricultura com floresta, daí seu nome. Essa prática de plantio reproduz o funcionamento da Natureza.<br><br>Embalagem de vidro, com tampa de metal e rótulo serigrafado, vem em saquinho 100% algodão e serigrafado.<br><br><strong>Descarte</strong><br>O saquinho de algodão pode ser reutilizado para guardar miudezas como brincos, colares, botões etc.<br>O vidrinho vai para o lixo reciclável, junto com os vidros (cesto verde).<br>A bula vai para o lixo reciclável, junto com os papéis (cesto azul).<br><br><strong>Saiba mais sobre Agrofloresta</strong><br>Um sistema agroflorestal propõe, por meio da confluência entre plantas agrícolas e arbóreas, um sistema muito abundante. Falamos em abundância, pois os consórcios entre diversos tipos de plantas, de diferentes estratos (baixo, médio, alto e emergente), aumentam o grau de diversidade e densidade da plantação. Entre elas desenvolve-se um mutualismo: troca de fluidos, água, nutrientes e, dessa forma, elas se protegem de pragas e atraem organismos vivos indispensáveis para a terra. Gera-se um novo ecossistema, com solo mais saudável e rico em matéria orgânica, dispensando o uso de agrotóxicos e insumos externos, melhorando a qualidade dos alimentos produzidos e do ar a sua volta. O sistema agroflorestal é um ciclo completo, que favorece o bem-estar das pessoas, recupera as florestas, produz alimentos, além de contribuir para a melhoria do ecossistema.</span></p>",
          "min_quantity": 1.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 600.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/citro.jpg?v=1647013271"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/agroflorestaldecitronela.png?v=1647013271"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "copia-de-cx-c-24-unid-lava-loucas-sem-fragancia-500ml_oceano-limpo",
          "product_id": "34070fe8-e287-4b8e-9435-a31cf9b67a44",
          "brand_handle": "positiv-a",
          "title": "Lava Louças em Pó para Máquina 450g - 16 unid.",
          "body_html": "<p><span data-mce-fragment=\"1\">Caixa com 16 unidades.</span></p>\n<p><span data-mce-fragment=\"1\"><span class=\"Polaris-Tag__TagText_lsfh6\" title=\"Preço de varejo sugerido (R $)_29.99\">Preço de varejo sugerido (unidade): R$ 29.99</span></span></p>\n<p><span>Produto pronto uso à base de óleo de coco; Sem fragrância ou corante; Rende 30 lavagens; Ação desengordurante; Livre de cloro, fosfato, sílica e derivados de petróleo; Dispensa o uso de secante abrilhantador; Embalagem zero plástico.</span></p>\n<p><span>Sou feito à base de coco, dispenso secante abrilhantador e mantenho o brilho das louças e metais ;)<br><br>ESTOU DE FÓRMULA NOVA: com misturas especiais de enzimas e extratos vegetais para garantir um desempenho ainda melhor!<br><br><strong>Modo de Usar</strong><br>Louças:<br><br>1) Remova os restos de alimento antes de colocar os utensílios na máquina de lavar louça;<br><br>2) Despeje 15g (uma colher de sopa) do produto no reservatório da máquina (8 serviços); caso as louças estejam muito engorduradas ou sua máquina seja maior, utilize até duas colheres (30g).<br><br><strong>IMPORTANTE!</strong><br>Não lave na máquina:<br><br>Artigos de madeira, porcelanas pintadas à mão;<br>Itens com aplicação de filetes de ouro ou prata;<br>Cristais finos, utensílios dourados ou prateados, estanho ou alumínio e antiguidades em geral.<br><br>Isso porque esses materiais podem ser sensíveis à lavagem à máquina - itens de plástico também podem reter resíduos do produto e ficar com aparência opaca. Siga sempre as instruções do fabricante da sua máquina!<br><br>Embalagem: fibralata (papel e metal), 100% reciclável e zero plástico<br><br><strong>Descarte</strong><br>Reciclagem<br><br><strong>Composição</strong><br>Óleo de coco, hidróxido de sódio (origem mineral, responsável pela saponificação), carbonato de sódio anidro (origem mineral, neutraliza a fórmula e ajuda a retirar gorduras), mistura de enzimas (agem em sujeiras como carboidratos, amido e proteínas), extratos vegetais (agem forte nas moléculas de gordura), cloreto de sódio (origem mineral, auxilia no enxágue), Sodium cocoyl isethionate (surfactante à base de coco que quebra a tensão superficial da água, permitindo a lavagem) e água.<br><br></span></p>",
          "min_quantity": 1.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 600.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/Lavaloucas1.jpg?v=1631128706"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "positiv-a-cx-c-12-espatula-de-bambu",
          "product_id": "dc653711-8d93-4e77-8abc-d7d559ff9d4e",
          "brand_handle": "positiv-a",
          "title": "Espátula de Bambu - 12 unid.",
          "body_html": "<p>Caixa com 12 unid.</p>\n<p>Preço de varejo sugerido (unidade): R$ 5.99</p>\n<p><span>Matéria-prima ecológica e sustentável; Produção 100% artesanal e brasileira; Super-resistente; Auxilia na dosagem e manuseio dos produtos cremosos da Positiv.a; É biodegradável, não gera resíduo para o planeta; Mantenha em lugar seco para aumentar sua durabilidade; Embalagem de papel kraft (sem nenhum ponto de cola).</span></p>\n<p><span>O bambu é uma matéria-prima extremamente sustentável!<br><br>Isso porque seu crescimento é rápido e seu uso é o mais variado possível: do alimento, à construção, passando por geração de energia e regeneração do solo. Mesmo sendo uma planta, é super-resistente.<br><br><strong>Composição</strong><br>Bambu com acabamento em verniz à base de água.<br><br><strong>Como Usar</strong><br>Use-a para auxiliar no manuseio e dosagem do Óleo de Coco e da Manteiga de Cupuaçu Positiv.a. Mantenha a espátula em local seco e arejado.</span></p>",
          "min_quantity": 1.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 600.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/espatula.png?v=1626836959"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/ESP1.jpg?v=1646945486"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/ESP2.jpg?v=1646945486"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/ESP3.jpg?v=1646945486"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "positiv-a-cx-c-24-pano-de-chao-organico-bege-positiva",
          "product_id": "616d745d-dfda-4dfb-96ca-c91a923809ba",
          "brand_handle": "positiv-a",
          "title": "Pano de chão orgânico bege",
          "body_html": "<p>Sou uma peça feita à mão, em algodão 100% orgânico e brasileiro, e passo por um processo único e artesanal Empresa B; Eu Reciclo&gt; Vegana, Agricultura Familiar, Compromisso 0 plástico, Capitalismo Consciente, parceiro Sea Sheapard.</p>",
          "min_quantity": 24.0,
          "max_quantity": null,
          "step_quantity": 24.0,
          "ship_all_states": true,
          "vendor_minimum": 600.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/Panodechaoorganico.png?v=1626836979"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "positiv-a-cx-c-12-trio-ecopad-de-algodao-saquinho",
          "product_id": "74761446-9c9a-4cdf-b11d-d3de38501844",
          "brand_handle": "positiv-a",
          "title": "Trio Ecopad de Algodão + Saquinho - 12 unid.",
          "body_html": "<p>Caixa com 12 unidades (trios).</p>\n<p>Contém três discos de tecido atoalhado de algodão, com acabamento costurado; Empresa B; Eu Reciclo&gt; Vegana, Agricultura Familiar, Compromisso 0 plástico, Capitalismo Consciente, parceiro Sea Sheapard.</p>",
          "min_quantity": 1.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 600.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/Trioecopaddealgodao.jpg?v=1626836988"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "positiv-a-cx-c-12-oleo-hidratante-de-coco-90-g",
          "product_id": "0335b221-cf00-411e-92b8-777e19f463f4",
          "brand_handle": "positiv-a",
          "title": "Óleo Hidratante de Coco 90g - 12 unid.",
          "body_html": "<p><span>Caixa com 12 unidades.</span></p>\n<p><span>100% puro e natura! Livre de parabenos, sulfatos, corantes, silicone e essências artificiais; Vegano; Cultivado de forma sustentável sem uso de agrotóxicos, respeitando o meio ambiente; Excelente emoliente para hidratar a pele do corpo e nutrir os cabelos; Rico em vitamina E, é um antioxidante natural; Indicado no tratamento para reforçar a estrutura de cabelos desvitalizados.</span></p>",
          "min_quantity": 12.0,
          "max_quantity": null,
          "step_quantity": 12.0,
          "ship_all_states": true,
          "vendor_minimum": 600.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/hidratantedecoco.png?v=1626836983"
          }, {
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/oleococo_cc05b3ee-ad46-478c-b1ee-69547b081a3c.jpg?v=1647464059"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "positiv-a-cx-c-12-sabonete-100-vegetal-capim-limao-e-alecrim-100-g",
          "product_id": "bc856f27-44ea-4e32-89a8-eafbf8284b6d",
          "brand_handle": "positiv-a",
          "title": "Sabonete 100% vegetal capim limão e alecrim 100gr",
          "body_html": "<p>Com óleos essenciais orgânicos e agroflorestais, e pedacinhos de Capim-Limão e Alecrim, que proporcionam esfoliação durante o banho; Empresa B; Eu Reciclo&gt; Vegana, Agricultura Familiar, Compromisso 0 plástico, Capitalismo Consciente, parceiro Sea Sheapard.</p>",
          "min_quantity": 12.0,
          "max_quantity": null,
          "step_quantity": 12.0,
          "ship_all_states": true,
          "vendor_minimum": 600.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/SABONETE100_VEGETAL.jpg?v=1626836983"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "positiv-a-cx-c-24-pano-de-prato-organico-bege-positiva",
          "product_id": "c16534a7-01ed-42df-8ca5-6c8434342aa2",
          "brand_handle": "positiv-a",
          "title": "Pano de prato orgânico bege",
          "body_html": "<p>Sou uma peça feita à mão, em algodão 100% orgânico e brasileiro, e passo por um processo único e artesanal Empresa B; Eu Reciclo&gt; Vegana, Agricultura Familiar, Compromisso 0 plástico, Capitalismo Consciente, parceiro Sea Sheapard.</p>",
          "min_quantity": 24.0,
          "max_quantity": null,
          "step_quantity": 24.0,
          "ship_all_states": true,
          "vendor_minimum": 600.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/panodepratobege.jpg?v=1626836978"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }],
        "next_token": "eyJ2ZXJzaW9uIjoyLCJ0b2tlbiI6IkFRSUNBSGg5OUIvN3BjWU41eE96NDZJMW5GeGM4WUNGeG1acmFOMUpqajZLWkFDQ25BRnhlbkNJSnY3L2NZYk96dlhPNXVKS0FBQURoRENDQTRBR0NTcUdTSWIzRFFFSEJxQ0NBM0V3Z2dOdEFnRUFNSUlEWmdZSktvWklodmNOQVFjQk1CNEdDV0NHU0FGbEF3UUJMakFSQkF5eGt6Z0c5TEV1ZkIwSmNlQUNBUkNBZ2dNMy81bTRWZUJmTHRQZGlRK09jTUxJZFBTRW93UzI2bDRObm5jNU1zRktsSVdvbkhrUy95Q1h4L3VSNmZrY2FmTmJURTNGbUkralpRWElkVmVUQzBIYldVdXl1MnM1Q2ZNajJHRmhwUWpZTm9lR08wTlJ0enk1enN3MzhSVlpKOXhDaVNWTml1UFkzVzBMMVpDZWh1QzNhN3VSaHBBckpXTHYyZHdGV1c2TGUvL001SHg4cXhYUExiVVAwMEJPT0E3dDdSTXZlbjBkZDMyOWRhc3Vvb0crRHBGaXJyYXpRNEIyRnkvZERleDN2b0xFb3ZkRW9FNkN3MzdydUROa1ZXTHdySkYrcXB2Um0xaUVjUDlrdnR1c1NOTThKTDJhYmNha2xxb1pwajFnRExkUlBQMHJPdDhvR01JR2hGR0VKNEI3OEkxc2lCbzVwMnRlV0Jhb0RZUUp3aHJ4aG1VdjRxb0trMnY0NE14RFlSbGxKd3d1TTFhN1hKSG4rdjBVV1pNZHBmZHBRVkRYdlZiMnY0RWw2RFU3RUNyY2VWMEJkRGRpd3hPZHZPTFpHa0FqaVVLR1hTMnlaMHFsSnVPRmxZOHVCN0dpMXhBVlRYTVFKcXk4cGZ5Ylk5SWkyZHZSVmxmOUwwS2dEdEw5WnZkYXYwM1FnYVlYMGZqdzNMaEdRTkVzWm0wYnIwTE9aYXF3ZXZHajFlL2VWK2xXZmRHMGxhdDY4WXRXTXFsVDQxUXRsOHRwalZWV28yY3VwWUtYTHdyNnA5NHJ2NDl0RGx4a2QvWnR5STBBQTJkYWM5VzRSNWhlS1ZFSEpQUytnOGF3VE9kT005eXI2SE5KTlhwaGJObUlkSjQ5ak1jbk1YM2JNZ1pSdzVDZVhjVEdLOUFwM3dRQzdaVitzZGhtRlk2SmJnamRaNWM4QWRPK0VubjRTbXVqSG8rNVNnanRKQmtrY3daRU51akQyM1RXbHFRcEpUZUgvSnM2V2x2aEozNmEvUlIzWWRiQmdXd0EyZ00rMFFOVnE3bmRiSjM0LzRCNXBvYmRRT284VU1ydTVqc00xQXJHKzYvK2U0TmlHNjZYWkhNTUFHeCtuYklhUEpzMGFaZm1ETG1ZZVdlYlh2Z0trZWpnYXlsTVBuVEd2Ulp0U29MZnRkTVNwK1h2Y2xVZTdUTStqYnJRQmw3cEZYZk13bnZmY2RoLzhxYnUzRFVMbWNnVU5tUUtQL3BvMzU5UEtaVXEvdUhwOEdrajhNV2lTUndtbVAzQTl1ODJoc0I5dHhXZmVwYkZURXlnY0RjVTVJNWhNd29nb0tWVDFnMzkxYTJ6Wis3TjJhUTVNTXpKdSs2R0gwcUZXN3dQbm9qaHdaYTJQZUd4N3hKc0FQYy9TZ2xOU1pESlRYdTlQNHZjR1B3aXVIakREMExPUEhPRjRJMGhFa0FTcFdvTjFMTG0xb2FFTG83L1BIcTdOUT09In0="
      },
      "banner": "https://krzstgblob.blob.core.windows.net/5ff4e85e672df8b22c1de26c/banner/1392e05bd17442c18aab880ff7412e78.jpg",
      "brand_handle": "positiv-a",
      "brand_id": "581a8b78-e762-435c-ba19-681076282e12",
      "brand_name": "Positiv.a",
      "cnpj": "22121913000107",
      "min": 600,
      "days_to_ship": 12.0,
      "description": "Somos uma empresa ecologicamente e socialmente responsável, que cria produtos e soluções para cuidar da sua casa, do seu corpo e do nosso planeta. #SomosNatureza",
      "index": null,
      "limit": null,
      "logo": "https://cdn.shopify.com/s/files/1/0556/2440/1049/files/positiva.png",
      "products_quantity": 74.0,
      "vendor_id": "9cc2bc1b-57e1-46a7-bfd3-39b57e6b1db4",
      "websites": [{
        "type": "Website",
        "url": "https://www.positiva.eco.br/Home"
      }]
    }
  }
};
const weNutz = {
  "data": {
    "getNmkpBrandByBrandHandle": {
      "products_brand_page": {
        "products": [{
          "handle": "goiaba-castanha",
          "product_id": "e389f8f8-b679-438a-b0a3-7231ea9b658a",
          "brand_handle": "we-nutz",
          "title": "Barrinha Goiaba & Castanha 35g",
          "body_html": "<p><span>Essa barrinha dispensa apresentações, ela chegou com tudo pra dar um toque de criatividade no seu lanchinho da manhã ou da tarde! Combinação melhor que essa não existe. A goiaba, carregada na vitamina C, super deliciosa junto com a gordura boa da castanha de caju, uma das nutz mais amadas, se transforma em uma explosão de sabores e dá o gostinho especial We Nutz.</span></p>",
          "min_quantity": 5.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 300.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/goiaba_e_castanha_267_2_a971c2a95323c7fa5337b08115eb10b1.png?v=1643208205"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "we-nutz-choconutz-branco-pasta-de-castanha",
          "product_id": "43ef823e-71dd-4f9c-ad84-c8ebddac764c",
          "brand_handle": "we-nutz",
          "title": "Choconutz Branco Pasta de Castanha 100g",
          "body_html": "<p><span>Está cansado de comer chocolate branco que sempre tem aquele gosto de baunilha, açúcar e mais nada? Vamos mudar essa realidade!</span></p>\n<p><span>Começando pela combinação do chocolate branco sem açúcar com a canela que traz um perfume delicioso, além de dar aquele bronzeado que o brasileiro ama. Para o recheio, mantivemos a filosofia do \"menos é mais\" e com apenas 5 ingredientes criamos uma obra prima. Uma pasta de castanha de caju super cremosa, com pedaços de coco e um toque de sal do Himalaia pra dar um up no sabor.</span></p>\n<p><span>É a libertação da mesmice! Depois dessa barra você nunca mais vai olhar chocolate branco com os mesmos olhos.</span></p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 300.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/choconutz_branco_pasta_de_castanha_de_caju_com_coco_7_3_737198b88c245c7668987a0a44c66841.png?v=1646258005"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "banana-amendoim",
          "product_id": "3ed7ee3b-6138-4dc9-a71c-cbbda66ad897",
          "brand_handle": "we-nutz",
          "title": "Barrinha Banana & Amendoim 35g",
          "body_html": "<p><span>A família cresceu! Nossas barrinhas estão com sabores cada vez mais diferentões e deliciosos. Mas tem uma coisa que nunca vai mudar: a qualidade dos ingredientes e a naturalidade. É barrinha de banana, é o gostinho do Brasil!!! Pensa em uma bananinha...agora acrescenta a crocância do amendoim e um toque especial do DNA We Nutz! Incomparável, ela chegou pra ser a mais nova queridinha dos Nutzlovers!</span></p>",
          "min_quantity": 5.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 300.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/banana_e_amendoim_269_2_30b3ae9cc12cc6e55949e5e74d993b2b.png?v=1643208204"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "we-nutz-pasta-de-amendoim-com-chocolate-70-250g",
          "product_id": "c5782cf6-956b-4d89-913c-3c8eb57c282d",
          "brand_handle": "we-nutz",
          "title": "Pasta de Amendoim com Chocolate 70% 250g",
          "body_html": "<p><span>Nessa criação We Nutz trouxemos duas paixões nacionais: o amendoim e o chocolate. A nossa pasta de Amendoim+Choco, além de super cremosa, vem com pedaços de chocolate 70% e amendoins torrados. Sim, muitos pedaços que dão um up no sabor e deixam a textura muito mais divertida. Naturalmente rica em proteína traz <strong>3,3 gramas por colherada de 15 gramas</strong>. </span></p>\n<p><span>Como se não fosse suficiente, ainda é fonte de energia para seu pré ou pós treino. Isso que nós chamamos de superfood! Fica uma delícia acompanhado de frutas, em receitas de cookies e como recheio de bolos. </span></p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 300.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/APASTA1.png?v=1643208201"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "we-nutz-nutznack-branco-com-canela-caramelo-salgado-e-amendoas",
          "product_id": "e1f3effd-db6a-47ce-86f2-fe376e2d7aeb",
          "brand_handle": "we-nutz",
          "title": "Nutznack Branco com Canela, Caramelo Salgado e Amêndoas 160g",
          "body_html": "<p><span>As vezes bate aquela vontade de um doce e a gente não sabe bem o que fazer. Oito em dez pessoas já pensam logo no chocolate. Mas aí já vem a culpa de ingerir açúcar, gordura e um monte de aditivos dos produtos industrializados.</span></p>\n<p><span>Pensando nisso, criamos o nosso Nutznack, o snack de chocolate perfeito pro seu desejo. Com somente 5 ingredientes. Pedaços de chocolate branco zero açúcar com caramelo de açúcar de coco e amêndoas torradas numa mistura tão equilibrada que beira a perfeição.</span></p>\n<p><span>Fica ótimo com aquele cafezinho pós-almoço,  assistindo uma série com o crush ou quando bate aquele desejo no meio da madrugada. Além de ser um presente muito charmoso.  Já levou a sua latinha?</span></p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 300.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/3073653.jpg?v=1643208199"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "we-nutz-pasta-de-castanha-de-caju-coco-450g",
          "product_id": "527cbf6f-5bab-492d-9cd2-75f9ec63a24c",
          "brand_handle": "we-nutz",
          "title": "Pasta de Castanha de Caju & Coco 450g",
          "body_html": "<p><span margin:=\"\" outline:=\"\" padding:=\"\" trebuchet=\"\">Castanha de caju é um produto irresistível, não é? Impossível comer 1 só. Pensando nisso criamos a nossa pasta de castanha de caju com coco. </span></p>\n<p><span margin:=\"\" outline:=\"\" padding:=\"\" trebuchet=\"\">Torramos nossas castanhas a perfeição, realçando todo seu sabor. Depois adicionamos óleo de coco, rico em antioxidante e pedaços de coco para dar aquele feito surpresa a cada mordida. Sem deixar de lado a nossa filosofia, tudo é natural. Industrializados não tem vez na We Nutz. O resultado é uma pasta fluida, perfeita para usar como recheio de bolos, para comer com panquecas ou simplesmente para cobrir aquela pipoca. </span></p>\n<p><span margin:=\"\" outline:=\"\" padding:=\"\" trebuchet=\"\">Um show de praticidade para trazer adoçar o seu dia. </span></p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 300.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/CASTANHAECOCO.png?v=1643208202"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "we-nutz-choconutz-branco-caramelo",
          "product_id": "70540892-8f24-4c82-b720-5058a639af7c",
          "brand_handle": "we-nutz",
          "title": "Choconutz Branco Caramelo 100g",
          "body_html": "<p><span>Pensado inicialmente como o nosso ovo de pascoa, esse sabor nasceu na nossa cozinha com a missão de ficar somente durante a festividade, mas fez tanto sucesso que não conseguimos desapegar.</span></p>\n<p><span>A combinação do chocolate branco, com o perfume da canela, a cremosidade das tâmaras e a crocância do amendoim traz uma sensação de conforto, como se a Vovó Nutz tivesse feito especialmente pra cada um de nós.</span></p>\n<p><span>Com a vantagem de ser feito somente com produtos naturais e sem conservantes. Pegue a sua antes que acabe!</span></p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 300.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/choconutz_branco_com_canela_caramelo_salgado_de_tamaras_11_3_454b54fb7b4ae505a006ca9c2c6cd5d6.png?v=1646258006"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "we-nutz-nutznack-matcha",
          "product_id": "1a9418a9-2845-425a-bb65-76026796e53a",
          "brand_handle": "we-nutz",
          "title": "Nutznack Chocolate Branco com Matcha 160g",
          "body_html": "<p><span>NUTZNACK BRANCO E MATCHA COM PISTACHE CARAMELIZADO, CRISPIES DE QUINOA E FLOR DE SAL</span></p>\n<p><strong><span>O lado verde da força</span></strong></p>\n<p>Combinamos o doce do chocolate branco </p>\n<p>E o caramelizado do Pistache</p>\n<p>Com a crocância do Crispies de Quinoa</p>\n<p> </p>\n<p><span>Plant Based</span></p>\n<p>Naturalmente gostoso </p>\n<p>Sem lactose </p>\n<p>Sem ingredientes de origem animal</p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 300.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/Capturadetela2021-12-28164342.png?v=1643208199"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "we-nutz-pasta-de-avela-com-choco-250g",
          "product_id": "1977b3fc-7830-4840-a00d-426bb18caa6f",
          "brand_handle": "we-nutz",
          "title": "Pasta de Avelã com Choco 250g",
          "body_html": "<p><span>Uma das queridinhas da família! E a gente sabe o porquê. É só abrir a tampa e é amor a primeira vista. Uma imensidão de experiências sensoriais ao mesmo tempo. </span></p>\n<p><span>Naturalmente proteica, rica em vitaminas antioxidantes e fibras, é uma aliada para um alimentação balanceada. Uma delicia de sobremesa. Versátil, pode ser consumida com frutas, recheio de bolos e cookies, ou para comer uma colherada para saciar aquele desejo de assaltar a despensa na madrugada.</span></p>\n<p>Sem adoçantes artificiais<br span=\"\" segoe=\"\" noto=\"\" helvetica=\"\" font-size:=\"\" droid=\"\">Sem conservantes<br span=\"\" segoe=\"\" noto=\"\" helvetica=\"\" font-size:=\"\" droid=\"\">Sem óleo de palma</p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 300.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/pastaavelacomchocowenutz_5.png?v=1643208200"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }, {
          "handle": "we-nutz-choconutz-70-banana-amendoim",
          "product_id": "5a5b1959-0025-4576-938f-ccf405cbe037",
          "brand_handle": "we-nutz",
          "title": "Choconutz 70% Banana & Amendoim 100g",
          "body_html": "<p><span>Se você conhece a WeNutz, já sabe que aqui nosso chocolate preto é 70% cacau e sem açúcar. Mas quem gosta de um sabor mais doce fica de fora? É claro que não.</span></p>\n<p><span>Nossa barra com recheio de Banana e Amendoim traz o dulçor na medida certa com um toque salgado pra realçar o conjunto. A banana traz aquele perfume e o crocante do amendoim completa esse pedaço de perdição. Seguindo nosso compromisso de alimento saudável, tudo é natural. </span></p>\n<p><span>Conservantes e aditivos não tem vez. São tantas vantagens que não tem como recusar.</span></p>",
          "min_quantity": 2.0,
          "max_quantity": null,
          "step_quantity": 1.0,
          "ship_all_states": true,
          "vendor_minimum": 300.0,
          "status": "active",
          "ie_products": false,
          "not_shipping_to": [],
          "images": [{
            "alt": null,
            "src": "https://cdn.shopify.com/s/files/1/0507/3226/9775/products/choconutz_70_banana_e_amendoim_236_4_649fecafb64fe8b69a1db7c1d4dff14b.png?v=1646258007"
          }],
          "options": [{
            "name": "Title",
            "position": 1,
            "values": ["Default Title"]
          }]
        }],
        "next_token": "eyJ2ZXJzaW9uIjoyLCJ0b2tlbiI6IkFRSUNBSGg5OUIvN3BjWU41eE96NDZJMW5GeGM4WUNGeG1acmFOMUpqajZLWkFDQ25BRlJZcXFvNmF4alFhOFdMVk52VzhRbEFBQURnRENDQTN3R0NTcUdTSWIzRFFFSEJxQ0NBMjB3Z2dOcEFnRUFNSUlEWWdZSktvWklodmNOQVFjQk1CNEdDV0NHU0FGbEF3UUJMakFSQkF3VE1QTkVweVFUdUFIbDJpa0NBUkNBZ2dNekw0NStqWVk1QlFweEsvS0JNdTM1OTZlemJ0UmVEcmxsSjZvWWczMHJGd0VMb1JmMlY2eEtUT09Ob2taM2l3OUQ4emxsUEt5aWExai9XYThQNk5DeUdITWJLbjVwZE50UENJVmFXdXNQSzJvL0NVNlM2cDVqTmsvYmk4NXh2VUV4ektZeTRLVncrbzByOGFZZWk3UG04RjNPcVJCUEdOSGl4bkVWa3FIc3JrQW56SEFRMGRUVTNzOGNoeFFFanFyZVplcHEweENwdEQ3TFZCQzhSbENKeDc5Q3JobEptVlR2UlpiN3ZEVDhRdi9DVUpSSCs1YktrUHh1RnJtWnZWTXE2ZVlwM2wyY2NJZ0tBWjlBYWtQTEp0RVpIZW5EVDNDU3o1QzV3S1pRVGJDOWVSMG4vbEE0MFg1ZGFxSFVEeFVTWjY0bjRHWm1ZN3N2SGFIQUpRWVBZc2o5RGd4ajdmcmZLb0lIR0ZsMGdETVZWTXFpckttaU1hdC9qWHlSc2F4WTFROU53bGtza1MwQ0Z5Wk9XN09tTTRVK1ZSQmRXZWlDWFZDSVVuZUFyYTBRTVJSVVNoVzN5WnM3aHFaaDZScEpTQitmbFl2eEdXYmRmU2QzazBHNWNGdEZxZXlEcDB6VHR1M0J5UEhUNTRPdU54ekttUGtEc29NZ1hIclRHNDFJVFVrNzBVTWhLWVM3azJTdS9NbjUyM1BYUHFzOFdJb2pXeTg1aVRMQmNjY3NpZVh2Y3dNL0R6bWxyZjh6eWd5cnRXaEFxWjc1VzhWaGxvdWl3cGJkdTZTU3QzNVZMWTAyQ1dFWUZ2M2E4NVR0K2RvckxBS0kwZU1sYnF3dHhUQWdRY29tZnc2MGtZNVo2ekNFY2NFTkpXYXJ2dHlYQkFMb0hTeUlNM0hrYUwrQVdmU0NBMjExVnBVbXZVMHdjL1VDK3FIYVkzN1gzckNONG1HRnBvbFNiWVVYbm9HaXhWYlJrdlJrK2JVT0FaU0V5WWZXRExFY0tFRE5mL1lwSTJodW5qRGlUK1lqdC9KUVpHWFhBdnBzdnFMQ01CbFR2MGw3YjBmdkh6VmRPR1kwc0poa0Q5Rnl3elN4TVU1MUJ2cDEwUkFGMFR5YTArQ1cyNlMyaFBvRmhYTDJrSzE0ZmdqTE14bHBYMEYzazZPRG5OWTFvY1JNMHVWWlBVR2hXVE5tS2dRbi9yM08xSm50UlpVbGtzWUVFWXRDUFFneUNzU2F2eWgrZDVkbzlxMVlCT3VkL3djU2lZRHB5d3hBejdMYUt3YWh6bTliYkxQQWh3QnorK20zZUUySEFxZnBxTk1ydjZxZzM2Rnd2bGxrWDd0bGFPd0x5RnNFQmhIcGlkMDdQTW93NkZsQzV3WHp6eFR4bTlXWVIzbWF0ejluOVNvaURBNTlxOVRMVjduOThQR2dIZjdaMWpZd3hlWnJkdHVFNm02eSJ9"
      },
      "banner": "https://fornecedor.inventa.app.br/supplier/image/b1505ee7f3d116ba0b19564f19644e00.jpeg?v=1634837461849",
      "brand_handle": "we-nutz",
      "brand_id": "5b6b9c0b-9d4b-47fa-a17e-5441a6a2319c",
      "brand_name": "We Nutz",
      "cnpj": "32122355000140",
      "min": 100,
      "days_to_ship": 7.0,
      "description": "",
      "index": null,
      "limit": null,
      "logo": "https://cdn.shopify.com/s/files/1/0556/2440/1049/files/we-nutz.png",
      "products_quantity": 28.0,
      "vendor_id": "47328e3d-1631-4502-82fb-b6ce30d95b42",
      "websites": [{
        "type": "Website",
        "url": "https://www.wenutz.com/"
      }]
    }
  }
};

/***/ }),

/***/ 3029:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "M": () => (/* binding */ FornecedorContainer)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./services/contexts/UserContext.js
var UserContext = __webpack_require__(6404);
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(5675);
// EXTERNAL MODULE: ./components/QuantityComponent/index.js
var QuantityComponent = __webpack_require__(5293);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/SprintGrowth/BrandProducts/BrandProducts/BrandProductCard/SeePrecoButton/index.js


const SeePrecoButton = () => {
  return /*#__PURE__*/_jsx("a", {
    href: "undefined/",
    class: "bg-primaryYellow text-center border-primaryYellow text-black rounded text-xs font-roboto font-medium px-8 py-3.5 w-full whitespace-nowrap",
    children: "Ver pre\xE7o"
  });
};
// EXTERNAL MODULE: ./services/utils.js
var utils = __webpack_require__(3393);
;// CONCATENATED MODULE: ./components/SprintGrowth/BrandProducts/BrandProducts/BrandProductCard/HidePriceButton/index.js



const HidePrice = () => {
  return /*#__PURE__*/jsx_runtime_.jsx("button", {
    className: "w-full bg-primaryYellow bottom-0 text-white font-medium font-roboto rounded py-2 mt-1 ",
    onClick: () => (0,utils.triggerEvent)("modal_open:login"),
    children: "Ver Pre\xE7o"
  });
};

/* harmony default export */ const HidePriceButton = (HidePrice);
;// CONCATENATED MODULE: ./components/SprintGrowth/BrandProducts/BrandProducts/BrandProductCard/index.js
// import SeePriceButton from "../../../FornecedorPage/SeePriceButton";
// import AddProduct from "../AddProductComponent";









const BrandProductCard = ({
  title,
  minimal_order,
  image,
  price,
  minimal_quant,
  discount
}) => {
  // const { auth } = useContext(UserContext);
  const auth = true;
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("li", {
    className: "relative w-56 h-[25rem] mx-3",
    children: [/*#__PURE__*/jsx_runtime_.jsx(next_image.default, {
      src: image,
      alt: title,
      width: 400,
      height: 400,
      className: "border-gray-100"
    }), auth ? /*#__PURE__*/(0,jsx_runtime_.jsxs)("h2", {
      className: "text-['#666666'] text-base text-slate-500",
      children: [title, " | Min R$", minimal_order.toFixed(2).toString().replace(".", ",")]
    }) : /*#__PURE__*/jsx_runtime_.jsx("h2", {
      className: "text-['#666666'] text-base text-slate-500",
      children: title
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("span", {
      className: "flex",
      children: [auth ? /*#__PURE__*/(0,jsx_runtime_.jsxs)("h3", {
        className: "flex text-lg text-gray-500",
        children: ["R$", /*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "relative top-1 material-icons-outlined",
          children: "lock"
        })]
      }) : /*#__PURE__*/(0,jsx_runtime_.jsxs)("h3", {
        className: "text-lg whitespace-nowrap",
        children: ["R$", /*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "material-icons-outlined",
          children: "lock"
        })]
      }), discount > 0 && /*#__PURE__*/(0,jsx_runtime_.jsxs)("p", {
        className: "text-xs text-red-500 ml-2 mt-0.5",
        children: [discount, "% off"]
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("p", {
      className: "text-base text-slate-500 ",
      children: ["Quantidade m\xEDnima ", minimal_quant, " und"]
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      style: {
        position: 'absolute',
        bottom: '0px',
        width: '100%'
      },
      children: /*#__PURE__*/jsx_runtime_.jsx(HidePriceButton, {})
    })]
  });
};

/* harmony default export */ const BrandProducts_BrandProductCard = (BrandProductCard);
;// CONCATENATED MODULE: ./components/SprintGrowth/BrandProducts/BrandProducts/index.js



const BrandProducts = ({
  fornecedor
}) => /*#__PURE__*/jsx_runtime_.jsx("div", {
  style: {
    marginHorizontal: 20
  },
  children: /*#__PURE__*/jsx_runtime_.jsx("ul", {
    className: "flex flex-wrap justify-center m-10 ",
    children: fornecedor.data.getNmkpBrandByBrandHandle.products_brand_page.products.map(product => {
      return /*#__PURE__*/jsx_runtime_.jsx(BrandProducts_BrandProductCard, {
        userLogged: true,
        title: product.title,
        minimal_order: product.min_quantity,
        image: product.images[0].src,
        price: product.price,
        minimal_quant: product.min_quantity
        /* discount={product.discount} */

      }, product.title);
    })
  })
});

/* harmony default export */ const BrandProducts_BrandProducts = (BrandProducts);
{
  /* 
  return (
     <div className="grid grid-cols-1 md:grid-cols-6">
       <ul className="flex mb-4 flex-wrap justify-center gap-y-4 gap-x-4 sm:grid-cols-3">
         {fornecedor.data.getNmkpBrandByBrandHandle.products_brand_page.products.map((product) => {
           return (
             <ProductCard
               userLogged={true}
               key={product.id}
               title={product.title}
               minimal_order={product.minimal_order}
               image={product.image}
               price={product.price}
               minimal_quant={product.minimal_quant}
               discount={product.discount}            
             />
           );
         })}
       </ul>
     </div>
   );
  */
}
;// CONCATENATED MODULE: ./components/SprintGrowth/FornecedorContainer/index.js




const FornecedorContainer = ({
  fornecedor
}) => /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
  className: "mx-auto w-fit",
  children: [/*#__PURE__*/jsx_runtime_.jsx("p", {
    className: "mx-auto w-fit mb-5 text-xl md:pt-24 lg:pt-0",
    children: "Nossos Produtos"
  }), /*#__PURE__*/jsx_runtime_.jsx(BrandProducts_BrandProducts, {
    fornecedor: fornecedor
  })]
});
/* 
<div className='flex mx-1/2 flex-col md:flex-row-reverse'>
      <BrandProducts fornecedor={fornecedor} />
      <div className='border-solid border-2 w-1/5'>BrandAside</div>
      </div>    
*/

/***/ }),

/***/ 2431:
/***/ (() => {

/* (ignored) */

/***/ })

};
;