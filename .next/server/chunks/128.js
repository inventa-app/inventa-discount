"use strict";
exports.id = 128;
exports.ids = [128];
exports.modules = {

/***/ 8128:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Mobilestatic)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./components/Dropdown/index.js
var Dropdown = __webpack_require__(7331);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/Landing/Header/Mobilestatic/DiscountsPopUp/index.js


const discounts = [{
  minimum: 1000,
  percentage: 5
}, {
  minimum: 2000,
  percentage: 10
}, {
  minimum: 4000,
  percentage: 20
}];

const DiscountsPopUp = () => {
  return /*#__PURE__*/_jsxs("div", {
    className: "pt-5",
    children: [/*#__PURE__*/_jsx("p", {
      className: "font-noto font-bold text-2xl",
      children: "Descontos"
    }), /*#__PURE__*/_jsx("ul", {
      children: discounts === null || discounts === void 0 ? void 0 : discounts.map((discount, index) => /*#__PURE__*/_jsxs("li", {
        className: "my-5 py-5 px-6 bg-primaryLight bg-opacity-30 rounded-lg",
        children: [/*#__PURE__*/_jsx("p", {
          className: "font-lato text-xs uppercase text-primaryDark",
          children: "Descontos"
        }), /*#__PURE__*/_jsxs("p", {
          className: "my-2 font-lato font-bold text-lg",
          children: ["Compre mais de R$ ", discount.minimum, " e ganhe ", discount.percentage, "%"]
        }), /*#__PURE__*/_jsx("p", {
          className: "w-fit p-1 pr-2 bg-primaryDark rounded-[0.188rem] font-noto text-xs uppercase text-white",
          children: "Em produtos da We Nutz"
        })]
      }, `discount${index}`))
    })]
  });
};

/* harmony default export */ const Mobilestatic_DiscountsPopUp = ((/* unused pure expression or super */ null && (DiscountsPopUp)));
// EXTERNAL MODULE: ./services/contexts/UserContext.js
var UserContext = __webpack_require__(6404);
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(5675);
;// CONCATENATED MODULE: ./components/Landing/Header/Mobilestatic/index.js








const data = {
  banner: 'https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg',
  logo: 'https://images.tcdn.com.br/files/859628/themes/13/img/settings/we.jpg',
  name: 'We Nutz',
  total_products: 500,
  discount: '10% off na primera compra'
};

const FornecedorHeader = ({
  fornecedor
}) => {
  const {
    0: showDropdown,
    1: setShowDropdown
  } = (0,external_react_.useState)(false);

  const handleToggleDropdown = () => {
    setShowDropdown(!showDropdown);
  };

  const {
    clientWidth
  } = (0,external_react_.useContext)(UserContext.UserContext);
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    style: {
      marginTop: 150,
      marginBottom: 10,
      width: clientWidth
    },
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex",
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        style: {
          position: "relative",
          top: -20,
          height: "170px",
          flex: "0 0 auto",
          width: "170px",
          margin: "20px"
        },
        children: /*#__PURE__*/jsx_runtime_.jsx("div", {
          style: {
            width: "170px",
            height: "170px",
            backgroundColor: "white",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            boxShadow: "0px 5px 25px rgb(0 0 0 / 15%)"
          },
          children: /*#__PURE__*/jsx_runtime_.jsx("img", {
            src: fornecedor.data.getNmkpBrandByBrandHandle.logo
          })
        })
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        children: [/*#__PURE__*/jsx_runtime_.jsx("h2", {
          className: "text-4xl font-medium text-[#666]",
          children: fornecedor.data.getNmkpBrandByBrandHandle.brand_name
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("span", {
          className: "font-light text-base font-lato text-secondaryDark",
          children: ["M\xEDnimo R$", fornecedor.data.getNmkpBrandByBrandHandle.min]
        }), /*#__PURE__*/jsx_runtime_.jsx("p", {
          className: "text-xl font-lato font-[300] text-secondaryDark",
          children: fornecedor.data.getNmkpBrandByBrandHandle.description
        })]
      })]
    })
  });
};

/* harmony default export */ const Mobilestatic = (FornecedorHeader);

/***/ })

};
;