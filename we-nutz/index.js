import React from 'react'
import { useRouter } from "next/router";
import { useContext } from "react";
import { UserContext } from "@services/contexts/UserContext";
import { products } from "@components/Carousel/mock_data";
import Carousel from "@components/Carousel";
import HeaderMobile from "@components/Landing/Header/Mobile";
import HeaderDesktop from "@components/Landing/Header/Desktop";
import { urlObjectKeys } from 'next/dist/shared/lib/utils';
import { FornecedorContainer } from '@components/SprintGrowth/FornecedorContainer';

import Link from 'next/link';
import Redirect from '@components/Redirect';
import { weNutz } from '@components/SprintGrowth/BrandProducts/BrandProducts/brands';

// lista de páginas posibles
const fornecedores = ["mais-pura", "we-nutz", "bs", "positiva"];


const FornecedorPromocionado = () => {
  const router = useRouter();
  const { clientWidth } = useContext(UserContext);
  var element;
  
  return (
    
    <div>
      {clientWidth >= 768 ? <HeaderDesktop fornecedor={weNutz} /> : <HeaderMobile />}
      <FornecedorContainer fornecedor={weNutz} />
      {(!fornecedores.includes(router.query.fornecedor)) && <Redirect />}
    </div>
  )
}

export default FornecedorPromocionado;