function legacyRedirect(pathname, data, SHOPIFY_DOMAIN, newTab = false) {
  const b = document.createElement("form");
  b.setAttribute("method", "GET");
  b.setAttribute(
    "action",
    SHOPIFY_DOMAIN + "/apps/user-session/redirect-shopify"
  );
  b.innerHTML = `<input name="pathname" value="${pathname}" />`;
  b.innerHTML += `<input name="updates" value="${data}" />`;
  b.id = "redirect-form";
  b.style.display = "none";
  if (newTab) {
    const formData = new FormData(b);
    const url =
      SHOPIFY_DOMAIN +
      "/apps/user-session/redirect-shopify?" +
      new URLSearchParams(formData).toString();
    window.open(url, "_blank");
  } else {
    document.querySelector("body").appendChild(b);
    document.getElementById("redirect-form").submit();
  }
}
