const inventaCartLegacy = {};

function getLegacyCart(callback) {
  if (typeof callback === "function") {
    if (inventaCartLegacy.cart) callback(inventaCartLegacy.cart);
    else inventaCartLegacy.callback = callback;
  }
}

function receiveLegacyCart(data) {
  if (inventaCartLegacy.callback) {
    inventaCartLegacy.callback(data);
  } else {
    inventaCartLegacy.cart = data;
  }
}

window.addEventListener("load", () => {
  const script = document.createElement("script");
  script.src =
    "https://inventa.shop/cart.json?callback=receiveLegacyCart&v=" +
    new Date().getTime();
  document.querySelector("head").appendChild(script);
});
