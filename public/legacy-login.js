function legacyLogin(email, password, SHOPIFY_DOMAIN) {
  const b = document.createElement("form");
  b.setAttribute("method", "POST");
  b.setAttribute("action", SHOPIFY_DOMAIN + "/account/login");
  b.innerHTML = '<input name="customer[email]" value="' + email + '" />';
  b.innerHTML += '<input name="customer[password]" value="' + password + '" />';
  b.innerHTML +=
    '<input type="hidden" name="form_type" value="customer_login" />';
  b.innerHTML += '<input type="hidden" name="utf8" value="✓" />';
  let return_to = window.location.pathname;
  if (return_to.includes("produtos"))
    return_to = return_to.replace("produtos", "products");
  b.innerHTML +=
    '<input type="hidden" name="return_to" value="' + return_to + '">';
  b.id = "login-form";
  b.style.display = "none";
  document.querySelector("body").appendChild(b);
  document.getElementById("login-form").submit();
}
