export const SHOPIFY_DOMAIN = process.env.SHOPIFY_DOMAIN;
export const SHOPIFY_ACCESS_TOKEN = process.env.SHOPIFY_ACCESS_TOKEN;
export const SHOPIFY_MULTIPASS = process.env.SHOPIFY_MULTIPASS;
export const JWT_SECRET_SIGN = process.env.JWT_SECRET_SIGN;
export const LEGACY_SITE = process.env.LEGACY_SITE;
export const APPSYNC_URL = process.env.APPSYNC_URL;
export const APPSYNC_API_KEY = process.env.APPSYNC_API_KEY;
