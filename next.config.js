module.exports = {
  env: {
    SHOPIFY_ACCESS_TOKEN: process.env.SHOPIFY_ACCESS_TOKEN,
    SHOPIFY_DOMAIN: process.env.SHOPIFY_DOMAIN,
    LEGACY_SITE: process.env.LEGACY_SITE,
    APPSYNC_API_KEY: process.env.APPSYNC_API_KEY,
    APPSYNC_URL: process.env.APPSYNC_URL,
    JWT_SECRET_SIGN: process.env.JWT_SECRET_SIGN,
  },
  reactStrictMode: true,
  images: {
    domains: [
      "cdn.shopify.com",
      "simply-delicious-food.com",
      "images.tcdn.com.br",
      "fornecedor.inventa.app.br",
    ],
    loader: 'akamai',
    path: '',
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: 'https://inventa.shop',
        permanent: true,
      },
    ]
  },
};
