import Image from "next/image";
const CouponImage = "/general_images/discount_banner/discount_coupon_image.png";
const Right = "/general_images/discount_banner/right.png";
const Left = "/general_images/discount_banner/left.png";

const DiscountBanner = ({ userLogged }) => {
  return (
    <div className="relative text-center py-12 px-7 md:py-20 md:px-14 bg-secondaryDark rounded-xl overflow-hidden">
      {userLogged ? (
        <div className="z-[2] relative">
          <div className="md:hidden flex flex-col items-center space-y-8">
            <h1 className="text-white font-noto font-bold text-2xl m-auto leading-relaxed">
              Ganhe 10% off na primeira <br /> compra com o cupom:
            </h1>
            <div>
              <Image
                src={CouponImage}
                alt="discount-coupon"
                width={270}
                height={100}
              />
            </div>
          </div>
          <div className="hidden md:flex flex-col items-center space-y-14">
            <h1 className="text-white text-center text-3xl lg:text-4xl leading-none font-bold font-noto m-auto">
              Ganhe 10% off na <br /> primeira compra
            </h1>
            <button
              className="bg-primaryYellow text-white font-roboto font-medium text-lg rounded px-20 py-6
                          hover:bg-buttonHover transition duration-200 ease-in-out"
            >
              Comprar agora
            </button>
          </div>
        </div>
      ) : (
        <div className="z-[2] relative flex flex-col items-center space-y-14">
          <h1 className="text-white font-noto font-bold text-2xl lg:text-4xl leading-relaxed ">
            Ganhe 10% off na <br /> primeira compra
          </h1>
          <button
            className="bg-primaryYellow text-white
                              font-roboto font-medium text-lg rounded
                              px-10 py-4
                              hover:bg-buttonHover transition duration-200 ease-in-out"
          >
            Cadastre-se
          </button>
        </div>
      )}
      <div className="z-[1] absolute top-0 right-0">
        <Image src={Right} alt="" width={78} height={123} />
      </div>
      <div className="z-[1] absolute bottom-0 left-0">
        <Image src={Left} alt="" width={50} height={118} />
      </div>
    </div>
  );
};

export default DiscountBanner;
