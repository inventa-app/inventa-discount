import { useEffect } from "react";

const Aside = ({
  children,
  display,
  position,
  timing,
  duration,
  className,
}) => {
  const positionStyles = {
    right: "right-0",
    left: "left-0",
    top: "top-0 left-0",
  };

  const sizeStyles = {
    right: { true: "w-screen", false: "w-0 px-0", default: "h-screen" },
    left: { true: "w-screen", false: "w-0 px-0", default: "h-screen" },
    top: { true: "h-screen", false: "h-0 py-0", default: "w-screen" },
  };

  if (!positionStyles[position]) {
    position = "right";
  }

  useEffect(() => {
    document.body.style.overflowY = `${
      display === false ? "scroll" : "hidden"
    }`;
    //document.body.style.overflowX = `${display === false ? "scroll" : "hidden"}`;
    return () => {
      document.body.style.overflowY = "auto";
    };
  }, [display]);

  return (
    <div>
      <div
        className={`bg-white lg:w-0 lg:h:0 overflow-hidden absolute inset-y-0 p-7 z-20
            ${sizeStyles[position || "right"].default || ""}
            ${sizeStyles[position || "right"][display] || ""} 
            ${positionStyles[position] || ""}
            ${timing || "ease"}
            ${duration || "duration-500"}
            ${className || ""}
            transition-all`}
      >
        {children}
      </div>
    </div>
  );
};

export default Aside;
