const Dropdown = ({ children, closeModal }) => {
  return (
    <div>
      <div className="bg-white py-10 border w-screen h-fit max-h-full overflow-y-auto fixed bottom-0 inset-x-0 rounded-t-2xl p-6 transform transition duration-200 ease-in-out z-[999]">
        <div className="float-right">
          <button className="focus:outline-none" onClick={closeModal}>
            <span className="material-icons-outlined">close</span>
          </button>
        </div>
        {children}
      </div>
    </div>
  );
};

export default Dropdown;
