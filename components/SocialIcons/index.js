import styles from './style.module.css';

const SocialIcons = () => {
	return (
		<div className="flex mt-1">
			<div className={styles.icon + ' ' + styles.instagram} />
			<div className={styles.icon + ' ' + styles.facebook} />
			<div className={styles.icon + ' ' + styles.polygon} />
		</div>
	);
};

export default SocialIcons;
