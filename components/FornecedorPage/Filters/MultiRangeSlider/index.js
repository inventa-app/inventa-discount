import { useState, useEffect } from "react";
import Bubble from "./Bubble";
import styles from "./style.module.css";

const MultiRangeSlider = () => {
  const min = 25;
  const max = 200;
  const [minValue, setMinValue] = useState(min);
  const [maxValue, setMaxValue] = useState(max);
  const [rangeLeft, setRangeLeft] = useState(0);
  const [rangeWidth, setRangeWidth] = useState(0);

  const handleMinValueChange = (e) => {
    const value = Math.min(+e.target.value, maxValue - 1);
    setMinValue(value);
  };

  const handleMaxValueChange = (e) => {
    const value = Math.max(+e.target.value, minValue + 1);
    setMaxValue(value);
  };

  const getPercent = (value) => {
    return Math.round(((value - min) / (max - min)) * 100);
  };

  useEffect(() => {
    const minPercent = getPercent(minValue);
    const maxPercent = getPercent(maxValue);

    setRangeLeft(minPercent);
    setRangeWidth(maxPercent - minPercent);
  }, [minValue, maxValue]);

  const leftSpanPosition = ((minValue - min) * 100) / (max - min) + "%";
  const rightSpanPosition = ((maxValue - min) * 100) / (max - min) + "%";

  return (
    <div className="pt-3">
      <Bubble value={minValue} position={leftSpanPosition} />

      <Bubble value={maxValue} position={rightSpanPosition} />

      <div className="flex items center justify-center py-6">
        <input
          className={`${styles.thumb} ${
            minValue > max - 100 ? "z-50" : "z-30"
          }`}
          type="range"
          min={min}
          max={max}
          value={minValue}
          onChange={handleMinValueChange}
        />

        <input
          className={`${styles.thumb} z-40`}
          type="range"
          min={min}
          max={max}
          value={maxValue}
          onChange={handleMaxValueChange}
        />

        <div className="relative w-56">
          <div className="absolute w-full h-1 rounded-md bg-[#C1C9D2] z-10" />

          <div
            className="absolute h-1 rounded-md bg-secondaryDark z-20"
            style={{ left: `${rangeLeft}%`, width: `${rangeWidth}%` }}
          />

          <div className="flex justify-between mt-2 mb-1 ">
            {[...Array(10)].map((_, index) => (
              <span
                className="text-[0.375rem] first-of-type:text-xs last-of-type:text-xs"
                key={`pipe${index}`}
              >
                |
              </span>
            ))}
          </div>

          <div className="flex justify-between">
            <span className="font-lato font-medium text-xs text-multiRangeSliderSpan">
              {min}
            </span>
            <span className="font-lato font-medium text-xs text-multiRangeSliderSpan">
              {max}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MultiRangeSlider;
