const Bubble = ({ value, position }) => {
  return (
    <div className="relative w-52 ml-[0.85rem]">
      <span
        className="absolute -top-4 z-1 py-1 px-2 text-multiRangeSliderBubble bg-secondaryLight text-xs font-lato font-semibold rounded-md shadow-sm"
        style={{ left: position }}
      >
        {value}
      </span>
    </div>
  );
};

export default Bubble;
