import CollapseItem from '@components/CollapseItem';
import MultiRangeSlider from './MultiRangeSlider';

const filters = [
	{
		title: 'Fabricado no Brasil',
		content: [
			{ item: 'Sim', quantity: '6' },
			{ item: 'Não', quantity: '8' },
		],
	},
	{
		title: 'Valores',
		content: [
			{ item: 'Cruelty Free', quantity: '8' },
			{ item: 'Orgânico', quantity: '6' },
			{ item: 'Vegano', quantity: '8' },
		],
	},
	{
		title: 'Categoria',
		content: [
			{ item: 'Mercearia', quantity: '8' },
			{ item: 'Beleza e Bem-Estar', quantity: '7' },
			{ item: 'Casa e Decoração', quantity: '12' },
		],
	},
];

const Filters = () => {
	return (
		<div className="min-w-max my-6 mx-8 pt-6 px-3 py-2 border rounded-lg">
			<p className="font-lato font-black text-lg text-secondaryDark">Filtrar</p>

			<CollapseItem title="Preço" type="filters">
				<MultiRangeSlider />
			</CollapseItem>

			{filters.map((filter, index) => (
				<CollapseItem key={index} title={filter.title} type="filters">
					{Array.isArray(filter.content) &&
						filter.content.map((content, index) => (
							<div
								key={index}
								className="mb-4 flex items-center justify-between"
							>
								<label className="flex items-center">
									<input type="checkbox" className="w-6 h-6" />

									<span className="ml-4 text-lg text-basicDark">
										{content.item}
									</span>
								</label>

								<span className="text-lg text-basicDark ml-10">
									{`(${content.quantity})`}
								</span>
							</div>
						))}
				</CollapseItem>
			))}
		</div>
	);
};

export default Filters;
