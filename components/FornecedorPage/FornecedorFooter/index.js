import { useState } from "react";

import ChipsList from "@components/ChipsList";
import Dropdown from "@components/Dropdown";

import FornecedorBio from "./FornecedorBio";
import FornecedorPopUp from "./FornecedorPopUp";

const FornecedorFooter = () => {
  const [showDropdown, setShowDropdown] = useState(false);

  const handleToggleDropdown = () => {
    setShowDropdown(!showDropdown);
  };

  return (
    <div className="border-y border-inventaBordersColor py-8 px-6">
      <div className="mb-4">
        <FornecedorBio />
      </div>

      <div>
        <p className="flex items-center font-lato font-light text-base text-secondaryDark">
          <span className="material-icons-outlined">location_on</span>
          Localizado em Salvador, Bahia
        </p>
        <p className="flex items-center font-lato font-light text-base text-secondaryDark">
          <span className="material-icons-outlined">house</span>
          Produto entregue para todo o Brasil
        </p>
      </div>

      <div className="mt-5 border-b border-inventaBordersColor pb-6">
        <ChipsList />
      </div>

      <p className="mt-4 font-lato font-light text-sm text-secondaryDark">
        A We Nutz foi fundada pelas sócias Isabelle Holanda e Clarissa Rodrigues
        em janeiro de 2019. Nossa marca conta com três linhas de snacks
        saudáveis: à base de frutas e nuts, chocolates e pastas (de amendoim,
        amêndoas e castanhas).
      </p>

      <p
        className="font-lato cursor-pointer mt-2 text-xs leading-5 text-secondaryDark border-b border-secondaryDark w-fit"
        onClick={handleToggleDropdown}
      >
        Conheça nossa história
      </p>

      {showDropdown && (
        <Dropdown closeModal={handleToggleDropdown}>
          <FornecedorPopUp />
        </Dropdown>
      )}
    </div>
  );
};

export default FornecedorFooter;
