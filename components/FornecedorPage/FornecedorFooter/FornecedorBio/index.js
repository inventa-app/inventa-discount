import styles from './style.module.css';
import SocialIcons from '@components/SocialIcons';

const FornecedorBio = ({ type = 'footer' }) => {
	return (
		<div className="flex">
			<div>
				<div className={styles.logo} />
				{type === 'footer' && (
					<p className="font-noto text-lg leading-5 text-secondaryDark">
						We Nutz
					</p>
				)}
			</div>
			<div>
				{type === 'popUp' && (
					<p className="font-noto text-4xl leading-[3rem] text-secondaryDark">
						We Nutz
					</p>
				)}
				<SocialIcons />
			</div>
		</div>
	);
};

export default FornecedorBio;
