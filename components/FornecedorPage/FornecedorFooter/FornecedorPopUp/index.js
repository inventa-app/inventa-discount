import styles from './style.module.css';

import FornecedorBio from '../FornecedorBio';
import ChipsList from '@components/ChipsList';

const FornecedorPopUp = () => {
	return (
		<>
			<div className="mb-9">
				<FornecedorBio type="popUp" />
			</div>

			<div className={styles.video} />

			<h2 className="my-4 font-noto font-bold text-lg leading-5 text-secondaryDark">
				Tudo começou com vovó Dalva
			</h2>

			<p className="font-lato font-light text-sm text-secondaryDark">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ornare commodo
				lacinia pretium sed. Id sed lobortis nisi porta. Risus ante non
				malesuada ipsum eu eget nunc molestie. Elit massa integer vitae pulvinar
				sed nisl ac quis. Condimentum a nulla quam nunc, dignissim ultrices
				turpis. Tempus feugiat quam vestibulum imperdiet cum ultrices facilisis
				felis donec. Sit euismod massa tincidunt auctor eu. Pellentesque
				molestie neque, eu eget tincidunt a risus. Lectus libero vel ut neque,
				nunc sapien. Est turpis etiam enim, dignissim. Ut turpis mollis
				imperdiet erat adipiscing dolor dolor, porttitor.
			</p>

			<h3 className="mt-8 mb-4 text-2xl leading-6 text-black">
				Nossos valores
			</h3>

			<div className="mt-4 mb-7">
				<ChipsList />
			</div>
		</>
	);
};

export default FornecedorPopUp;
