import Breadcrumb from "@components/Breadcrumb";
import SocialIcons from "@components/SocialIcons";
import Image from "next/image";

const data = {
  banner:
    "https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg",
  logo: "https://images.tcdn.com.br/files/859628/themes/13/img/settings/we.jpg",
  name: "We Nutz",
  description:
    "A We Nutz foi fundada pelas sócias Isabelle Holanda e Clarissa Rodrigues em janeiro de 2019, . Nossa marca conta com três linhas de snacks saudáveis: à base de frutas e nuts, chocolates e pastas (de amendoim, amêndoas e castanhas).",
  total_products: 500,
  discount: "10% off na primera compra",
  days_to_ship: 1,
  ranking: 3,
  delivery_price: "free",
  delivery: "full",
};

const FornecedorHeader = () => {
  return (
    <div className="mx-8 my-6 mt-4">
      {/* <div className="mb-4">
        <Breadcrumb page="brand" name={data.name} />
      </div> */}
      <div className="px-12 my-2 h-60 flex relative items-center">
        <Image
          src={data.banner}
          alt="banner"
          layout="fill"
          objectFit="cover"
          className="z-0"
        />
        <Image
          src={data.logo}
          alt="logo"
          width={144}
          height={144}
          className="rounded z-10"
        />
      </div>
      <h2 className="text-6xl mb-6 font-noto text-secondaryDark">
        {data.name}
      </h2>
      <div className="flex">
        <div className="w-3/12">
          <SocialIcons />
          <div className="mt-4">
            {data.days_to_ship && (
              <div className="flex">
                <div className="w-5 flex justify-center mr-3">
                  <span className="material-icons-outlined text-xl text-secondaryDark">
                    flash_on
                  </span>
                </div>
                <p className="font-light text-base font-lato text-secondaryDark">
                  Envio em
                  {data.days_to_ship === 1
                    ? " 24hs"
                    : ` ${data.days_to_ship} dias`}
                </p>
              </div>
            )}
            {data.ranking && (
              <div className="flex">
                <div className="w-5 flex justify-center mr-3">
                  <span className="material-icons-outlined text-xl text-secondaryDark">
                    trending_up
                  </span>
                </div>
                <p className="font-light text-base font-lato text-secondaryDark">
                  {" "}
                  Top {data.ranking} en satisfação{" "}
                </p>
              </div>
            )}
            {data.delivery_price === "free" && (
              <div className="flex">
                <div className="w-5 flex justify-center mr-3">
                  <span className="material-icons-outlined text-xl text-secondaryDark">
                    local_shipping
                  </span>
                </div>
                <p className="font-light text-base font-lato text-secondaryDark">
                  Frete grátis
                </p>
              </div>
            )}
            {data.delivery && (
              <div className="flex">
                <div className="w-5 flex justify-center mr-3">
                  <span className="material-icons-outlined text-xl text-secondaryDark">
                    home
                  </span>
                </div>
                <p className="font-light text-base font-lato text-secondaryDark">
                  {data.delivery === "full"
                    ? " Produto entregue para todo o Brasil"
                    : data.delivery}
                </p>
              </div>
            )}
          </div>
        </div>
        <div className="w-9/12 mx-20">
          <p className="text-lg font-lato font-light text-secondaryDark">
            {data.description}
          </p>
          <a className="text-lg font-lato underline hover:cursor-pointer text-secondaryDark">
            Conheça nossa história
          </a>
        </div>
      </div>
    </div>
  );
};

export default FornecedorHeader;
