import { useState } from "react";
import Dropdown from '@components/Dropdown';
import DiscountsPopUp from './DiscountsPopUp';
import Image from 'next/image'

const data={
    banner:'https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg',
    logo:'https://images.tcdn.com.br/files/859628/themes/13/img/settings/we.jpg',
    name:'We Nutz',
    total_products:500,
    discount:'10% off na primera compra',
}

const FornecedorHeader = () => {
    const [showDropdown, setShowDropdown] = useState(false);

    const handleToggleDropdown = () => {
        setShowDropdown(!showDropdown);
    };

    return (
        <div className='block'>
            <div className='w-full h-36 relative'>
                <Image
                    src={data.banner}
                    alt='banner'
                    layout='fill'
                    className="object-cover"
                />
            </div>
            <div className='flex justify-left space-between mx-4 my-2'>
                <Image src={data.logo} alt='logo' height={80} width={80} className="rounded"/>
                <div className="ml-3.5 grow">
                    <p className="text-2xl font-noto text-secondaryDark">{data.name}</p>
                    <div className="flex grow justify-evenly mt-2.5">
                        <div>
                            <span className="material-icons-outlined text-center text-secondaryLight block">storefront</span>
                            <p className="text-xs mt-2 font-noto text-secondaryDark text-center">{`+${data.total_products} produtos`}</p>
                        </div>
                        <div>
                            <span className="material-icons-outlined text-center text-secondaryLight block" >history_edu</span>
                            <p className="text-xs mt-2 font-noto text-secondaryDark text-center"> Nossa história </p>
                        </div>
                    </div>
                </div>
            </div>
            {data.discount && (
                <a className="flex justify-center items-center mt-5 bg-primaryLight bg-opacity-40 center mx-4 my-2 py-1 rounded cursor-pointer" href="#" onClick={handleToggleDropdown}>
                    <span className="material-icons-outlined text-sm">discount</span>
                    <p className="text-sm text-center font-lato mx-1">{data.discount}</p>
                    <span className="material-icons-outlined text-xs">arrow_forward_ios</span>
                </a>
            )}
            {showDropdown && (
                <Dropdown closeModal={handleToggleDropdown}>
                    <DiscountsPopUp />
                </Dropdown>
            )}
        </ div>
    );
}
 
export default FornecedorHeader;
