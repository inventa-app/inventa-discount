import CollapseItem from "@components/CollapseItem";

const faqs = [
  {
    title: "Como faço pra comprar na Inventa",
    content:
      "Basta inserir seus dados (CNPJ, telefone, email e seu nome) no formulário no início desta página",
  },
  {
    title: "Como faço pra vender na Inventa",
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    title: "Como funciona o pagamento?",
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    title: "A Inventa atende em território brasileiro?",
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
];

const padNumber = (number) => {
  return number < 10 ? `0${number}` : number;
};

const FAQ = () => {
  return (
    <>
      <h3 className="my-16 ml-8 font-noto text-secondaryDark text-4xl leading-[120%]">
        Dúvidas Frequentes
      </h3>
      <ol>
        {faqs.map((faq, index) => (
          <li
            key={index}
            className="px-8 mb-3 pt-6 pb-9 flex items-start border-b-2 border-[#CDD6DA] border-opacity-25 "
          >
            <p className="mt-1 mr-12 font-lato font-bold text-4xl leading-[120%] tracking-[0.016rem] text-[#3C3C43] opacity-50">
              {padNumber(index + 1)}
            </p>
            <div className="w-full">
              <CollapseItem title={faq.title} type="faq">
                <p className="font-noto font-light text-lg leading-5 text-secondaryLight">
                  {faq.content}
                </p>
              </CollapseItem>
            </div>
          </li>
        ))}
      </ol>
    </>
  );
};

export default FAQ;
