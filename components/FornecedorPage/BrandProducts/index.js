import ProductCard from "@components/ProductCard";
import ProducstList from "./mock_data";

const BrandProducts = () => {
  return (
    <ul className="flex mb-4 flex-wrap justify-center gap-y-4 gap-x-4">
      {ProducstList.map((product) => {
        return (
          <ProductCard
            userLogged={true}
            key={product.id}
            title={product.title}
            minimal_order={product.minimal_order}
            image={product.image}
            price={product.price}
            minimal_quant={product.minimal_quant}
            discount={product.discount}
          />
        );
      })}
    </ul>
  );
};

export default BrandProducts;
