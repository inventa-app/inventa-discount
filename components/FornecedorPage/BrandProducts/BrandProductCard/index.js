import SeePriceButton from "../SeePriceButton";
import AddProduct from "../AddProductComponent";
import { useContext } from "react";
import { UserContext } from "@services/contexts/UserContext";

import Image from "next/image";
import { QuantityComp } from "@components/QuantityComponent";
const BrandProductCard = ({
  title,
  minimal_order,
  image,
  price,
  minimal_quant,
  discount,
}) => {
  // const { auth } = useContext(UserContext);
  const auth = true;
  return (
    <li className="w-40">
      <Image
        src={image}
        alt={title}
        width={170}
        height={170}
        className="border-gray-100"
      />

      {auth ? (
        <h2 className="text-['#666666'] text-xs font-thin">
          {title} | Min R$
          {minimal_order.toFixed(2).toString().replace(".", ",")}
        </h2>
      ) : (
        <h2 className="text-['#666666'] text-xs font-thin">{title}</h2>
      )}

      <span className="flex">
        {auth ? (
          <h3 className="flex text-lg">
            R${price.toFixed(2).toString().replace(".", ",")}
          </h3>
        ) : (
          <h3 className="text-lg whitespace-nowrap">
            R$
            <span className="material-icons-outlined">lock</span>
          </h3>
        )}
        {discount > 0 && (
          <p className="text-xs text-red-500 ml-2 mt-0.5">{discount}% off</p>
        )}
      </span>
      <p className="text-xs font-thin ">
        Quantidade mínima {minimal_quant} und
      </p>
      {/* {auth ? <AddProduct minimal_quant={minimal_quant} /> : <SeePriceButton />} */}
      {auth ? <QuantityComp /> : <SeePriceButton />}
    </li>
  );
};

export default BrandProductCard;
