import { useContext } from "react";
import { UserContext } from "@services/contexts/UserContext";

import WhatsAppButton from "@components/WhatsAppButton";
import Footer from "./innerComponents/Footer";
import Header from "./innerComponents/Header";
import Notification from "@components/Notification";

import Container from "@components/Container";
import BottomNavbar from "@components/BottomNavbar/BottomNavbar";

const Layout = ({ children, menuItems, user, notification, footerItems }) => {
  const auth = user && user.user;
  const { clientWidth } = useContext(UserContext);

  return (
    <>
      <div className="fixed top-0 left-0 w-full z-50 mb-40">
        <Notification notification={notification} />
        <Header
          userLogged={auth && user.first_name ? user.first_name : false}
          menuItems={menuItems}
        />
      </div>
      <main className="mt-28">
        <Container>
          {children} {clientWidth <= 768 ? <BottomNavbar auth={auth} /> : null}
        </Container>
      </main>
      <Footer footerItems={footerItems} />
      <WhatsAppButton />
    </>
  );
};

export default Layout;
