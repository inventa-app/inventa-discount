import CollapseItem from "@components/CollapseItem";

const Footer = () => {
  return (
    <footer className="bg-basicLight">
      <div className="flex items-center justify-center flex-col p-10 lg:py-16 lg:px-32 xl:px-40">
        <div className="flex justify-between flex-col lg:flex-row lg:flex-wrap w-full">
          <div className="leading-loose mr-10">
            <a
              href="https://inventa.shop/"
              className="text-black font-extralight lg:text-5xl text-3xl tracking-widest"
            >
              INVENTA
            </a>
            <ul>
              <li className="text-grayText text-sm">@ 2022</li>
              <li>
                <a
                  href="https://inventa.shop/pages/politica-de-privacidade"
                  className="hover:text-primaryYellow text-grayText text-sm"
                >
                  Política de privacidades
                </a>
              </li>
              <li>
                <a
                  href="https://inventa.shop/pages/termos-e-condicoes-lojistas"
                  className="hover:text-primaryYellow text-grayText text-sm"
                >
                  Termos e condições
                </a>
              </li>
            </ul>
          </div>

          <CollapseItem title="Produtos" type="footer">
            <ul>
              <li>
                <a
                  href="https://inventa.shop/pages/fornecedor"
                  className="hover:text-primaryYellow text-grayText text-sm"
                >
                  Fornecedor
                </a>
              </li>
              <li>
                <a
                  href="https://inventa.shop/#beneficios-inventa"
                  className="hover:text-primaryYellow text-grayText text-sm"
                >
                  Varejista
                </a>
              </li>
            </ul>
          </CollapseItem>

          <CollapseItem title="Inventa" type="footer">
            <ul>
              <li>
                <a
                  href="https://inventa.shop/pages/sobre-inventa"
                  className="hover:text-primaryYellow text-grayText text-sm"
                >
                  Sobre
                </a>
              </li>
              <li>
                <a
                  href="https://inventa.shop/"
                  className="hover:text-primaryYellow text-grayText text-sm"
                >
                  Missão
                </a>
              </li>
              <li>
                <a
                  href="https://inventa.shop/"
                  className="hover:text-primaryYellow text-grayText text-sm"
                >
                  Oportunidades
                </a>
              </li>
            </ul>
          </CollapseItem>

          <CollapseItem title="Suporte" type="footer">
            <ul>
              <li>
                <a
                  href="https://inventa.shop/"
                  className="hover:text-primaryYellow text-grayText text-sm"
                >
                  FAQ
                </a>
              </li>
              <li>
                <a
                  href="https://inventa.shop/pages/fornecedor#faq"
                  className="hover:text-primaryYellow text-grayText text-sm"
                >
                  Ajuda Fornecedor
                </a>
              </li>
              <li>
                <a
                  href="https://inventa.shop/#faq"
                  className="hover:text-primaryYellow text-grayText text-sm"
                >
                  Ajuda Varejista
                </a>
              </li>
            </ul>
          </CollapseItem>

          <CollapseItem title="Acompanhe" type="footer">
            <ul>
              <li>
                <a
                  href="https://www.linkedin.com/company/inventashop/"
                  className="hover:text-primaryYellow text-grayText text-sm"
                >
                  LinkedIn
                </a>
              </li>
              <li>
                <a
                  href="https://www.instagram.com/inventa.shop"
                  className="hover:text-primaryYellow text-grayText text-sm"
                >
                  Instagram
                </a>
              </li>
              <li>
                <a
                  href="https://www.facebook.com/inventa.shop"
                  className="hover:text-primaryYellow text-grayText text-sm"
                >
                  Facebook
                </a>
              </li>
            </ul>
          </CollapseItem>
        </div>
        <div className="flex mt-10 w-full">
          <p className="text-xs text-grayText">
            Inventa App Ltda - CNPJ: 41.356.168/0001-11 | Inscrição Estadual:
            131.409.133.112 | R Jose Maria Lisboa, 880, 01423-912 |{" "}
            <a
              href="mailto:contato@inventa.shop"
              className="hover:text-primaryYellow"
            >
              contato@inventa.shop
            </a>
          </p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
