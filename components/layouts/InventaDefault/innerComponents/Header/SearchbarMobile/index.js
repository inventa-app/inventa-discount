import { useState } from "react";
import Image from "next/image";

import { LEGACY_SITE } from "@constants/index";
import Aside from "@components/Aside";

const Searchbar = () => {
  const [showSearch, setShowSearch] = useState(false);

  let searchHistory = [
    "Glory by nature",
    "Chocolate",
    "Em promoção",
    "Chocolate",
    "Em promoção",
  ];

  let recentHistory = [
    {
      name: "Chocolate brancho recheado 150g",
      price: "50,00",
      brand: "We Nutz",
      image:
        "https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg",
    },
    {
      name: "Chocolate brancho recheado 150g",
      price: "50,00",
      brand: "We Nutz",
      image:
        "https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg",
    },
    {
      name: "Chocolate brancho recheado 150g",
      price: "50,00",
      brand: "We Nutz",
      image:
        "https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg",
    },
    {
      name: "Chocolate brancho recheado 150g",
      price: "50,00",
      brand: "We Nutz",
      image:
        "https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg",
    },
  ];

  let categories = [
    {
      name: "Mercearia",
      image:
        "https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg",
    },
    {
      name: "Cervejas",
      image:
        "https://images.tcdn.com.br/files/859628/themes/13/img/settings/we.jpg",
    },
    {
      name: "Chocolates",
      image:
        "https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg",
    },
  ];

  const displaySearch = () => {
    setShowSearch(!showSearch);
  };

  return (
    <>
      <span
        className="material-icons-outlined flex lg:hidden"
        onClick={displaySearch}
      >
        search
      </span>
      <Aside
        display={showSearch}
        position="right"
        timing="ease-in-out"
        duration="duration-500"
      >
        <div className="flex">
          <form
            action={LEGACY_SITE + "/search"}
            method="GET"
            role="search"
            className={`w-full flex justify-end`}
          >
            <span
              className={`material-icons-outlined text-basicDark lg:hidden -mr-6 z-10 items-center text-base flex`}
            >
              search
            </span>
            <input
              type="search"
              name="search"
              placeholder="Procurar por marca, produto ou categoria"
              className={`border-2 border-basicMedium rounded-lg pl-8 pr-2 text-sm focus:outline-none text-black h-full placeholder:font-noto placeholder:text-secondaryLight placeholder:text-xs py-1.5 w-full`}
            />
          </form>
          <button
            onClick={displaySearch}
            className="text-black text-xs font-noto ml-8"
          >
            Cancelar
          </button>
        </div>
        <p className="text-noto font-light text-lg mt-4 mb-2">
          Buscado recentemente
        </p>
        <div className="flex flex-wrap">
          {searchHistory.map((item, index) => (
            <p key={`${index}-${item}`} className="text-xs font-lato mr-4 my-1">
              <span className="text-secondaryLight text-xs">x</span>
              {item}
            </p>
          ))}
        </div>
        <p className="text-noto font-light text-lg mt-4 mb-2">Mais buscados</p>
        <div className="flex flex-wrap">
          {categories.map((item, index) => (
            <div
              className="flex rounded-3xl bg-basicMedium p-2 m-1"
              key={`${index}-${item.name}`}
            >
              <Image
                src={item.image}
                alt={item.title}
                width={18}
                height={18}
                className="rounded-full"
              />
              <p className="text-xs font-noto mx-2 text-inventaGrayText">
                {item.name}
              </p>
            </div>
          ))}
        </div>
        <p className="text-noto font-light text-lg mt-4 mb-2">
          Visto recentemente
        </p>
        <div className="flex flex-col overflow-auto h-2/6">
          {recentHistory.map((item, index) => (
            <div className="flex p-1 m-1" key={`${index}-${item.name}`}>
              <Image
                src={item.image}
                alt={item.title}
                width={56}
                height={56}
                className="border rounded-md border-inventaGrayText"
              />
              <div>
                <p className="px-2 text-sm font-lato font-bold">{item.name}</p>
                <p className="px-2 text-xs font-lato">
                  {`R\$ ${item.price} ${item.brand}`}
                </p>
              </div>
            </div>
          ))}
        </div>
        <div className="w-full bg-basicMedium h-0.5 block my-4 mx-auto" />
        <p className="text-sm font-lato font-bold my-3 text-center underline hover:cursor-pointer">
          Ganhe 20% off na primera compra
        </p>
      </Aside>
    </>
  );
};

export default Searchbar;
