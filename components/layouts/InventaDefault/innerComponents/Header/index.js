import { useState, useEffect } from "react";
import { listenEvent, triggerEvent } from "@services/utils";
import { useContext } from "react";
import { UserContext } from "@services/contexts/UserContext";

import Searchbar from "./SearchbarMobile";
import CreditPopUp from "@components/CreditPopUp";
import Aside from "@components/Aside";
import Badge from "@components/Badge";
import HeaderMenu from "@components/HeaderMenu";
import FloatingMenu from "@components/FloatingMenu";
import Container from "@components/Container";
import Redirect from "@components/Redirect";

const Header = ({ userLogged, menuItems }) => {
  const [show, setShow] = useState(false);
  const { clientWidth, cartCount } = useContext(UserContext);
  const LEGACY_SITE = process.env.LEGACY_SITE;

  const [showSearch, setShowSearch] = useState(false);

  let asideItems = [
    { label: "Home", link: "https://inventa.shop/" },
    { label: "Com envios gratuitos", link: "#" },
    { label: "Promoções", link: "#" },
    { label: "Novos produtos", link: "#" },
    { label: "Todas as marcas", link: "#" },
    { label: "Todos os produtos", link: "#" },
    { label: "Cosméticos", link: "#" },
    { label: "Mercearia", link: "#" },
    { label: "Casa e Decoração", link: "#" },
  ];

  let profileItems = [
    { label: "Meus pedidos", link: "/account" },
    { label: "Endereços", link: "/account/addresses" },
    { label: "Crédito", link: "#" },
    { label: "Sair", link: "/api/account/logout" },
  ];

  const [showCreditPopUp, setShowCreditPopUp] = useState(false);

  const handleToggleCreditPopUp = () => {
    setShowCreditPopUp(!showCreditPopUp);
  };

  const handleProfileItemClick = (label) => {
    if (label === "Crédito") {
      handleToggleCreditPopUp();
    }
  };

  const toggleSearch = () => {
    setShowSearch(!showSearch);
  };

  useEffect(() => {
    listenEvent("asidebar:open", () => {
      if (!show) setShow(true);
    });
    listenEvent("asidebar:close", () => {
      setShow(false);
    });
    listenEvent("credit-popup:open", () => {
      setShowCreditPopUp(true);
    });
  }, []);

  const [cartChange, setCartChange] = useState(cartCount());
  useEffect(() => {
    if (typeof cartChange !== "boolean" && cartCount() !== cartChange) {
      setCartChange(true);
      setInterval(() => {
        setCartChange(cartCount());
      }, 3000);
    }
  });

  return (
    <>
      <header className="relative z-50">
        {/* NAVBAR */}
        <div className="bg-secondaryDark">
          <Container
            tailwindClass={`w-full h-[4.3125rem] flex flex-row relative items-center justify-between px-1 md:px-10 py-4 text-black lg:text-white`}
          >
            {/*<div className="lg:hidden">
              <button
                onClick={(e) => {
                  e.preventDefault();
                  triggerEvent("asidebar:open");
                }}
                className="focus:outline-none"
              >
                <span className="material-icons-outlined">menu</span>
              </button>
            </div> */}
                <Redirect
                      href="https://inventa.shop/"
                      /* className="hover:text-primaryYellow text-[15px] font-light" */
                    >
                      <span className="text-white ml-5 md:ml-0 md:flex tracking-widest py-1 md:text-4xl text-xl font-logo-inventa hover:text-primaryYellow">INVENTA</span>
                    </Redirect>
            <div className="flex flex-row items-center space-x-4 xl:space-x-18 xxl:space-x-12 xl:pr-5 mr-3 md:mr-0">
              {!userLogged && (
                <div className="flex-col hidden lg:flex text-lg font-lato leading-6 mx-5">
                  <div className="flex flex-row items-center">
                    <a
                      href="/account/login"
                      onClick={(e) => {
                        e.preventDefault();
                        triggerEvent("modal_open:login");
                      }}
                      className="hover:text-primaryYellow text-[15px] font-light"
                    >
                      <span>Entrar</span>
                    </a>
                    <span className="text-base font-light">
                      {" "}
                      &nbsp;/&nbsp;{" "}
                    </span>
                    <Redirect
                      href="https://inventa.shop/"
                      className="hover:text-primaryYellow text-[15px] font-light"
                    >
                      <span>Criar conta</span>
                    </Redirect>
                  </div>
                </div>
              )}
              {userLogged && (
                <div className="flex-col hidden lg:flex text-sm mx-5">
                  <p className="ml-3 mb-0.5 font-system font-light">
                    Olá{" "}
                    {userLogged && typeof userLogged === "string"
                      ? userLogged.substring(0, 17)
                      : ""}
                  </p>
                  <FloatingMenu
                    arrow="right"
                    clickable={
                      <div className="bg-inventaGrayText px-3 py-0.5 rounded-2xl space-x-2 flex flex-row">
                        <p className="text-white font-system">
                          <span className="mr-3 text-[16px]">Meus Pedidos</span>
                          <span className="material-icons-outlined text-[20px] font-bold absolute right-0 mt-0 top-1/2 -translate-y-1/2 text-white">
                            expand_more
                          </span>
                        </p>
                      </div>
                    }
                  >
                    {profileItems.map((item, index) => {
                      if (item.label == "Sair")
                        return (
                          <a
                            className="py-1 px-5 hover:bg-basicMedium hover:text-primary hover:cursor-pointer last:rounded-b first:pt-2 last:pb-2 first:rounded-t font-lato font-bold text-secondaryDark text-sm"
                            href={item.link}
                            key={`${index}-${item.label}`}
                          >
                            {item.label}
                          </a>
                        );
                      else if (item.label === "Crédito")
                        return (
                          <a
                            className="py-1 px-5 hover:bg-basicMedium hover:text-primary hover:cursor-pointer last:rounded-b first:pt-2 last:pb-2 first:rounded-t font-lato font-bold text-secondaryDark text-sm"
                            href={item.link}
                            key={`${index}-${item.label}`}
                            onClick={() => handleProfileItemClick(item.label)}
                          >
                            {item.label}
                          </a>
                        );
                      else
                        return (
                          <Redirect
                            key={`${index}-${item.label}`}
                            href={item.link}
                            className="py-1 px-5 hover:bg-basicMedium hover:text-primary hover:cursor-pointer last:rounded-b first:pt-2 last:pb-2 first:rounded-t font-lato font-bold text-secondaryDark text-sm"
                          >
                            {item.label}
                          </Redirect>
                        );
                    })}
                  </FloatingMenu>
                </div>
              )}
              {/* <Searchbar /> */}
              {showSearch ? (
                <span
                  className="material-icons-outlined lg:hidden"
                  onClick={() => toggleSearch()}
                >
                  close
                </span>
              ) : (
                <></>
              )}

              <Redirect href="https://inventa.shop" className="flex cursor-pointer">
                <Badge
                  size={clientWidth >= 1024 ? "md" : "md"}
                  value={cartCount()}
                  className={
                    typeof cartChange === "boolean" && cartChange
                      ? " motion-safe:animate-bounce"
                      : ""
                  }
                >
                  <span className="material-icons-outlined lock-icon text-white">
                    shopping_cart
                  </span>
                </Badge>
                <span className="hidden text-white md:inline-block pl-4 mt-2 text-base font-lato">
                  {" "}
                  Carrinho
                </span>
              </Redirect>
            </div>
          </Container>
        </div>
        <HeaderMenu menuItems={menuItems} />
        {/* SIDEBAR */}
        {show && (
          <Aside
            display={show}
            position="left"
            timing="ease-in-out"
            duration="duration-500"
            className="p-7 space-y-6"
          >
            <div className="flex justify-between items-center ">
              <div className="flex items-center space-x-2">
                <span className="material-icons-outlined">person_outline</span>
                {/* <p className="text-lg">Cadastre-se</p> */}

                {userLogged ? (
                  <p>
                    Olá{" "}
                    {userLogged && typeof userLogged === "string"
                      ? userLogged.substring(0, 17)
                      : ""}
                  </p>
                ) : (
                  <p className="text-lg">Cadastre-se</p>
                )}
              </div>
              <div>
                <button
                  className="text-2xl font-thin focus:outline-none"
                  onClick={() => {
                    triggerEvent("asidebar:close");
                  }}
                >
                  x
                </button>
              </div>
            </div>
            {/*<div className="flex items-center bg-gray-100 rounded p-4 space-x-2">
              <span className="material-icons-outlined">monetization_on</span>
              <p className="text-sm">Obter 10% off na primera compra</p>
            </div>*/}
            <ul className="space-y-2">
              {asideItems.map((item, index) => (
                <li key={`${index}-${item.label}`}>
                  <Redirect href={item.link}>{item.label}</Redirect>
                </li>
              ))}
            </ul>
            <div className="border-t pt-6">
              {userLogged ? (
                <a href="/api/account/logout">Sair</a>
              ) : (
                <ul className="space-y-2">
                  <li>
                    <a
                      href="/account/login"
                      onClick={(e) => {
                        e.preventDefault();
                        triggerEvent("modal_open:login");
                      }}
                    >
                      Login
                    </a>
                  </li>
                  <li>
                    <Redirect href="https://inventa.shop/">Cadastrar-se</Redirect>
                  </li>
                  <li>
                    <Redirect href="/pages/fornecedor">
                      Ser um Fornecedor
                    </Redirect>
                  </li>
                </ul>
              )}
            </div>
          </Aside>
        )}
      </header>

      {showCreditPopUp && <CreditPopUp toggle={handleToggleCreditPopUp} />}
    </>
  );
};

export default Header;
