import { useEffect, useContext, useState } from "react";
import Spinner from "@components/Spinner";
import { validEmail, validPhone } from "@services/utils";
import { UserContext } from "@services/contexts/UserContext";
import { data } from "autoprefixer";
import { ResetPassword } from "@services/shopify-requests";

const Login = ({ changeForm }) => {
  const { userLogin, sendLoginCode } = useContext(UserContext);
  const [loading, setLoading] = useState(false);
  const [loginCreds, setLoginCreds] = useState({
    user: "",
    password: "",
    codeUser: "",
    error: "",
    code: "",
  });
  const [codeForm, setCodeForm] = useState(false);

  const loginButton = {};
  const codeButton = {};
  const sentCodeButton = {};

  useEffect(() => {
    if (loginCreds.error && loginCreds.error.form === "code")
      setCodeForm(false);
  }, [loginCreds]);

  if (!validEmail(loginCreds.user) || loginCreds.password.length < 1)
    loginButton.disabled = true;
  if (!validEmail(loginCreds.codeUser) && !validPhone(loginCreds.codeUser))
    codeButton.disabled = true;
  if (loginCreds.code.length !== 4) sentCodeButton.disabled = true;

  const handleChange = (value, name) =>
    setLoginCreds({ ...loginCreds, error: "", [name]: value });

  const handleLogin = (e) => {
    e.preventDefault();
    if (
      !loading &&
      validEmail(loginCreds.user) &&
      loginCreds.password.length > 0
    ) {
      setLoading("login");
      userLogin(loginCreds.user, loginCreds.password).then(({ ok }) => {
        if (!ok) {
          handleChange(
            { form: "login", message: "E-mail ou senha incorretos." },
            "error"
          );
          setLoading(false);
        }
      });
    }
  };

  const loginChallenge = ({ ok, triggerFunction, err, attempts }) => {
    if (attempts === 0) handleChange("", "code");
    setLoading(false);
    if (ok) {
      if (attempts > 0)
        handleChange(
          { form: "sent-code", message: "Código inválido." },
          "error"
        );
      if (typeof triggerFunction === "function")
        setCodeForm({
          ok: true,
          try_f: triggerFunction,
        });
    } else {
      handleChange(
        {
          form: "code",
          message: "Código incorreto. Solicite um novo código.",
        },
        "error"
      );
    }
  };

  const handleStartLoginCode = (e) => {
    e.preventDefault();
    if (validEmail(loginCreds.codeUser) || validPhone(loginCreds.codeUser)) {
      setLoading("code");
      sendLoginCode(loginCreds.codeUser, loginChallenge);
    }
  };

  const handleTryCode = (e) => {
    e.preventDefault();
    if (loginCreds.code && loginCreds.code.length === 4) {
      setLoading("sent-code");
      codeForm.try_f(loginCreds.code);
    }
  };

  if (codeForm)
    return (
      <div className="py-6 px-12 w-full">
        <h4 className="text-2xl font-bold font-poppins mb-4">
          Código de verificação
        </h4>
        <form onSubmit={handleTryCode} action="/">
          {loginCreds.error && loginCreds.error.form === "sent-code" && (
            <p className="text-formErrorColor bg-formErrorColor/[0.07] mb-3 px-5 py-3 font-systemUi">
              {loginCreds.error.message}
            </p>
          )}
          <input
            className="w-full text-center text-4xl font-systemUi py-3 tracking-[20px] border-slate-200 border"
            onChange={(e) => handleChange(e.target.value, "code")}
            placeholder="0000"
            value={loginCreds.code}
            maxLength="4"
            name="customer[code]"
            type="text"
            onKeyDown={(e) => {
              if (
                isNaN(e.key) &&
                e.key != "Backspace" &&
                e.key != "Enter" &&
                e.key != "Delete"
              )
                e.preventDefault();
            }}
          />
          <button
            {...sentCodeButton}
            type="submit"
            className="text-center transition duration-700 disabled:text-black/30 mt-3 text-lg font-bold font-poppins py-6 rounded w-full bg-inventaYellow"
          >
            {loading === "sent-code" ? (
              <Spinner background="black" fill="gray" />
            ) : (
              "Validar código"
            )}
          </button>
        </form>
        <p className="text-center my-5 text-xl">
          ou{" "}
          <span
            className="font-bold cursor-pointer"
            onClick={() => setCodeForm(false)}
          >
            Utilize senha para entrar
          </span>
        </p>
      </div>
    );

  return (
    <div className="py-6 px-12 w-full">
      <h4 className="text-2xl font-bold font-poppins mb-4">
        Entrar como Lojista
      </h4>
      <form
        action={process.env.LEGACY_SITE + "/account/login"}
        method="POST"
        onSubmit={handleLogin}
      >
        {loginCreds.error && loginCreds.error.form === "login" && (
          <p className="text-formErrorColor bg-formErrorColor/[0.07] mb-3 px-5 py-3 font-systemUi">
            {loginCreds.error.message}
          </p>
        )}
        <div className="relative w-full mb-3">
          <input
            name="customer[email]"
            type="email"
            value={loginCreds.user}
            required="required"
            autoFocus
            className="p-4 w-full border-slate-200 border"
            onChange={(e) => handleChange(e.target.value, "user")}
          />
          <label
            className="absolute text-xs top-0.5 left-3"
            htmlFor="customer[email]"
          >
            E-mail
          </label>
        </div>
        <div className="relative w-full">
          <input
            name="customer[password]"
            required="required"
            autoComplete="current-password"
            type="password"
            className="p-4 w-full border-slate-200 border"
            onChange={(e) => handleChange(e.target.value, "password")}
          />
          <label
            className="absolute text-xs top-0.5 left-3"
            htmlFor="customer[password]"
          >
            Senha
          </label>
        </div>
        <small
          className="underline hover:text-primaryYellow cursor-pointer"
          onClick={() => changeForm("ForgotPassword")}
        >
          Esqueci a senha
        </small>
        <button
          type="submit"
          className="text-center transition duration-700 disabled:text-black/30 mt-3 text-lg font-bold font-poppins py-4 rounded w-full bg-inventaYellow"
          {...loginButton}
        >
          {loading == "login" ? (
            <Spinner background="black" fill="gray" />
          ) : (
            "Entrar com senha"
          )}
        </button>
      </form>
      <p className="my-4 text-center text-lg">ou</p>
      <form onSubmit={handleStartLoginCode} action="/">
        {loginCreds.error && loginCreds.error.form === "code" && (
          <p className="text-formErrorColor bg-formErrorColor/[0.07] mb-3 px-5 py-3 font-systemUi">
            {loginCreds.error.message}
          </p>
        )}
        <div className="relative w-full">
          <input
            value={loginCreds.codeUser}
            name="customer[code-email]"
            required="required"
            className="p-4 w-full border-slate-200 border mb-3"
            onChange={(e) => handleChange(e.target.value, "codeUser")}
          />
          <label
            className="absolute text-xs top-0.5 left-3"
            htmlFor="customer[code-email]"
          >
            Telefone (com DDD) ou Email
          </label>
        </div>
        <button
          className="text-center transition duration-700 disabled:text-white/30 text-white text-lg font-bold font-poppins py-4 rounded w-full bg-black"
          {...codeButton}
        >
          {loading == "code" ? (
            <Spinner background="black" fill="gray" />
          ) : (
            "Entrar com código"
          )}
        </button>
      </form>
    </div>
  );
};

const ForgotPassword = ({ changeForm }) => {
  const [email, setEmail] = useState();
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState();

  const resetButton = {};
  if (!validEmail(email)) resetButton.disabled = true;

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validEmail(email) && !loading) {
      setMessage(false);
      setLoading(true);
      try {
        ResetPassword(email)
          .then((res) => {
            setMessage({
              message:
                "Enviamos um e-mail com instruções para redefinir a senha.",
              color: "formSuccessColor",
            });
            setLoading(false);
          })
          .catch((err) => {
            setMessage({
              message: "Algo deu errado, tente mais tarde por favor",
              color: "formErrorColor",
            });
            setLoading(false);
          });
      } catch (error) {
        setMessage({
          message: "Algo deu errado, tente mais tarde por favor",
          color: "formErrorColor",
        });
        setLoading(false);
      }
      setEmail("");
    }
  };

  return (
    <div className="py-6 px-12 w-full">
      <h4 className="text-2xl font-bold font-poppins mb-4">Esqueci a senha</h4>
      {message && (
        <p
          className={`bg-${message.color}/[0.07] text-${message.color} mb-3 px-5 py-3 font-systemUi`}
        >
          {message.message}
        </p>
      )}
      <form
        action={process.env.LEGACY_SITE + "/account/recover"}
        method="POST"
        onSubmit={handleSubmit}
      >
        <input
          type="hidden"
          name="form_type"
          value="recover_customer_password"
        />
        <input type="hidden" name="utf8" value="✓" />
        <div className="relative w-full">
          <input
            value={email}
            name="email"
            type="email"
            required="required"
            className="p-4 w-full border-slate-200 border mb-3"
            onChange={(e) => setEmail(e.target.value)}
          />
          <label className="absolute text-xs top-0.5 left-3" htmlFor="email">
            E-mail
          </label>
        </div>
        <button
          type="submit"
          className="text-center transition duration-700 disabled:text-black/30 mt-3 text-lg font-bold font-poppins py-4 rounded w-full bg-inventaYellow"
          {...resetButton}
        >
          {loading ? <Spinner background="black" fill="gray" /> : "Recuperar"}
        </button>
      </form>
      <p className="mt-5">
        Lembrou a sua senha?{" "}
        <span
          className="underline cursor-pointer"
          onClick={() => changeForm("Login")}
        >
          Voltar para o login
        </span>
      </p>
    </div>
  );
};

module.exports = {
  Login,
  ForgotPassword,
};
