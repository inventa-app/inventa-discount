import { useContext, useState } from "react";
import { UserContext } from "@services/contexts/UserContext";
import Forms from "./Forms";
import Image from "next/image";
import { shopifyImagesLoader } from "@services/utils";
import Redirect from "@components/Redirect";

const banner_images = {
  "popup-mobile":
    "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/popup-NMKP-cropped.png",
  "popup-desktop":
    "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/popup-NMKP-cropped.png",
  "background-bg": "#a8cbde",
};

const Login = ({ closeFunction }) => {
  const { clientWidth } = useContext(UserContext);
  const [form, setForm] = useState("Login");

  const Form = Forms[form];
  return (
    <div className="md:flex h-[700px]">
      <div className="h-[420px] md:h-full md:w-[400px] md:max-w-[400px] bg-testAbBackground relative">
        <h1 className="p-5 md:mt-10 pl-10 mb-10 text-4xl font-noto text-white">O atacado online exclusivo para lojistas</h1>
        <Image
          className="custom-image w-auto md-w-100"
          alt="Banner image"
          src={
            clientWidth >= 768
              ? banner_images["popup-desktop"]
              : banner_images["popup-mobile"]
          }
          width={clientWidth >= 768 ? 400 : clientWidth}
          height={clientWidth >= 768 ? 507 : parseInt(clientWidth * 0.63)}
          quality={90}
          loading="eager"
        />
      </div>
      <div className="flex-1">
        {Form && <Form changeForm={setForm} />}
        <ToRegisterButton />
      </div>
    </div>
  );
};

export default Login;

function ToRegisterButton() {
  return (
    <div className="px-12 w-full pb-6">
      <h4 className="text-xl font-bold font-poppins mb-4 pt-6 border-t border-solid border-inventaBorderGray">
        Primeira vez na Inventa?
      </h4>
      <Redirect href="/">
        <p className="border-black border text-center mt-3 text-lg font-bold font-poppins py-4 rounded">
          Cadastre-se como Lojista
        </p>
      </Redirect>
    </div>
  );
}
