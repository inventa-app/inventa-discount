const MetaTags = ({ pageData = {} }) => (
  <>
    <meta name="theme-color" content="#ddaa2e" />
    <meta name="description" content={pageData.description} />
    <meta property="product:price:currency" content="BRL" />
    <link rel="shortcut icon" type="image/png" href="/favicon_96x96.png" />
    <link rel="icon" href="/favicon.ico" />

    <meta property="og:type" content={pageData.type} />
    <meta property="og:title" content={pageData.title} />
    <meta property="og:image" content={pageData.image} />
    <meta property="og:image:secure_url" content={pageData.image} />
    <meta property="og:description" content={pageData.description} />
    <meta property="og:url" content={pageData.url} />
    <meta property="og:site_name" content="Inventa" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content={pageData.title} />
    <meta name="twitter:description" content={pageData.description} />
    <meta name="twitter:image" content={pageData.image} />

    <meta charSet="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </>
);

export default MetaTags;
