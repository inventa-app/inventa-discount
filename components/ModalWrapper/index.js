import { useRef } from "react";

const ModalWrapper = ({ children, onClose, closeColor = "black" }) => {
  const modalWrapperRef = useRef(null);
  const handleClick = (e) => {
    if (
      !modalWrapperRef.current.innerHTML.includes(e.target.innerHTML) &&
      typeof onClose == "function"
    )
      onClose();
  };

  return (
    <div
      onClick={handleClick}
      className="z-50 fixed flex justify-center items-center top-0 left-0 w-screen h-screen fade-in bg-neutral-700/50"
    >
      <div
        ref={modalWrapperRef}
        className="bg-white relative w-full h-full md:w-3/4 md:h-fit md:min-w-[760px] md:min-h-[250px] md:max-h-[600px] xxl:max-h-[700px] md:max-w-[900px] overflow-y-auto"
      >
        <div
          className={`absolute z-10 px-2 right-0 text-3xl md:text-black text-${closeColor}`}
          onClick={() => {
            if (typeof onClose == "function") onClose();
          }}
        >
          <span className="material-icons-outlined cursor-pointer">close</span>
        </div>
        {children}
      </div>
    </div>
  );
};

export default ModalWrapper;
