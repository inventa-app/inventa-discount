import { useContext } from "react";
import { UserContext } from "@services/contexts/UserContext";
import Container from "@components/Container";
import CollapseItem from "@components/CollapseItem";

const Notification = ({ notification }) => {
  const { clientWidth } = useContext(UserContext);

  return (
    <div className="text-center px-2 py-3 bg-[#FFE2AA]">
      <Container>
        {notification ? (
          <div
            className="notification-banner"
            dangerouslySetInnerHTML={{ __html: notification }}
          ></div>
        ) : clientWidth >= 768 ? (
          <p className="text-sm">
            <span className="font-bold whitespace-nowrap">Frete grátis</span>{" "}
            para todo Brasil,{" "}
            <span className="font-bold whitespace-nowrap">20% de desconto</span>{" "}
            na primeira compra e{" "}
            <span className="font-bold whitespace-nowrap">
              parcelamento no cartão em 12x sem juros!
            </span>
          </p>
        ) : (
          <CollapseItem type="notification">
            <p className="text-sm">
              <span className="font-bold whitespace-nowrap">Frete grátis</span>,
              na primeira compra e{" "}
              <span className="font-bold whitespace-nowrap">
                12 parcelas sem juros!
              </span>
            </p>
          </CollapseItem>
        )}
      </Container>
    </div>
  );
};

export default Notification;
