const Tags = ['Cruelty Free', 'Orgânico', 'Vegano'];

const ChipsList = () => {
	return (
		<div className="flex items-center justify-between w-11/12 overflow-y-hidden">
			{Tags?.map((tag, index) => (
				<div
					key={`chip${index}`}
					className="flex items-center justify-center h-7 mr-1 py-2 px-4 rounded-full bg-inventaChipBg text-xs text-inventaGrayText whitespace-nowrap"
				>
					{tag}
				</div>
			))}
		</div>
	);
};

export default ChipsList;
