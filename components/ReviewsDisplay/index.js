import CommentReview from "@components/ReviewsDisplay/CommentReview";
import StarRating from "@components/ReviewsDisplay/StarRating";
import { useState } from "react";

const ReviewsDisplay = ({ name, reviewsArray }) => {
  const [starsFilter, setStarsFilter] = useState(0);
  const [showReviews, setShowReviews] = useState(2);

  const reviewsStars = (stars) => {
    if (stars > 0) {
      const filteredReviews = reviewsArray.filter((item) => item.rate == stars);
      return filteredReviews;
    }
    return reviewsArray;
  };
  const reviews = reviewsStars(starsFilter);

  const ratings = reviewsArray.reduce(function (acc, curr) {
    return acc + curr.rate;
  }, 0);
  const media = (ratings / reviewsArray.length).toFixed(1);

  return (
    <div className="ml-2 mt-8 text-slate-800">
      <h1 className="text-2xl	mt-10 mb-5 md:font-light	md:text-4xl	">
        Avaliações {name[0].toUpperCase() + name.slice(1)}
      </h1>
      <div className="flex relative">
        <h2 className="text-4xl mb-3 md:text-5xl">{media}</h2>
        <p className="absolute bottom-2 left-16 md:bottom-2 md:left-20 text-basicDark text-xs">
          Média entre {reviewsArray.length} opiniões
        </p>
        <div className="mt-1 ml-2">
          <StarRating
            rate={media}
            filter={true}
            setStarsFilter={setStarsFilter}
          />
        </div>
      </div>
      <div>
        <ul className="list-none">
          {reviews.map((item, i) => {
            if (reviews[i] && i < showReviews) {
              return (
                <CommentReview
                  key={reviews[i].id}
                  title={reviews[i].title.toUpperCase()}
                  comment={reviews[i].comment}
                  rate={reviews[i].rate}
                  comment_date={reviews[i].comment_date}
                />
              );
            }
          })}
        </ul>
      </div>
      {showReviews < reviewsArray.length && (
        <button
          className="font-bold text-black underline text-sm mt-5 mb-8"
          onClick={() => setShowReviews(showReviews + 3)}
        >
          Ver mais avaliações
        </button>
      )}
    </div>
  );
};

export default ReviewsDisplay;
