const StarRating = ({ rate, filter, setStarsFilter }) => {
  rate = Math.round(rate);

  const setFilter = (index) => {
    if (filter) {
      setStarsFilter(index);
    }
  };

  const filterStyle =
    "text-primaryYellow material-icons-outlined text-3xl relative bottom-2";
  const defaultStyle = "text-primaryYellow material-icons-outlined text-md";

  return (
    <div>
      {[...Array(5)].map((item, index) => {
        return (
          <button key={index} onClick={() => setFilter(index + 1)}>
            {index < rate ? (
              <span className={filter ? filterStyle : defaultStyle}>star</span>
            ) : (
              <span className={filter ? filterStyle : defaultStyle}>
                star_border
              </span>
            )}
          </button>
        );
      })}
    </div>
  );
};

export default StarRating;
