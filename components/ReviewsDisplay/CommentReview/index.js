import StarRating from "@components/ReviewsDisplay/StarRating";

const CommentReview = ({ title, comment, rate, comment_date }) => {
  const getDays = () => {
    comment_date = new Date(comment_date);
    const current_date = new Date();
    const days = Math.round(
      (current_date.getTime() - comment_date.getTime()) / (1000 * 3600 * 24)
    );
    return days;
  };

  return (
    <li className="mt-6 list-none	">
      <StarRating rate={rate} filter={false} />
      <h3 className="text-lg md:text-xl">{title}</h3>
      <p className="text-sm">{comment}</p>
      <p className="text-basicDark mt-1">Há {getDays()} dias</p>
    </li>
  );
};

export default CommentReview;
