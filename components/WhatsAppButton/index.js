import { useState, useEffect } from "react";
import Image from "next/image";
import { listenEvent } from "@services/utils";

const WhatsAppButton = () => {
  const [expanded, setExpanded] = useState(false);

  useEffect(() => {
    listenEvent("whatsapp:open", (detail) => {
      if (!expanded) setExpanded(detail);
    });
    listenEvent("whatsapp:close", () => {
      if (expanded) setExpanded(false);
    });
  }, []);

  const handleCloseExpanded = () => {
    setExpanded(false);
  };

  const link_default = "https://wa.me/5511912709419?text=Olá, pode ajudar-me?";

  return (
    <div className="flex flex-col fixed z-10 bottom-16 right-4">
      {expanded && (
        <div className="flex mb-2">
          <span
            className="material-icons absolute left-0 top-0 z-10 cursor-pointer"
            onClick={handleCloseExpanded}
          >
            close
          </span>

          <div className="relative py-2 px-3">
            <a
              className="flex items-center justify-center rounded-full border-4 border-white"
              href={expanded.wpp_link}
            >
              <Image
                src={expanded.agent_icon}
                alt="Laura"
                width={64}
                height={64}
              />
            </a>

            <div className="absolute bottom-0 right-0 w-0 h-0 border-8 border-b-white border-r-white border-t-transparent border-l-transparent" />
          </div>

          <a
            className="flex flex-col justify-center px-2 bg-white rounded-xl rounded-bl-none shadow-sm font-lato font-bold text-base text-secondaryDark"
            href={expanded.wpp_link}
          >
            <p>Olá, sou {expanded.agent_name}</p>
            <p>{expanded.agent_message}</p>
          </a>
        </div>
      )}

      <a
        className="relative self-end max-w-fit py-4 px-5 bg-whatsApp flex items-center justify-start rounded-full rounded-br-none text-white"
        href={link_default}
        target="_blank"
      >
        <span className="absolute p-2 bg-notification rounded-full top-0 left-0" />
        <span className="material-icons text-4xl">whatsapp</span>
        {expanded && (
          <p className="px-2 whitespace-nowrap text-lg">Continue no WhatsApp</p>
        )}
      </a>
    </div>
  );
};

export default WhatsAppButton;
