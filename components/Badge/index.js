const Badge = ({ children, size, value = 0, className = "" }) => {
  const sizeClasses = {
    sm: "px-1 py-0.5 text-xs",
    md: "px-1.5 py-1 text-sm",
    lg: "px-3 py-2 text-md",
  };

  return (
    <div className={`relative flex align-middle ${className}`}>
      {
        <p
          className={`bg-primaryYellow text-white rounded-xl absolute -right-1 -top-2 font-bold font-lato leading-none ${sizeClasses[size]}`}
        >
          {value}
        </p>
      }
      {children}
    </div>
  );
};

export default Badge;
