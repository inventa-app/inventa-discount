import { triggerEvent } from "@services/utils";
const HidePrice = () => {
  return (
    <button
      className="w-full text-center bg-primaryYellow text-white font-medium font-roboto text-base rounded py-4 md:py-5 lg:py-6 mt-1"
      onClick={() => triggerEvent("modal_open:login")}
    >
      Ver Preço
    </button>
  );
};

export default HidePrice;
