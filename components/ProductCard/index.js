import { formatPrice } from "@services/utils";

import Image from "next/image";
import HidePrice from "@components/HidePriceButton";
import QuantityComp from "@components/QuantityComponent";
const ProductCard = ({
  userLogged,
  title,
  minimal_order,
  image,
  price,
  minimal_quant,
  discount,
}) => {
  return (
    <li className="w-40">
      <Image
        src={image}
        alt={title}
        width={170}
        height={170}
        className="border-gray-100"
      />

      {userLogged ? (
        <h2 className="text-inventaGrayText text-xs font-normal">
          {title} | Min {formatPrice(minimal_order)}
        </h2>
      ) : (
        <h2 className="text-inventaGrayText text-xs font-normal">{title}</h2>
      )}

      <span className="flex">
        {userLogged ? (
          <h3 className="flex text-lg">{formatPrice(price)}</h3>
        ) : (
          <h3 className="text-lg whitespace-nowrap">
            R$
            <span className="relative top-1 material-icons-outlined">lock</span>
          </h3>
        )}
        {discount > 0 && (
          <p className="text-xs text-red-500 ml-2 mt-0.5">{discount}% off</p>
        )}
      </span>
      <p className="text-xs font-light font-lato text-black ">
        Quantidade mínima {minimal_quant} und
      </p>
      {userLogged ? (
        <QuantityComp
          quantity={{ min_quantity: 0 }}
          stock_props={{ sell_without_stock: false }}
        />
      ) : (
        <HidePrice />
      )}
    </li>
  );
};

export default ProductCard;
