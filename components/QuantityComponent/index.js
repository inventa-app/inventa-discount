import { useState, useEffect, useRef } from "react";

const Quantity = ({
  mobile,
  initialQuantity = 0,
  quantity: { min_quantity = 0, max_quantity = false, step_quantity = 1 },
  stock_props: { sell_without_stock = true, stock },
  callback,
  keyIndex,
}) => {
  const [keyValue, setKeyValue] = useState(keyIndex);
  const [init, setInit] = useState(initialQuantity);
  const [value, setValue] = useState(initialQuantity);

  let input = useRef();

  useEffect(() => {
    if (init !== initialQuantity || keyValue !== keyIndex) {
      setValue(initialQuantity);
      setInit(initialQuantity);
      setKeyValue(keyIndex);
      input.current.value = initialQuantity;
    }
  });

  const increment = () => {
    updateValue(value + step_quantity);
  };
  const decrement = () => {
    if (value > 0) {
      if (value - step_quantity < min_quantity) updateValue(0);
      else updateValue(value - step_quantity);
    }
  };

  const handleValue = (e) => {
    const val = +e.target.value;
    setValue(val);
  };

  const handleValidation = (e) => {
    if ((e.key && e.key == "Enter") || !e.key) {
      updateValue(value);
    }
    if (e.key) {
      let validKey = false;
      switch (e.key) {
        case "Backspace":
        case "ArrowRight":
        case "ArrowLeft":
        case "Delete":
        case "Tab":
          validKey = true;
          break;
      }
      if (isNaN(e.key) && !validKey) {
        e.preventDefault();
      }
    }
  };

  const updateValue = (quantity) => {
    let newValue = quantity;
    if (step_quantity && quantity % step_quantity !== 0) {
      newValue = +(quantity + step_quantity - (quantity % step_quantity));
    }
    if (max_quantity && newValue > max_quantity) {
      newValue = max_quantity;
    }
    if (min_quantity && newValue > 0 && newValue < min_quantity) {
      newValue = min_quantity;
    }
    if (!sell_without_stock && !isNaN(stock) && newValue > stock) {
      newValue = stock;
    }
    input.current.value = newValue;
    setValue(newValue);
    if (typeof callback === "function") callback(newValue);
  };

  useEffect(() => {
    input.current.value = value;
  }, []);

  return (
    <div
      className={`quantity border border-black flex items-center justify-between ${
        mobile ? "px-2 py-1 w-full" : "px-4 py-2 md:w-1/2 w-full"
      } rounded `}
    >
      <button
        className="text-5xl  text-gray-500 "
        onClick={decrement}
        disabled={value <= 0}
      >
        -
      </button>
      <input
        ref={input}
        type="text"
        name="quantity"
        className="w-1/2 text-center text-2xl focus:outline-none"
        onBlur={handleValidation}
        onKeyDown={handleValidation}
        onChange={handleValue}
      />
      <button
        className="text-5xl  text-gray-500 "
        onClick={increment}
        disabled={max_quantity !== null && value >= max_quantity}
      >
        +
      </button>
    </div>
  );
};

export default Quantity;
