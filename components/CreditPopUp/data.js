const FirstIcon = "/general_images/CreditPopUp/firstIcon.svg";
const SecondIcon = "/general_images/CreditPopUp/secondIcon.svg";
const ThirdIcon = "/general_images/CreditPopUp/thirdIcon.png";
const FourthIcon = "/general_images/CreditPopUp/fourthIcon.svg";

const data = [
  {
    icon: FirstIcon,
    title: "À vista",
    description:
      "Via boleto ou cartão de crédito, você paga logo após finalizá-la. Após a confirmação do pagamento, o nosso time de logística vai correr para entregar tudo rapidinho.",
  },
  {
    icon: SecondIcon,
    title: "Parcelado no cartão de crédito",
    description:
      "Você também pode parcelar suas compras em até 12x no cartão de crédito, sem juros!",
  },
  {
    icon: ThirdIcon,
    title: "Recebíveis da maquininha de cartão",
    description:
      "TruePay é um meio de pagamento que libera o seu saldo das suas maquininhas, sem nenhuma taxa. Isso libera mais capital para comprar e prazo para pagar.",
  },
  {
    icon: FourthIcon,
    title: "Parcelado no boleto",
    description:
      "Você poderá parcelar todo o valor aprovado para o seu perfil em até 3x no prazo de até 90 dias. Geramos e enviamos os boletos por e-mail assim que o envio for confirmado pelos fabricantes.",
    description2:
      "Assim, você recebe e vende os produtos para seus clientes e só depois precisa pagar a Inventa. Legal, né? E melhor ainda: pagando as contas em dia, você ganha mais limite e prazo para levar mais produtos para sua clientela!",
  },
];

export default data;
