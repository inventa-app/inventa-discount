import { useEffect, useRef, useContext } from "react";
import { UserContext } from "@services/contexts/UserContext";
import data from "./data";
import Image from "next/image";

const CreditPopUp = ({ toggle }) => {
  const { clientWidth } = useContext(UserContext);
  const breakpointMd = clientWidth >= 768 ? true : false;

  const ref = useRef(null);

  const handleTogglePopUp = () => {
    toggle();
  };

  useEffect(() => {
    const handleClickOutside = (e) => {
      if (ref.current && !ref.current.contains(e.target)) {
        toggle();
      }
    };

    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref, toggle]);

  return (
    <div className="flex flex-row-reverse items-start justify-center bg-black bg-opacity-70 w-screen h-screen fixed top-0 z-50 py-6 overflow-y-auto">
      {breakpointMd && (
        <span
          className="material-icons cursor-pointer text-4xl text-white"
          onClick={handleTogglePopUp}
        >
          close
        </span>
      )}

      <div
        className="relative flex flex-col bg-white rounded-md pt-8 px-12 w-11/12 md:max-w-4xl h-fit max-h-screen overflow-y-auto text-secondaryDark"
        ref={ref}
      >
        {!breakpointMd && (
          <span
            className="material-icons absolute cursor-pointer text-4xl top-2 right-2"
            onClick={handleTogglePopUp}
          >
            close
          </span>
        )}

        <p className="font-bold text-2xl">
          Como eu posso pagar minhas compras na Inventa?
        </p>

        <p className="mt-2 mb-6">
          Você pode pagar seus pedidos de quatro maneiras:
        </p>

        {data.map((item, index) => (
          <div
            key={index}
            className="md:ml-6 mb-6 flex flex-col md:flex-row items-start"
          >
            <div className="mr-4">
              <Image
                src={item.icon}
                alt={"Image for " + item.title}
                width={50}
                height={50}
              />
            </div>

            <div className="w-full md:w-11/12">
              <p className="font-bold">{item.title}</p>
              <p>{item.description}</p>
              {item.description2 && <p className="mt-2">{item.description2}</p>}
            </div>
          </div>
        ))}

        <div className="my-6 w-full h-px bg-inventaBorderGray"></div>
      </div>
    </div>
  );
};

export default CreditPopUp;
