import Redirect from "@components/Redirect";

const Breadcrumb = ({ steps = [] }) => {
  return (
    <nav className="mx-4 pt-4 sm:pt-0">
      <ul className="flex flex-wrap w-full items-center gap-y-0.5 gap-x-3 text-basicDark lg:text-secondaryDark text-xs md:text-sm lg:text-lg font-lato font-normal">
        {steps.map(({ link, label }, index) => (
          <li
            key={index + "li-list"}
            className={
              steps.length - 1 !== index
                ? "cursor-pointer hover:text-primaryYellow"
                : ""
            }
          >
            {steps.length - 1 !== index ? (
              <>
                <Redirect href={link}>{label}&nbsp;</Redirect>
                <span>&nbsp;&nbsp;/</span>
              </>
            ) : (
              <p>{label}</p>
            )}
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Breadcrumb;
