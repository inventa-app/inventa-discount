import { useState, useEffect, useContext } from "react";
import { UserContext } from "@services/contexts/UserContext";

function CollapseItem({ children, title, type, titleLink }) {
  const { clientWidth } = useContext(UserContext);

  const [show, setShow] = useState(false);

  useEffect(() => {
    if (clientWidth > 1024 && type !== "faq") {
      setShow(true);
    } else {
      setShow(false);
    }

    if (type == "description") {
      setShow(true);
    }

  }, [clientWidth]);

  const toggle = () => {
    if (type === "footer" && clientWidth > 1024) {
      return;
    }

    setShow((prev) => !prev);
  };

  return (
    <div>
      {type !== "notification" && (
        <div
          className={`flex items-center justify-between ${
            type === "description" ? "py-2 px-4 border rounded my-4" : ""
          } ${type !== "footer" ? "cursor-pointer" : ""}`}
          onClick={toggle}
        >
          <p
            className={`font-bold font-roboto text-black my-3 ${
              type === "faq"
                ? "my-2 font-noto text-secondaryDark text-xl leading-10"
                : ""
            } ${
              type === "filters"
                ? "font-lato font-black text-2xl text-secondaryDark"
                : ""
            }
          ${type === "footer" ? "lg:text-base 2xl:text-lg" : ""}
          `}
          >
            {type === "footer" && titleLink && clientWidth > 1024 ? (
              <a href={titleLink} target="_blank">{title}</a>
            ) : (
              <span>{title}</span>
            )}
          </p>

          {type === "description" && (
            <span className="material-icons-outlined">expand_more</span>
          )}

          {type === "faq" && !show && (
            <span className="px-3 py-2 material-icons text-2xl bg-basicMedium rounded-full">add</span>
          )}

          {type === "faq" && show && (
            <span className="px-3 py-2 material-icons text-2xl text-white bg-secondaryDark rounded-full">close</span>
          )}

          {type === "filters" && (
            <span className="material-icons-outlined ml-10">expand_more</span>
          )}
        </div>
      )}

      {type === "notification" && (
        <div className="flex items-center justify-center" onClick={toggle}>
          <div className={`${show ? "" : "truncate"}`}>{children}</div>
          {clientWidth <= 480 ? (
            <span className="material-icons-outlined">{show ? "expand_less" : "expand_more"}</span>
          ) : null}
        </div>
      )}

      {show && type !== "notification" && (
        <div className={`leading-loose font-lato`}>{children}</div>
      )}
    </div>
  );
}

export default CollapseItem;
