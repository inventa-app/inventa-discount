import { useState } from "react";
import Dropdown from '@components/Dropdown';
import DiscountsPopUp from './DiscountsPopUp';
import { useContext } from "react";
import { UserContext } from "@services/contexts/UserContext";
import Image from 'next/image'

const data={
    banner:'https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg',
    logo:'https://images.tcdn.com.br/files/859628/themes/13/img/settings/we.jpg',
    name:'We Nutz',
    total_products:500,
    discount:'10% off na primera compra',
}

const FornecedorHeader =  ({ fornecedor }) => {
    const [showDropdown, setShowDropdown] = useState(false);

    const handleToggleDropdown = () => {
        setShowDropdown(!showDropdown);
    };

    const { clientWidth } = useContext(UserContext);
    


    return (
        <div className='block' style={{borderBottom: '6px solid #ddaa2e', width: clientWidth}}>
            <div className='w-full h-72 relative'>
                <Image
                    src={fornecedor.data.getNmkpBrandByBrandHandle.banner ? fornecedor.data.getNmkpBrandByBrandHandle.banner : 'https://cdn.shopify.com/s/files/1/0507/3226/9775/files/banner-brand-background.png?v=1633535674'}
                    alt='banner'
                    layout='fill'
                    className="object-cover"
                />
            </div>
            
        </ div>
    );
}
 
export default FornecedorHeader;
