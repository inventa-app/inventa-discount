const discounts = [
  {
    minimum: 1000,
    percentage: 5,
  },
  {
    minimum: 2000,
    percentage: 10,
  },
  {
    minimum: 4000,
    percentage: 20,
  },
];

const DiscountsPopUp = () => {
  return (
    <div className="pt-5">
      <p className="font-noto font-bold text-2xl">Descontos</p>

      <ul>
        {discounts?.map((discount, index) => (
          <li
            key={`discount${index}`}
            className="my-5 py-5 px-6 bg-primaryLight bg-opacity-30 rounded-lg"
          >
            <p className="font-lato text-xs uppercase text-primaryDark">
              Descontos
            </p>
            <p className="my-2 font-lato font-bold text-lg">
              Compre mais de R$ {discount.minimum} e ganhe {discount.percentage}
              %
            </p>
            <p className="w-fit p-1 pr-2 bg-primaryDark rounded-[0.188rem] font-noto text-xs uppercase text-white">
              Em produtos da We Nutz
            </p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default DiscountsPopUp;
