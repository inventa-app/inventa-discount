import Breadcrumb from "@components/Breadcrumb";
import SocialIcons from "@components/SocialIcons";
import Image from "next/image";


const FornecedorHeader = ({ fornecedor }) => {
  console.log(fornecedor);
  return (
    <div style={{marginBottom: '-100px'}}>
      <div
        style={{
          backgroundImage: `url(${fornecedor.data.getNmkpBrandByBrandHandle.banner ? fornecedor.data.getNmkpBrandByBrandHandle.banner : 'https://cdn.shopify.com/s/files/1/0507/3226/9775/files/banner-brand-background.png?v=1633535674'})`,
          backgroundRepeat: "no-repeat",
          width: "100%",
          height: "300px",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          borderBottom: "6px solid #ddaa2e",
        }}
      ></div>
      <div className="flex">
        <div
          style={{
            position: "relative",
            top: -100,
            height: "270px",
            flex: "0 0 auto",
            width: "270px",
            margin: "20px",
          }}
        >
          <div
            style={{
              width: "270px",
              height: "270px",
              backgroundColor: "white",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              boxShadow: "0px 5px 25px rgb(0 0 0 / 15%)",
            }}
          >
            <img src={fornecedor.data.getNmkpBrandByBrandHandle.logo} />
          </div>
        </div>
        <div>
          <h2 className="text-4xl font-medium text-[#666]">{fornecedor.data.getNmkpBrandByBrandHandle.brand_name}</h2>
          <span className="font-light text-base font-lato text-secondaryDark">
            Mínimo R{fornecedor.data.getNmkpBrandByBrandHandle.min}
          </span>

          <p className="text-xl font-lato font-[300] text-secondaryDark">
            {fornecedor.data.getNmkpBrandByBrandHandle.description}
          </p>
        </div>
      </div>
    </div>
  );
};

export default FornecedorHeader;
