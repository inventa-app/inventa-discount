import { useState } from "react";
import Dropdown from '@components/Dropdown';
import DiscountsPopUp from './DiscountsPopUp';
import { useContext } from "react";
import { UserContext } from "@services/contexts/UserContext";
import Image from 'next/image'

const data={
    banner:'https://simply-delicious-food.com/wp-content/uploads/2018/11/holiday-dessert-board-3-500x375.jpg',
    logo:'https://images.tcdn.com.br/files/859628/themes/13/img/settings/we.jpg',
    name:'We Nutz',
    total_products:500,
    discount:'10% off na primera compra',
}

const FornecedorHeader =  ({ fornecedor }) => {
    const [showDropdown, setShowDropdown] = useState(false);

    const handleToggleDropdown = () => {
        setShowDropdown(!showDropdown);
    };

    const { clientWidth } = useContext(UserContext);
    


    return (
        <div style={{marginTop: 150, marginBottom: 10, width: clientWidth}}>
      <div className="flex">
        <div
          style={{
            position: "relative",
            top: -20,
            height: "170px",
            flex: "0 0 auto",
            width: "170px",
            margin: "20px",
          }}
        >
          <div
            style={{
              width: "170px",
              height: "170px",
              backgroundColor: "white",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              boxShadow: "0px 5px 25px rgb(0 0 0 / 15%)",
            }}
          >
            <img src={fornecedor.data.getNmkpBrandByBrandHandle.logo} />
          </div>
        </div>
        <div>
          <h2 className="text-4xl font-medium text-[#666]">{fornecedor.data.getNmkpBrandByBrandHandle.brand_name}</h2>
          <span className="font-light text-base font-lato text-secondaryDark">
            Mínimo R${fornecedor.data.getNmkpBrandByBrandHandle.min}
          </span>

          <p className="text-xl font-lato font-[300] text-secondaryDark">
            {fornecedor.data.getNmkpBrandByBrandHandle.description}
          </p>
        </div>
      </div>
    </div>
    );
}
 
export default FornecedorHeader;
