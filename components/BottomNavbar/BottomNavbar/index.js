import Redirect from "@components/Redirect";
import { triggerEvent } from "@services/utils";

const BottomNavbar = ({ auth }) => {
  return (
    <nav className="fixed bottom-0 inset-x-0 bg-white shadow-md h-14 border-t px-4 flex items-center z-10">
      {auth ? (
        <div className="flex items-center justify-between w-full text-basicDark px-4 md:px-8">
          <div className="flex justify-center items-center flex-col">
            <span className="material-icons-outlined">card_giftcard</span>
            <Redirect
              href="/account"
              className="text-[0.5rem] hover:text-primaryYellow"
            >
              Meus Pedidos
            </Redirect>
          </div>
          <div className="flex justify-center items-center flex-col">
            <span className="material-icons-outlined">local_shipping</span>
            <Redirect
              href="/account/addresses"
              className="text-[0.5rem] hover:text-primaryYellow"
            >
              Endereços
            </Redirect>
          </div>
          <div className="flex justify-center items-center flex-col">
            <span className="material-icons-outlined">monetization_on</span>
            <a
              href="#"
              onClick={(e) => {
                e.preventDefault();
                triggerEvent("credit-popup:open");
              }}
              className="text-[0.5rem] hover:text-primaryYellow"
            >
              Crédito
            </a>
          </div>
          <a
            href="https://inventa.shop/"
            className="flex justify-center items-center flex-col text-[0.5rem] hover:text-primaryYellow"
            onClick={(e) => {
              e.preventDefault();
              triggerEvent("asidebar:open");
            }}
          >
            <span className="material-icons-outlined">more_horiz</span>
            Mais
          </a>
        </div>
      ) : (
        <div className="flex items-center justify-center space-x-4 w-full h-full">
          <Redirect
            href="https://inventa.shop/"
            className="bg-primaryYellow text-center border-primaryYellow text-black rounded text-xs font-roboto font-medium px-8 py-3.5 w-full whitespace-nowrap"
          >
            Cadastre-se
          </Redirect>
          <button
            onClick={() => {
              triggerEvent("modal_open:login");
            }}
            className="bg-white border border-black rounded text-dark text-xs font-roboto font-medium px-8 py-3 w-full"
          >
            Entrar
          </button>
          <div className="flex justify-center items-center flex-col text-basicDark w-52 text-center my-4">
            <span className="material-icons-outlined">live_help</span>
            <Redirect
              href="/#faq"
              className="text-[0.5rem] hover:text-primaryYellow"
            >
              FAQ Lojista
            </Redirect>
          </div>
        </div>
      )}
    </nav>
  );
};

export default BottomNavbar;
