import { useState, useEffect } from "react";

const Redirect = ({ href = "/", toLegacy, children, ...rest }) => {
  const host = process.env.LEGACY_SITE;
  const [props, setProps] = useState({
    href: toLegacy ? host + href : href,
    ...rest,
  });
  useEffect(() => {
    if (toLegacy) {
      setProps({
        ...props,
        onMouseDown: (e) => {
          if (e.button === 1) {
            e.preventDefault();
            if (href != "#") {
              const cartUpdates =
                window.localStorage.getItem("cart_user-updates");
              window.localStorage.removeItem("cart_user-updates");
              if (cartUpdates && typeof window.legacyRedirect === "function") {
                const data = {};
                data["cart_user-updates"] = cartUpdates;
                legacyRedirect(
                  href,
                  encodeURIComponent(JSON.stringify(data)),
                  process.env.LEGACY_SITE,
                  true
                );
              } else window.open(process.env.LEGACY_SITE + href, "_blank");
            }
          }
        },
        onClick: (e) => {
          const controlClick = e.ctrlKey ? true : false;
          e.preventDefault();
          if (href != "#") {
            const cartUpdates =
              window.localStorage.getItem("cart_user-updates");
            window.localStorage.removeItem("cart_user-updates");
            if (cartUpdates && typeof window.legacyRedirect === "function") {
              const data = {};
              data["cart_user-updates"] = cartUpdates;
              legacyRedirect(
                href,
                encodeURIComponent(JSON.stringify(data)),
                process.env.LEGACY_SITE,
                controlClick
              );
            } else {
              if (controlClick)
                window.open(process.env.LEGACY_SITE + href, "_blank");
              else window.location.href = process.env.LEGACY_SITE + href;
            }
          }
        },
      });
    }
  }, []);
  return <a {...props}>{children}</a>;
};

export default Redirect;
