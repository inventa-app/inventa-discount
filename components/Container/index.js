const Container = ({ children, tailwindClass }) => {
  return (
    <div
      className={`max-w-[1440px] mx-auto ${tailwindClass ? tailwindClass : ""}`}
    >
      {children}
    </div>
  );
};

export default Container;
