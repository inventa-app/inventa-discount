const CadastroBanner = () => {
    return ( 
        <div className="w-full bg-secondaryDark py-7 rounded-tr-3xl rounded-tl-3xl flex flex-col items-center">
            <p className="text-[2.5rem] w-[46rem] text-white text-center font-noto font-extrabold mb-10"> 
                Cadastre-se e tenha acesso aos melhores descontos
            </p>
            <button className="text-white text-lg bg-primaryYellow py-4 px-36 rounded-md font-roboto font-medium">
                Cadastre-se
            </button>
        </div> 
    );
}
 
export default CadastroBanner;