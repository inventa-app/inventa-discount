import FloatingMenu from "@components/FloatingMenu";
import Container from "@components/Container";
import Redirect from "@components/Redirect";

const HeaderMenu = ({ menuItems }) => {
  const url =
    typeof window !== "undefined" ? window.location.href.split("/") : "";

  if (menuItems && menuItems.menu)
    return (
      <div className="border-b-gray-300 border-b-[0.0625rem] bg-white">
        <Container>
          <ul className="flex md:justify-center w-full floating-menu_wrapper overflow-x-auto pr-3">
            {menuItems.menu.map((item, index) => (
              <li key={`${index}-${item.label}`} className="whitespace-nowrap">
                {item.submenu ? (
                  <FloatingMenu
                    hover
                    mark="down"
                    position="bottom"
                    className="mt-[2.532rem]"
                    clickable={
                      <Redirect
                        href={`${item.link}`}
                        className={`font-lato font-bold w-full lg:text-base text-sm pl-3 pr-6 lg:pl-2 py-4 flex text-center hover:text-primaryYellow ${
                          url.includes(item.link)
                            ? "text-secondaryDark"
                            : "text-basicDark"
                        } `}
                      >
                        {item.label}
                      </Redirect>
                    }
                  >
                    <ul>
                      {item.submenu.map((subItem, index) => (
                        <li key={`${index}-${subItem.label}`}>
                          {subItem.submenu ? (
                            <FloatingMenu
                              hover
                              mark="right"
                              position="right"
                              clickable={
                                <Redirect
                                  key={`${index}-${subItem.label}`}
                                  href={subItem.link}
                                  className="h-full w-full pl-4 pr-6 py-2 hover:text-primaryYellow hover:cursor-pointer font-lato font-bold text-basicDark text-sm flex"
                                >
                                  {subItem.label}
                                </Redirect>
                              }
                            >
                              {subItem.submenu.map((deepItem, index) => (
                                <Redirect
                                  key={`${index}-${deepItem.label}`}
                                  href={deepItem.link}
                                  className="h-full w-full px-4 py-2 hover:text-primaryYellow hover:cursor-pointer font-lato font-bold text-basicDark text-sm flex"
                                >
                                  {deepItem.label}
                                </Redirect>
                              ))}
                            </FloatingMenu>
                          ) : (
                            <Redirect
                              key={`${index}-${subItem.label}`}
                              href={subItem.link}
                              className="h-full w-full px-4 py-2 hover:text-primaryYellow hover:cursor-pointer font-lato font-bold text-basicDark text-sm flex"
                            >
                              {subItem.label}
                            </Redirect>
                          )}
                        </li>
                      ))}
                    </ul>
                  </FloatingMenu>
                ) : (
                  <Redirect
                    href={`${item.link}`}
                    className={`font-lato font-bold lg:text-base text-sm px-3 lg:px-4 py-4 block text-center hover:text-primaryYellow ${
                      url.includes(item.link)
                        ? "text-secondaryDark"
                        : "text-basicDark"
                    }`}
                  >
                    {item.label}
                  </Redirect>
                )}
              </li>
            ))}
          </ul>
        </Container>
      </div>
    );
  else return <></>;
};

export default HeaderMenu;
