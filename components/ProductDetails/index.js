import React, { useContext } from "react";
import Endereco from "./Endereco";
import Info from "./Info";
import ImageViewer from "./ImageViewer";
import { UserContext } from "../../services/contexts/UserContext";
import Breadcrumb from "@components/Breadcrumb";
import Fornecedor from "./Description/Fornecedor/index";
import DescriptionText from "./Description/Product/DescriptionText";
import Caracteristicas from "./Description/Product/Caracteristicas";
import MoreInformation from "./Description/Product/MoreInformation";
import { shopifyImagesLoader } from "@services/utils";
import Redirect from "@components/Redirect";

const ProductWrapper = ({ product, user }) => {
  const auth = user && user.user;
  const { clientWidth } = useContext(UserContext);
  const breakpointMd = clientWidth >= 768 ? true : false;
  const breakpointLg = clientWidth >= 1024 ? true : false;

  const {
    title,
    images,
    min_quantity,
    body_html,
    brand_info,
    max_quantity,
    step_quantity,
    not_shipping_to,
    options,
    status,
    values,
    variants,
    vendor_minimum,
    ie_products,
  } = product;

  return (
    <div>
      {/* {breakpointMd ? <></> : <Endereco />} */}
      <div className="">
        <Breadcrumb
          steps={[
            { label: "Inventa atacado para o varejo", link: "/" },
            {
              label: brand_info.brand_name,
              link: `/collections/vendors?q=${encodeURIComponent(brand_info.brand_name)}`,
            },
            { label: title },
          ]}
        />
      </div>
      {breakpointMd ? (
        <></>
      ) : (
        <ProdTitle name={title} fornecedor={brand_info} />
      )}
      <div className="grid grid-cols-1 lg:grid-cols-2 px-2 items-center">
        <div className="order-1 px-4">
          <ImageViewer
            images={
              Array.isArray(images)
                ? images.map((image) => ({
                    original: shopifyImagesLoader({
                      src: image.src,
                      width: 1000,
                      height: 600,
                    }),
                    thumbnail: shopifyImagesLoader({
                      src: image.src,
                      width: 250,
                      height: 150,
                    }),
                    thumbnailWidth: 40,
                    originalHeight: 50,
                  }))
                : []
            }
          />
        </div>
        <div className="order-2 px-6 lg:px-0 lg:flex lg:h-full lg:items-start">
          <Info
            not_shipping_to={not_shipping_to}
            ie_product={ie_products}
            fornecedor={brand_info}
            name={title}
            auth={auth}
            options={options}
            variants={variants && variants.length ? variants : []}
            quantity={{
              min_quantity,
              max_quantity,
              step_quantity,
            }}
          />
        </div>
        <div className="order-4 lg:order-3 px-6 h-full flex flex-col items-center lg:border-y lg:mt-[2.2rem]">
          <DescriptionText description={body_html} />
        </div>
        <div className="order-3 self-start lg:order-4 px-6 lg:px-0">
          <Fornecedor
            minReales={vendor_minimum}
            not_shipping_to={not_shipping_to}
            from={
              Array.isArray(values) &&
              values.find((value) => value.name == "Origem")?.value
            }
            fornecedor={brand_info}
          />
        </div>
        <div className="order-5 px-6 py-4">
          <Caracteristicas title="Características do produto" values={values} />
          {!breakpointMd && <MoreInformation brand_info={brand_info} />}
        </div>
      </div>
    </div>
  );
};

export default ProductWrapper;

export const ProdTitle = ({ name, fornecedor }) => {
  return (
    <div className="p-4 md:p-0">
      <h1 className="text-black text-xl lg:text-3xl my-2 font-['Noto_Sans']">
        <span className="font-light">{name}</span>
        <span className="text-gray-400"> no atacado direto com fornecedor</span>
      </h1>
      <Redirect
        href={`/collections/vendors?q=${encodeURIComponent(
          fornecedor.brand_name
        )}`}
        className="text-sm xl:text-base font-lato underline text-blue-400 tracking-wide"
      >
        Conhecer mais produtos da {fornecedor.brand_name}
      </Redirect>
    </div>
  );
};
