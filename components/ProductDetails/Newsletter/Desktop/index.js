import styles from "./style.module.css";
const Mails = "/general_images/Newsletter/mails.png";
import Image from "next/image";

const NewsletterDesktop = () => {
  return (
    <div className={styles.container}>
      <div className="flex items-center justify-center pt-12 pb-7">
        <Image src={Mails} alt="Mails" width={290} height={265} />
        <form className="ml-32">
          <p className="max-w-sm font-lato font-extrabold text-4xl text-white">
            Fique por dentro de promoções e novas ofertas da Inventa.
          </p>
          <input
            className="h-14 mt-8 rounded-l py-3 px-4 placeholder-newsletterInputPlaceholder"
            type="email"
            name="newsletter"
            placeholder="Seu e-mail"
          />
          <button
            className="uppercase rounded-r py-4 px-6 text-white bg-newsletterInputButton"
            type="submit"
          >
            Inscrever-Se
          </button>
        </form>
      </div>
    </div>
  );
};

export default NewsletterDesktop;
