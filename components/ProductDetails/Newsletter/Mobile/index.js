const Newsletter = () => {
  return (
    <div className="bg-secondaryDark text-white p-10 space-y-3">
      <h3 className="text-2xl font-bold font-noto">Assine nossa newsletter</h3>
      <p className="text-lg font-light">
        Quer ser a primeira a pessoa a saber sobre produtos e promoções?
      </p>
      <form className="space-y-4">
        <input
          type="text"
          name="newsletter"
          id="newsletter"
          placeholder="Seu email"
          className="font-light text-black placeholder:font-light placeholder:text-black w-full p-3 rounded focus:outline-none"
        />
        <button className="bg-primaryYellow text-black font-bold text-lg w-full focus:outline-none rounded py-4 font-lato">
          ASSINAR
        </button>
      </form>
    </div>
  );
};
export default Newsletter;
