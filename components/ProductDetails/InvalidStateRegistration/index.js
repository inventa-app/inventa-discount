const InvalidStateRegistration = () => {
  return (
    <div className="bg-formErrorColor/[0.07] w-full px-10 py-2 my-2">
      <p className="text-formErrorColor	text-center text-sm">
        Esse fornecedor não realiza entregas para clientes que não tenham
        Inscrição Estadual válida. Mais informações{" "}
        <a
          target="_blank"
          href="http://www.sintegra.gov.br/"
          rel="noreferrer"
          className="underline"
        >
          aqui
        </a>
      </p>
    </div>
  );
};

export default InvalidStateRegistration;
