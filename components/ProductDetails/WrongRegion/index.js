const WrongRegion = () => {
  return (
    <div className="w-full text-center my-2">
      <div className="h-14 bg-neutral-100 flex items-center justify-center">
        <p className="text-xl font-lato font-bold text-center text-neutral-700">
          Indisponível
        </p>
      </div>
      <p className="text-base font-lato font-light">
        Marca não atende a região
      </p>
    </div>
  );
};

export default WrongRegion;
