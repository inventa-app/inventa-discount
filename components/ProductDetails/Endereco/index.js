const Endereco = () => {
  return (
    <>
      <div className="flex justify-between items-center px-7 py-2 bg-basicLight cursor-pointer">
        <span className="material-icons-outlined text-sm">
          add_location_alt
        </span>
        <p className="text-sm font-lato">Informar meu endereço</p>
        <span className="material-icons-outlined">chevron_right</span>
      </div>
    </>
  );
};

export default Endereco;
