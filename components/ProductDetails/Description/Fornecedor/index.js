import { formatPrice } from "@services/utils";
import Image from "next/dist/client/image";
import Redirect from "@components/Redirect";

const Fornecedor = ({ fornecedor, not_shipping_to, from, minReales }) => {
  return (
    <div className="mt-2 text-neutral-500 border-t lg:border lg:rounded lg:m-4 lg:px-10 lg:py-6">
      <h2 className="font-bold text-lg lg:text-2xl mb-4 mt-10 font-lato lg:mt-0">
        Informações sobre o fornecedor
      </h2>
      <ul className="w-full space-y-4 text-sm lg:text-base">
        <li className="flex items-center space-x-2">
          {fornecedor.logo && (
            <div className="h-20 w-20">
              <Image
                className="brand-logo-image"
                src={fornecedor.logo}
                height={80}
                width={80}
                alt={fornecedor.brand_handle}
              />
            </div>
          )}
          <p className="font-lato">
            {fornecedor.brand_name},{" "}
            {minReales && minReales > 0
              ? formatPrice(minReales) + " pedido mínimo"
              : "sem mínimo"}
          </p>
        </li>
        {from && (
          <li className="flex items-center space-x-2">
            <span className="material-icons-outlined text-black">
              location_on
            </span>
            <p className="font-lato">Envia de {from}</p>
          </li>
        )}
        {(!not_shipping_to || !not_shipping_to.length) && (
          <li className="flex items-center space-x-2">
            <span className="material-icons-outlined text-black">house</span>
            <p className="font-lato">Produto entregue para todo o Brasil</p>
          </li>
        )}
      </ul>
      <Redirect
        href={`/collections/vendors?q=${encodeURIComponent(
          fornecedor.brand_name
        )}`}
        className="flex items-center justify-between font-roboto font-bold text-sm text-black border rounded p-4 my-4 lg:text-base lg:opacity-70 lg:text-[#386DBC] lg:border-none lg:p-0 lg:mt-12"
      >
        Ver mais sobre a marca {fornecedor.brand_name}{" "}
        <span className="material-icons-outlined">chevron_right</span>
      </Redirect>
    </div>
  );
};

export default Fornecedor;
