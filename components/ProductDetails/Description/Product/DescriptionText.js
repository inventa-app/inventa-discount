const DescriptionText = ({ description }) => {
  return (
    <div className="text-neutral-500 my-6 font-bold lg:font-normal lg:text-black lg:py-7 lg:my-0">
      <h3 className="font-lato text-lg mb-4 lg:text-2xl lg:mt-0">
        Descrição do produto
      </h3>
      <div
        className="text-sm lg:text-base font-lato font-normal lg:leading-6 text-justify"
        dangerouslySetInnerHTML={{ __html: description }}
      ></div>
    </div>
  );
};

export default DescriptionText;
