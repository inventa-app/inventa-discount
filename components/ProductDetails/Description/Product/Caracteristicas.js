import { useContext } from "react";
import CollapseItem from "@components/CollapseItem";
import { UserContext } from "@services/contexts/UserContext";

//Do a table with the values array

const CaracteristicasTable = ({ values }) => {
  return (
    <table className="w-full rounded lg overflow-hidden striped-table">
      <tbody>
        {values.map(({ name, value }) =>
          value && value !== false && value !== "false" ? (
            <tr key={name} className="h-12">
              <td className="text-base font-lato w-1/2  text-black pl-3 lg:pl-6 cell">
                {name}
              </td>
              <td className="text-base font-lato w-1/2 text-black pl-3 lg:pl-6 cell">
                {name == "Tempo de validade"
                  ? value + " dias"
                  : value == "true"
                  ? "Sim"
                  : value == "false"
                  ? "Não"
                  : value}
              </td>
            </tr>
          ) : null
        )}
      </tbody>
    </table>
  );
};

const Caracteristicas = ({ title, values }) => {
  const { clientWidth } = useContext(UserContext);

  return (
    <>
      {clientWidth >= 768 ? (
        <div className="mb-7 lg:py-7 w-full">
          <h3 className="font-lato text-lg mb-4 lg:text-2xl lg:my-4">
            {title}
          </h3>
          <CaracteristicasTable values={values} />
        </div>
      ) : (
        <CollapseItem open={true} title={title} type="description">
          <CaracteristicasTable values={values} />
        </CollapseItem>
      )}
    </>
  );
};

export default Caracteristicas;
