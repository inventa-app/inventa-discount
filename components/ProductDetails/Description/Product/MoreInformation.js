import { useContext } from "react";
import CollapseItem from "@components/CollapseItem";
import { UserContext } from "../../../../services/contexts/UserContext";

const MoreInformation = ({ brand_info }) => {
  const { clientWidth } = useContext(UserContext);
  const { description } = brand_info;
  return (
    <>
      {clientWidth >= 768 ? (
        <div>
          <p className="text-sm text-black">{description}</p>
        </div>
      ) : description && (
        <CollapseItem
          open={false}
          title="Mais informações sobre o fornecedor"
          type="description"
        >
          <div className="border rounded p-4 mb-6">
            <p className="text-sm text-black">{description}</p>
          </div>
        </CollapseItem>
      )}
    </>
  );
};

export default MoreInformation;
