import { useContext } from "react";
import CollapseItem from "@components/CollapseItem";
import { UserContext } from "@services/contexts/UserContext";

const Pagamento = ({ title, pagamento }) => {
  const { clientWidth } = useContext(UserContext);

  const divs = pagamento?.map((item, index) => (
    <div className="flex flex-col space-y-2" key={`pagamento-${index}`}>
      <p className="font-light">{item.type}</p>
      <div className="flex items-center space-x-4">
        <div className="bg-gray-200 h-10 w-10"></div>
        <div className="bg-gray-200 h-10 w-10"></div>
        <div className="bg-gray-200 h-10 w-10"></div>
      </div>
    </div>
  ));

  return (
    <>
      {clientWidth >= 768 ? (
        <div className="mt-10 md:mt-0 mb-6 sm:text-neutral-500 lg:m-4 lg:px-10 lg:py-6 lg:border lg:rounded ">
          <h2 className="font-bold text-lg lg:text-2xl mb-4 font-lato lg:mt-0 ">
            {title}
          </h2>
          {divs}
        </div>
      ) : (
        <CollapseItem open={true} title={title} type="description">
          <div className="flex flex-col space-y-4 border rounded p-4">
            {divs}
          </div>
        </CollapseItem>
      )}
    </>
  );
};

export default Pagamento;
