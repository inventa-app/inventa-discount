import { useState, useEffect } from "react";

const Variants = ({ variants, options, onChange, quantity }) => {
  if (
    !options.length ||
    (options.length === 1 && options[0].values.length <= 1)
  )
    return <></>;
  const availableOption = (option, type) =>
    variants.some(
      ({
        option_values,
        variant_stock: { inventory_quantity, sell_without_stock },
      }) => {
        return (
          (sell_without_stock ||
            (!sell_without_stock &&
              inventory_quantity >= quantity.min_quantity)) &&
          option_values.some((o) => o.type === type && o.value === option)
        );
      }
    );
  const initOptions = () => {
    const obj = {};
    options.map((option) => {
      let found = false;
      for (let i = 0; i < option.values.length && !found; i++) {
        if (availableOption(option.values[i], option.name)) {
          obj[option.name] = option.values[i];
          found = true;
        }
      }
    });
    return obj;
  };
  const [optionsSelected, setOptionsSelected] = useState(initOptions());

  useEffect(() => {
    const variant = variants.find(
      ({ option_values }) =>
        !option_values.some(
          ({ type, value }) => optionsSelected[type] !== value
        )
    );
    if (typeof onChange === "function" && variant)
      onChange(variant.shopify_variant_id.actual_id);
  }, [optionsSelected]);

  const handleChange = (option, type) => {
    const obj = { ...optionsSelected };
    obj[type] = option;
    setOptionsSelected(obj);
  };

  const isAvailable = (option, type) => {
    const beforeObjs = {};
    for (let i = 0; i < options.length && options[i].name !== type; i++) {
      beforeObjs[options[i].name] = optionsSelected[options[i].name];
    }
    beforeObjs[type] = option;
    if (
      !variants.some(
        ({ option_values }) =>
          !option_values.some(({ type, value }) => beforeObjs[type] !== value)
      )
    )
      return false;
    return true;
  };
  return (
    <>
      {options.map(({ name, values }, index) => {
        if (!values || values.length <= 1) return <></>;
        return (
          <div className="w-full" key={"select-variant_" + name}>
            <h4 className="text-xl mb-2">
              {name !== "Title" ? name : "Variantes"}:
            </h4>
            <select
              onChange={(e) => handleChange(e.target.value, name)}
              className="w-full custom-select bg-variantSelector/20 rounded-md px-6 py-4"
            >
              {values.map((option) => {
                const props = {};
                if (
                  !isAvailable(option, name) ||
                  !availableOption(option, name)
                )
                  props.disabled = true;
                return (
                  <option {...props} key={"variant-option_" + option}>
                    {option}
                  </option>
                );
              })}
            </select>
          </div>
        );
      })}
    </>
  );
};

export default Variants;
