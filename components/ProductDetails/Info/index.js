import { useState, useContext, useEffect } from "react";
import { formatPrice } from "@services/utils";
import { triggerEvent } from "@services/utils";
import Envio from "./Envio";
import Variants from "../Variants";
import { ProdTitle } from "..";
import { UserContext } from "@services/contexts/UserContext";
import HidePrice from "@components/HidePriceButton";
import QuantityComp from "@components/QuantityComponent";
import WrongRegion from "../WrongRegion";
import InvalidStateRegistration from "../InvalidStateRegistration";

const Info = ({
  auth,
  name,
  fornecedor,
  variants,
  options,
  quantity,
  not_shipping_to = [],
  ie_product = false,
}) => {
  const [showEnvio, setShowEnvio] = useState(false);
  const { clientWidth, getCartInfo, user } = useContext(UserContext);
  const [variantSelected, setVariantSelected] = useState(variants[0]);
  const [quantityInCart, setQuantityInCart] = useState(
    getCartInfo().find(
      (item) => variants[0].shopify_variant_id.actual_id === item.id
    ) || { id: variants[0].shopify_variant_id.actual_id, qty: 0 }
  );

  const getActualVariantQty = () => {
    return {
      qty: quantityInCart.qty,
    };
  };

  useEffect(() => {
    const newQty = getCartInfo().find(
      (item) => variantSelected.shopify_variant_id.actual_id === item.id
    ) || { id: variantSelected.shopify_variant_id.actual_id, qty: 0 };
    setQuantityInCart(newQty);
  }, [variantSelected]);

  const {
    variant_stock: {
      inventory_quantity,
      available = false,
      sell_without_stock = false,
    },
    variant_price: { price, original_price, discount, resell_price, taxes },
    shopify_variant_id: { actual_id },
  } = variantSelected || variants[0];

  return (
    <>
      <div
        className={`flex flex-col w-full space-y-3 ${
          auth ? "lg:space-y-3" : "lg:space-y-3"
        } my-4 lg:border lg:rounded lg:p-4 lg:mx-4`}
      >
        <div className="lg:order-1">
          {clientWidth >= 768 && (
            <ProdTitle name={name} fornecedor={fornecedor} />
          )}
        </div>
        {variants && variants.length > 0 && (
          <div className="lg:order-2">
            <Variants
              variants={variants}
              options={options}
              quantity={quantity}
              onChange={(id) =>
                setVariantSelected(
                  variants.find(
                    ({ shopify_variant_id: { actual_id } }) => actual_id === id
                  )
                )
              }
            />
          </div>
        )}
        <div className={`font-lato pt-4 ${auth ? "lg:order-3" : "lg:order-7"}`}>
          {!!resell_price && (
            <p className="font-light text-lg">
              <span>Preço de Revenda: </span>
              <span className="font-normal">{formatPrice(resell_price)}</span>
            </p>
          )}
        </div>
        <div
          className={`flex lg:order-4 ${
            auth ? "flex-col" : "items-center space-x-2"
          }`}
        >
          {auth && !!discount && (original_price || price) && (
            <div>
              <p className="text-red-500 line-through font-roboto font-normal lg:text-xl">
                {formatPrice(
                  ((original_price || price) * 100) / (100 - discount)
                )}
              </p>
            </div>
          )}
          <div className="flex items-center space-x-4">
            {auth ? (
              <div className="flex items-center space-x-2 font-roboto">
                {!!price && (
                  <p className="text-4xl font-normal font-roboto">
                    {formatPrice(
                      original_price && original_price > 0
                        ? original_price
                        : price
                    )}
                  </p>
                )}
                {taxes &&
                  user &&
                  user.customer_state &&
                  taxes[user.customer_state] > 0 && (
                    <p className="text-sm lg:text-lg font-extralight font-roboto">
                      +{" "}
                      {formatPrice(
                        ((original_price && original_price > 0
                          ? original_price
                          : price) *
                          taxes[user.customer_state]) /
                          100
                      )}{" "}
                      ICMS ST
                    </p>
                  )}
              </div>
            ) : (
              <div className="flex items-center">
                <p className="text-4xl font-normal font-roboto">R$</p>
                <span className="material-icons-outlined lock-icon">lock</span>
              </div>
            )}
          </div>
        </div>
        {auth === true && (
          <div className="lg:order-5">
            <p className="text-blue-500 font-roboto">
              Ver os meios de pagamento
            </p>
          </div>
        )}
        <div className="flex flex-col space-y-2 lg:order-6">
          <div className="flex items-center space-x-2 text-sm">
            <span className="material-icons-outlined lg:text-2xl">
              local_shipping
            </span>
            <p className="text-success text-base font-roboto lg:text-xl">
              Frete grátis
            </p>
          </div>
          {auth === false && (
            <div className="cursor-pointer flex items-center space-x-2 text-sm ml-5">
              <span className="material-icons-outlined">location_on</span>
              <p
                className="text-blue-500 font-roboto text-base lg:text-lg"
                onClick={(e) => {
                  e.preventDefault();
                  triggerEvent("modal_open:login");
                }}
              >
                Ver se é eletivo para minha região
              </p>
            </div>
          )}
        </div>
        <div className="lg:order-8">
          {auth && (
            <>
              {quantity.min_quantity && quantity.min_quantity > 1 && (
                <p className="font-light text-sm mb-2 font-lato">
                  Quantidade mínima {quantity.min_quantity} unidades
                </p>
              )}
              {quantity.max_quantity && (
                <p className="font-light text-sm mb-2 font-lato">
                  Quantidade máxima {quantity.max_quantity} unidades
                </p>
              )}
            </>
          )}
          {auth ? (
            <>
              {user.customer_state &&
              not_shipping_to.includes(user.customer_state) ? (
                <WrongRegion />
              ) : ie_product &&
                (user.ie_ok == "false" || user.ie_ok == false) ? (
                <InvalidStateRegistration />
              ) : (
                <QuantityComp
                  keyIndex={variantSelected.shopify_variant_id.actual_id}
                  initialQuantity={getActualVariantQty().qty}
                  quantity={quantity}
                  stock_props={{
                    sell_without_stock,
                    stock: inventory_quantity,
                  }}
                  callback={(qty) => {
                    if (qty > 0) {
                      triggerEvent("cart:update-product", {
                        qty,
                        id: variantSelected.shopify_variant_id.actual_id,
                      });
                    } else {
                      triggerEvent("cart:remove-product", {
                        id: variantSelected.shopify_variant_id.actual_id,
                      });
                    }
                  }}
                />
              )}
            </>
          ) : (
            <HidePrice />
          )}
        </div>
      </div>

      {showEnvio && <Envio closeModal={() => setShowEnvio(false)} />}
    </>
  );
};

export default Info;
