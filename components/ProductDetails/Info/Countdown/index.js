const Countdown = () => {
  return (
    <div className="w-full bg-secondaryDark text-md text-white flex items-center justify-between py-2 px-10">
      <div className="flex items-center">
        <span className="material-icons-outlined">money_off_csred</span>
      </div>
      <div>
        <p className="font-base text-xs md:text-base font-roboto">
          25% off acima de 60 produtos{" "}
        </p>
      </div>
      <div>
        <p className="font-light text-xs md:text-base font-roboto">12:00:15</p>
      </div>
    </div>
  );
};

export default Countdown;
