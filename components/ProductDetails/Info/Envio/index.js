import { useState } from "react";
import Dropdown from "@components/Dropdown";

const Envio = ({ closeModal }) => {
  const [value, setValue] = useState();
  const [step, setStep] = useState(1);

  const handleSubmit = (e) => {
    e.preventDefault();
    setStep(2);
  };

  return (
    <>
      <Dropdown closeModal={closeModal}>
        {step == 1 && (
          <div className="space-y-5 h-56" id="first-form">
            <h3 className="text-xl font-normal">Digite um CEP</h3>
            <form onSubmit={handleSubmit}>
              <label
                htmlFor="cep"
                className="text-neutral-800 text-sm font-light"
              >
                Informar um CEP
              </label>
              <input
                type="text"
                name="cep"
                id="cep"
                className="border-b border-black focus:outline-none w-full"
                required
                value={value}
                onChange={(e) => setValue(e.target.value)}
              />
              <button
                type="submit"
                className="w-full bg-black text-white p-3 rounded mt-7"
              >
                Aceitar
              </button>
            </form>
            <p
              className="text-center text-xs text-blue-500"
              onClick={() => setStep(3)}
            >
              Não sei meu cep
            </p>
          </div>
        )}
        {step == 2 && (
          <div className="h-56" id="calculo">
            <h3 className="text-xl mb-2">Calculo de Envio</h3>
            <div className="space-y-2 text-sm">
              <p>Calculamos os custos</p>
              <p>CEP: {value} - Rua da Laranja, Salvador, Bahia</p>
              <div className="flex items-center space-x-2">
                <p>Voce tem:</p>
                <span className="material-icons-outlined text-success">
                  local_shipping
                </span>
                <p className="text-success font-normal">Frete grátis</p>
              </div>
            </div>
            <button className="w-full bg-black text-white p-3 rounded mt-7">
              Cadastrar-se
            </button>
          </div>
        )}
        {step == 3 && (
          <div className="h-56 flex flex-row justify-center items-center">
            <div className="space-y-2">
              <form onSubmit={(e) => e.preventDefault()}>
                <label htmlFor="endereco" className="font-light">
                  Digite um endereço
                </label>
                <input
                  type="text"
                  name="endereco"
                  id="endereco"
                  className="border-b border-black focus:outline-none w-full"
                  required
                />
                <button className="w-full bg-black text-white p-3 rounded mt-7">
                  Aceitar
                </button>
              </form>
              <p className="text-center text-xs text-blue-500">
                Não sei meu cep
              </p>
            </div>
          </div>
        )}
      </Dropdown>
    </>
  );
};

export default Envio;
