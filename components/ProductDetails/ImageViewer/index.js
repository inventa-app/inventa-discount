import { UserContext } from "@services/contexts/UserContext";
import { useContext, useRef } from "react";
import ImageGallery from "react-image-gallery";
import "react-image-gallery/styles/css/image-gallery.css";

const ImageViewer = ({ images = [] }) => {
  const { clientWidth } = useContext(UserContext);
  const imageGalleryElement = useRef();

  return (
    <div className="w-100 pt-4 flex justify-center">
      <div className="w-full h-full">
        <ImageGallery
          ref={imageGalleryElement}
          onClick={() => {
            imageGalleryElement.current.fullScreen();
          }}
          items={images}
          lazyLoad={true}
          showPlayButton={false}
          thumbnailPosition={
            clientWidth && clientWidth > 768 ? "left" : "bottom"
          }
          showIndex={true}
          showThumbnails={images.length > 1}
        />
      </div>
    </div>
  );
};

export default ImageViewer;
