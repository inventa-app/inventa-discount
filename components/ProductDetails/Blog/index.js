import data from "./data.js";
import Image from "next/image";

const Blog = () => {
  return (
    <>
      <h1 className="mb-9 font-noto text-5xl text-inventaGrayText">
        Conheça nosso Blog
      </h1>

      <div className="flex">
        {data?.map((item, index) => (
          <div className="mr-8 last-of-type:mr-0" key={index}>
            <Image src={item.image} alt={item.title} width={255} height={204} />

            <p className="font-lato font-bold text-2xl text-inventaGrayText">
              {item.title}
            </p>

            <p className="font-lato text-lg text-inventaGrayText">
              {item.description}
            </p>
          </div>
        ))}
      </div>
    </>
  );
};

export default Blog;
