const Blog1 = "/general_images/Blog/blog1.png";
const Blog2 = "/general_images/Blog/blog2.png";
const Blog3 = "/general_images/Blog/blog3.png";
const Blog4 = "/general_images/Blog/blog4.png";
const Blog5 = "/general_images/Blog/blog5.png";

const data = [
  {
    image: Blog1,
    title: "Lorem IPSUM",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    image: Blog2,
    title: "Lorem IPSUM",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    image: Blog3,
    title: "Lorem IPSUM",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    image: Blog4,
    title: "Lorem IPSUM",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    image: Blog5,
    title: "Lorem IPSUM",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
];

export default data;
