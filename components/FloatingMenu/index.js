import { useState, useRef, useEffect } from "react";

const FloatingMenu = ({
  children,
  arrow,
  mark,
  hover,
  position,
  clickable,
}) => {
  const [display, setDisplay] = useState(false);
  const ref = useRef(null);

  const arrowStyle = {
    right: "right-4",
    left: "left-4",
    center: "left-1/2 -translate-x-1/2",
  };

  const positionStyle = {
    right: "md:left-full md:-translate-y-10 -translaye-x-1",
    left: "md:left-0",
    bottom: "md:top-full md:-translate-y-1",
    top: "",
  };

  const markDirection = {
    up: "expand_less",
    right: "navigate_next",
    left: "navigate_before",
    down: "expand_more",
  };

  const handleClick = () => {
    setDisplay(!display);
  };

  const handleHoverEnter = () => {
    if (hover) {
      setDisplay(true);
    }
  };

  const handleHoverLeave = () => {
    if (hover) {
      setDisplay(false);
    }
  };

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (ref.current && !ref.current.contains(event.target)) {
        setDisplay(false);
      }
    };
    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("click", handleClickOutside, true);
    };
  }, []);

  return (
    <div
      className="z-10"
      ref={ref}
      onMouseEnter={handleHoverEnter}
      onMouseLeave={handleHoverLeave}
    >
      <div className="flex justify-between items-center hover:cursor-pointer relative">
        <div onClick={handleClick}>{clickable}</div>
        {mark && (
          <span
            className="material-icons-outlined text-base absolute right-0 top-1/2 -translate-y-1/2 text-basicDark"
            onClick={handleHoverEnter}
          >
            {markDirection[mark]}
          </span>
        )}
      </div>
      {display && (
        <div
          className={`flex flex-col border-t-inventaBordersColor border bg-white floating-menu absolute rounded text-black shadow-lg z-20 ${positionStyle[position]}`}
        >
          <div className="relative">
            {arrow && display && (
              <div
                className={`absolute -top-4  h-0 w-0 border-8 border-transparent border-b-white
                                    ${arrowStyle[arrow] || "left-1"}
                                `}
              />
            )}
          </div>
          {children}
        </div>
      )}
    </div>
  );
};

export default FloatingMenu;
