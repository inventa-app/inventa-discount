import Image from "next/image";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/splide/dist/css/splide.min.css";
import { formatPrice } from "@services/utils";
import ProductCard from "@components/ProductCard";
import style from "./style.module.css";
import { breakpointsTypes } from "./breakpoints";

const Carousel = ({ userLogged, type, items, title }) => {
  const arrowsStyle = {
    arrows: `splide__arrows ${style["arrows"]}`,
    arrow: "splide__arrow",
    prev: `splide__arrow--prev ${style["splide__arrow--prev"]}`,
    next: `splide__arrow--next ${style["splide__arrow--next"]}`,
  };

  return (
    <div className="w-[95%] pl-2 relative left-2">
      <h3 className="w-[75%] text-lg font-lato text-inventaGrayText font-medium	ml-2 mb-6 md:text-[2rem] md:leading-9">
        {title}
      </h3>
      <Splide
        options={{
          pagination: false,
          rewind: true,
          gap: type == "brands" ? "2rem" : "1rem",
          arrowPath:
            "M7.98 10L32 10L32 6L7.98 6V-9.53674e-07L0 8L7.98 16V10V10Z",
          classes: arrowsStyle,
          perPage: type == "brands" ? 8 : 7,
          breakpoints: breakpointsTypes[type],
        }}
      >
        {items.map((item, i) =>
          type == "brands" ? (
            <SplideSlide key={item + i}>
              <div className="w-fit h-full text-center mb-12">
                <a
                  className="flex w-32 h-32 m-auto p-4 
                border border-slate-300"
                  href={item.link}
                >
                  <Image
                    src={item.image}
                    alt={item.name}
                    width={100}
                    height={100}
                  />
                </a>
                <div className="mb-5 text-center w-32">
                  <a href={item.link}>
                    <p className="mt-2 text-xs">{item.name}</p>
                  </a>
                  <p className="text-basicDark text-xs font-light w-fit m-auto">
                    Mín. {formatPrice(item.price)}
                  </p>
                </div>
              </div>
            </SplideSlide>
          ) : (
            <SplideSlide key={item + i}>
              <div className="mb-12 w-fit">
                <ul>
                  <ProductCard
                    key={item.id}
                    userLogged={userLogged}
                    title={item.title}
                    minimal_order={item.minimal_order}
                    image={item.image}
                    price={item.price}
                    minimal_quant={item.minimal_quant}
                    discount={item.discount}
                  />
                </ul>
              </div>
            </SplideSlide>
          )
        )}
      </Splide>
    </div>
  );
};

export default Carousel;
