const ProductImage = "/general_images/product_image.png";

export const brands = [
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/smell-of-fragrance_100x.png",
    name: "Smell of Fragrance",
    link: "https://inventa.shop/collections/vendors?q=Smell+of+Fragrance",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/megazinebr_100x.png",
    name: "MegazineBR",
    link: "https://inventa.shop/collections/vendors?q=MegazineBR",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/ricosti_100x.png",
    name: "Ricosti",
    link: "https://inventa.shop/collections/vendors?q=Ricosti",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/bioz-green_100x.png",
    name: "Bioz Green",
    link: "https://inventa.shop/collections/vendors?q=Bioz+Green",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/olea-saboaria_100x.png",
    name: "Olea Saboaria",
    link: "https://inventa.shop/collections/vendors?q=Olea+Saboaria",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/natural-science_100x.png",
    name: "Natural Science",
    link: "https://inventa.shop/collections/vendors?q=Natural+Science",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/oz-candy_100x.png",
    name: "Oz Candy",
    link: "https://inventa.shop/collections/vendors?q=Oz+Candy",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/raizes-do-campo_100x.png",
    name: "Raízes do Campo",
    link: "https://inventa.shop/collections/vendors?q=Ra%C3%ADzes+do+Campo",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/luzz-cacau_100x.png",
    name: "Luzz Cacau",
    link: "https://inventa.shop/collections/vendors?q=Luzz+Cacau",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/royalwax_100x.png",
    name: "RoyalWax",
    link: "https://inventa.shop/collections/vendors?q=RoyalWax",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/smell-of-fragrance_100x.png",
    name: "Smell of Fragrance",
    link: "https://inventa.shop/collections/vendors?q=Smell+of+Fragrance",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/megazinebr_100x.png",
    name: "MegazineBR",
    link: "https://inventa.shop/collections/vendors?q=MegazineBR",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/ricosti_100x.png",
    name: "Ricosti",
    link: "https://inventa.shop/collections/vendors?q=Ricosti",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/bioz-green_100x.png",
    name: "Bioz Green",
    link: "https://inventa.shop/collections/vendors?q=Bioz+Green",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/olea-saboaria_100x.png",
    name: "Olea Saboaria",
    link: "https://inventa.shop/collections/vendors?q=Olea+Saboaria",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/natural-science_100x.png",
    name: "Natural Science",
    link: "https://inventa.shop/collections/vendors?q=Natural+Science",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/oz-candy_100x.png",
    name: "Oz Candy",
    link: "https://inventa.shop/collections/vendors?q=Oz+Candy",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/raizes-do-campo_100x.png",
    name: "Raízes do Campo",
    link: "https://inventa.shop/collections/vendors?q=Ra%C3%ADzes+do+Campo",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/luzz-cacau_100x.png",
    name: "Luzz Cacau",
    link: "https://inventa.shop/collections/vendors?q=Luzz+Cacau",
    price: 300,
  },
  {
    image:
      "https://cdn.shopify.com/s/files/1/0507/3226/9775/files/royalwax_100x.png",
    name: "RoyalWax",
    link: "https://inventa.shop/collections/vendors?q=RoyalWax",
    price: 300,
  },
];

export const products = [
  {
    id: 1,
    title: "We Green 150g",
    image: ProductImage,
    minimal_order: 300,
    price: 30,
    discount: 20,
    minimal_quant: 5,
  },
  {
    id: 2,
    title: "We Green 150g",
    image: ProductImage,
    minimal_order: 300,
    price: 30,
    discount: 0,
    minimal_quant: 1,
  },
  {
    id: 3,
    title: "We Green 150g",
    image: ProductImage,
    minimal_order: 300,
    price: 30,
    discount: 20,
    minimal_quant: 1,
  },
  {
    id: 4,
    title: "We Green 150g",
    image: ProductImage,
    minimal_order: 300,
    price: 30,
    discount: 0,
    minimal_quant: 1,
  },
  {
    id: 5,
    title: "We Green 150g",
    image: ProductImage,
    minimal_order: 300,
    price: 30,
    discount: 0,
    minimal_quant: 1,
  },
  {
    id: 6,
    title: "We Green 150g",
    image: ProductImage,
    minimal_order: 300,
    price: 30,
    discount: 20,
    minimal_quant: 1,
  },
  {
    id: 7,
    title: "We Green 150g",
    image: ProductImage,
    minimal_order: 300,
    price: 30,
    discount: 40,
    minimal_quant: 1,
  },
  {
    id: 8,
    title: "We Green 150g",
    image: ProductImage,
    minimal_order: 300,
    price: 30,
    discount: 20,
    minimal_quant: 1,
  },
  {
    id: 9,
    title: "We Green 150g",
    image: ProductImage,
    minimal_order: 300,
    price: 30,
    discount: 20,
    minimal_quant: 1,
  },
  {
    id: 10,
    title: "We Green 150g",
    image: ProductImage,
    minimal_order: 300,
    price: 30,
    discount: 0,
    minimal_quant: 1,
  },
];
