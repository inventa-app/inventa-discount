export const breakpointsTypes = {
  "brands": {
    1200: {
      perPage: 7,
    },
    1024: {
      perPage: 6,
      gap: "2rem",
    },
    768: {
      perPage: 4,
      gap: "2rem",
    },
    600: {
      perPage: 3,
    },
    425: {
      perPage: 3,
      gap: "1rem",
    },
    375: {
      perPage: 3,
      gap: "2rem",
    },
    360: {
      perPage: 3,
      gap: "3.5rem",
    },
  },
  "products": {
    1200: {
      perPage: 6,
    },
    1024: {
      perPage: 4.5,
      gap: "1rem",
    },
    768: {
      perPage: 4,
      gap: "4rem",
    },
    600: {
      perPage: 3,
    },
    425: {
      perPage: 2.5,
      gap: "5rem",
    },
    375: {
      perPage: 3,
      gap: "8rem",
    },
    360: {
      perPage: 2,
      gap: "0.5rem",
    },
  },
  "highlights": {
    1440: {
      perPage: 4,
    },
    1200: {
      perPage: 4,
      gap: "1rem",
    },
    1024: {
      perPage: 3,
      gap: "1rem",
    },
    1000: {
      perPage: 2.5,
    },
    768: {
      perPage: 2,
    },
    425: {
      perPage: 2,
    },
    375: {
      perPage: 3,
    },
    360: {
      perPage: 2,
      gap: "0.5rem",
    },
  },
};
