import React from 'react'

export const DropDown = ({ texto = "falta-texto", opciones = ["sin opciones"] }) => {
    return (
        <div>
            <label for={texto}>{texto}</label>
            <select name={texto} id={texto}>
                <option value="">escolher</option>
                {opciones.map(opt => <option value={opt}>{opt}</option>)}
            </select>
        </div>
    )
}