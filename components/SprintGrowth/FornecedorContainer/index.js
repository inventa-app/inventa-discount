import React from 'react'
import BrandProducts from '../BrandProducts/BrandProducts'

export const FornecedorContainer = ({ fornecedor }) => (
  <div className="mx-auto w-fit">
    <p className='mx-auto w-fit mb-5 text-xl md:pt-24 lg:pt-0'>Nossos Produtos</p>
    <BrandProducts fornecedor={fornecedor} />
  </div>
)

/* 
<div className='flex mx-1/2 flex-col md:flex-row-reverse'>
      <BrandProducts fornecedor={fornecedor} />
      <div className='border-solid border-2 w-1/5'>BrandAside</div>
      </div>    
*/ 