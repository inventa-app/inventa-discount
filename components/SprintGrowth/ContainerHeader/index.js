import React from 'react'
import { DropDown } from '../Dropdown'

const textoDePaginado = "mostrar";
const opcionesDepaginado = [
    "12",
    "24",
    "48",
]

const textoDeOrden = "ordenação";
const opcionesDeOrden = [
    "Mais Vendidos",
    "Alfabeticamente A-Z",
    "Alfabeticamente Z-A",
    "Preço crescente",
    "Preço decrescente",
    "Mais recentes",
    "Data, velho para novo",
    "% Liquidação",
]

export const ContainerHeader = () => {
    return (
        <div className=''>
            <div className='m-5'>Nro de Productos</div>
            <div className='flex justify-end '>
                <DropDown texto={textoDePaginado} opciones={opcionesDepaginado} />
                <DropDown texto={textoDeOrden} opciones={opcionesDeOrden} />
            </div>
        </div>
    )
}
