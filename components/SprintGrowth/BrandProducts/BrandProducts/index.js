import ProductCard from "./BrandProductCard";

const BrandProducts = ({ fornecedor }) => (
  <div style={{marginHorizontal: 20}}>
    <ul className="flex flex-wrap justify-center m-10 ">
    {fornecedor.data.getNmkpBrandByBrandHandle.products_brand_page.products.map((product) => {
      return (
        <ProductCard
          userLogged={true}
          key={product.title}
          title={product.title}
          minimal_order={product.min_quantity}
          image={product.images[0].src}
          price={product.price}
          minimal_quant={product.min_quantity}
        /* discount={product.discount} */
        />
      );
    })}
  </ul>
  </div>
);

export default BrandProducts;


{/* 
return (
    <div className="grid grid-cols-1 md:grid-cols-6">
      <ul className="flex mb-4 flex-wrap justify-center gap-y-4 gap-x-4 sm:grid-cols-3">
        {fornecedor.data.getNmkpBrandByBrandHandle.products_brand_page.products.map((product) => {
          return (
            <ProductCard
              userLogged={true}
              key={product.id}
              title={product.title}
              minimal_order={product.minimal_order}
              image={product.image}
              price={product.price}
              minimal_quant={product.minimal_quant}
              discount={product.discount}            
            />
          );
        })}
      </ul>
    </div>
  );
*/}