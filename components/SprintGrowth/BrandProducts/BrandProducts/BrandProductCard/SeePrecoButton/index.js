import React from "react";

export const SeePrecoButton = () => {
  return (
    <a
      href="undefined/"
      class="bg-primaryYellow text-center border-primaryYellow text-black rounded text-xs font-roboto font-medium px-8 py-3.5 w-full whitespace-nowrap"
    >
      Ver preço
    </a>
  );
};
