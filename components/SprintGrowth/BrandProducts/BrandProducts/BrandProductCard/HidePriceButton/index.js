import { triggerEvent } from "@services/utils";
const HidePrice = () => {
  return (
    <button
      className="w-full bg-primaryYellow bottom-0 text-white font-medium font-roboto rounded py-2 mt-1 "
      onClick={() => triggerEvent("modal_open:login")}
    >
      Ver Preço
    </button>
  );
};

export default HidePrice;
