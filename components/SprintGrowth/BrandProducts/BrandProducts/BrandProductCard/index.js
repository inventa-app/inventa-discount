// import SeePriceButton from "../../../FornecedorPage/SeePriceButton";
// import AddProduct from "../AddProductComponent";
import { useContext } from "react";
import { UserContext } from "@services/contexts/UserContext";
import Image from "next/image";
import { QuantityComp } from "@components/QuantityComponent";
import { SeePrecoButton } from "./SeePrecoButton";
import HidePrice from "./HidePriceButton";
const BrandProductCard = ({
  title,
  minimal_order,
  image,
  price,
  minimal_quant,
  discount,
}) => {
  // const { auth } = useContext(UserContext);
  const auth = true;
  return (
    <li className="relative w-56 h-[25rem] mx-3">
      <Image
        src={image}
        alt={title}
        width={400}
        height={400}
        className="border-gray-100"
      />
      {auth ? (
        <h2 className="text-['#666666'] text-base text-slate-500">
          {title} | Min R$
          {minimal_order.toFixed(2).toString().replace(".", ",")}
        </h2>
      ) : (
        <h2 className="text-['#666666'] text-base text-slate-500">{title}</h2>
      )}
      <span className="flex">
        {auth ? (
          <h3 className="flex text-lg text-gray-500">
            R$<span className="relative top-1 material-icons-outlined">lock</span>
          </h3>
        ) : (
          <h3 className="text-lg whitespace-nowrap">
            R$
            <span className="material-icons-outlined">lock</span>
          </h3>
        )}
        {discount > 0 && (
          <p className="text-xs text-red-500 ml-2 mt-0.5">{discount}% off</p>
        )}
      </span>
      <p className="text-base text-slate-500 ">
        Quantidade mínima {minimal_quant} und
      </p>
      {/* {auth ? <AddProduct minimal_quant={minimal_quant} /> : <SeePriceButton />} */}
      {/* {auth ? <QuantityComp /> : <SeePriceButton />} */}
      {/* <SeePrecoButton /> */}
      <div style={{position: 'absolute',
bottom: '0px', width: '100%'}}>
      <HidePrice />
      </div>
    </li>
  );
};
export default BrandProductCard;


