import { gql } from "graphql-request";

export const get_product_handle = (customer_state = false) => gql`
  query getNmkpProductByHandle($handle: String!) {
    getNmkpProductByHandle(handle: $handle) {
      body_html
      handle
      brand_handle
      images {
        alt
        src
      }
      ie_products
      max_quantity
      min_quantity
      not_shipping_to
      options {
        name
        position
        values
      }
      product_id
      ship_all_states
      shopify_product_id {
        actual_id
      }
      status
      step_quantity
      title
      values {
        name
        value
      }
      variants {
        position
        option_values {
          type
          value
        }
        shopify_variant_id {
          actual_id
        }
        sku
        status
        variant_id
        variant_price {
          price
          discount
          original_price
          resell_price
          ${
            customer_state && typeof customer_state === "string"
              ? `taxes {
                ` +
                customer_state.toUpperCase() +
                `
              } `
              : ""
          }
        }
        variant_stock {
          available
          inventory_item_id
          inventory_quantity
          sell_without_stock
        }
      }
      vendor_minimum
      brand_info {	
        brand_id
        vendor_id
        brand_handle
        brand_name
        cnpj
        days_to_ship
        description
        logo
        banner
        websites {url type}
      }
    }
  }
`;
