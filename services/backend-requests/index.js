import { GraphQLClient } from "graphql-request";
import { get_product_handle } from "./queries";

const client = new GraphQLClient(process.env.APPSYNC_URL, {
  headers: {
    "Content-Type": "application/json",
    "x-api-key": process.env.APPSYNC_API_KEY,
  },
});

export const GetProductByHandle = async (handle, customer_state = false) =>
  await client.request(get_product_handle(customer_state), { handle });
