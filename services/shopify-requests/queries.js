import { gql } from "graphql-request";

export const customer_login = ({ email, password }) => gql`
    mutation {
        customerAccessTokenCreate(input: {email: "${email}", password: "${password}"}) {
            customerAccessToken {
                accessToken,
                expiresAt
            },
            customerUserErrors {
                code
            }
        }
    }
`;

export const customer_reset_password = (email) => gql`
  mutation {
    customerRecover(email: "${email}") {
      customerUserErrors{
        code
        field
        message
      }
    }
  }
`;

export const cart_create = () => gql`
  mutation createCart($cartInput: CartInput) {
    cartCreate(input: $cartInput) {
      cart {
        id
        createdAt
        updatedAt
        lines(first: 10) {
          edges {
            node {
              id
              quantity
              merchandise {
                ... on ProductVariant {
                  id
                }
              }
            }
          }
        }
        attributes {
          key
          value
        }
        estimatedCost {
          totalAmount {
            amount
            currencyCode
          }
          subtotalAmount {
            amount
            currencyCode
          }
          totalTaxAmount {
            amount
            currencyCode
          }
          totalDutyAmount {
            amount
            currencyCode
          }
        }
      }
    }
  }
`;

export const cart_recover = ({ cart_id }) => gql`
cart(id:"${cart_id}") {
    id
    createdAt
    updatedAt
    lines(first:10) {
      edges {
        node {
          id
          quantity
          merchandise {
            ... on ProductVariant {
              id
            }
          }
          attributes {
            key
            value
          }
        }
      }
    }
    attributes {
      key
      value
    }
    estimatedCost {
      totalAmount {
        amount
        currencyCode
      }
      subtotalAmount {
        amount
        currencyCode
      }
      totalTaxAmount {
        amount
        currencyCode
      }
      totalDutyAmount {
        amount
        currencyCode
      }
    }
    buyerIdentity {
      email
      phone
      customer {
        id
      }
      countryCode
    }
  }
}
`;

export const cart_modify = () => gql`
  mutation cartLinesUpdate($cartId: ID!, $lines: [CartLineUpdateInput!]!) {
    cartLinesUpdate(cartId: $cartId, lines: $lines) {
      cart {
        id
        lines(first: 10) {
          edges {
            node {
              id
              quantity
              merchandise {
                ... on ProductVariant {
                  id
                }
              }
            }
          }
        }
        estimatedCost {
          totalAmount {
            amount
            currencyCode
          }
          subtotalAmount {
            amount
            currencyCode
          }
          totalTaxAmount {
            amount
            currencyCode
          }
          totalDutyAmount {
            amount
            currencyCode
          }
        }
      }
    }
  }
`;

export const cart_addLine = () => gql`
  mutation cartLinesAdd($lines: [CartLineInput!]!, $cartId: ID!) {
    cartLinesAdd(lines: $lines, cartId: $cartId) {
      cart {
        id
        lines(first: 10) {
          edges {
            node {
              id
              quantity
              merchandise {
                ... on ProductVariant {
                  id
                }
              }
            }
          }
        }
        estimatedCost {
          totalAmount {
            amount
            currencyCode
          }
          subtotalAmount {
            amount
            currencyCode
          }
          totalTaxAmount {
            amount
            currencyCode
          }
          totalDutyAmount {
            amount
            currencyCode
          }
        }
      }
      userErrors {
        code
        field
        message
      }
    }
  }
`;

export const products = ({ quantity }) => gql`
{
    products(first: ${quantity}) {
        edges {
            node {
                id
                title
                description
                handle
                tags
                images(first: 1) { 
                    edges {
                        node {
                            altText
                            transformedSrc(maxWidth: 200, maxHeight: 200)
                        }
                    }
                }
                variants(first: 100) {
                    edges {
                        node {
                            id
                            title
                            quantityAvailable
                            priceV2 {
                                amount
                                currencyCode
                            }
                        }
                    }
                }
            }
        }
    }
}
`;

export const cart_get = ({ cartId }) => gql`
  query checkoutURL {
    cart(id: "${cartId}") {
        id
        createdAt
        updatedAt
        lines(first:10) {
          edges {
            node {
              id
              quantity
              merchandise {
                ... on ProductVariant {
                  id
                }
              }
              attributes {
                key
                value
              }
            }
          }
        }
    }
  }
`;

export const checkout_create = () => gql`
  mutation checkoutCreate($input: CheckoutCreateInput!) {
    checkoutCreate(input: $input) {
      checkout {
        id
        webUrl
        lineItems(first: 5) {
          edges {
            node {
              title
              quantity
            }
          }
        }
      }
    }
  }
`;

export const checkout_linkCustomer = () => gql`
  mutation associateCustomerWithCheckout(
    $checkoutId: ID!
    $customerAccessToken: String!
  ) {
    checkoutCustomerAssociateV2(
      checkoutId: $checkoutId
      customerAccessToken: $customerAccessToken
    ) {
      checkout {
        id
        webUrl
      }
      checkoutUserErrors {
        code
        field
        message
      }
      customer {
        id
      }
    }
  }
`;

export const checkout_redirectLink = ({ cartId }) => gql`
query {
    node(id:"${cartId}" ) {
      ... on Checkout {
        id
        webUrl
      }
    }
  }
`;
