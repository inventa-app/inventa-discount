//Here we make every request via Graphql to Shopify
//The functions are declared in this file, the queries are in the queries.js file.
import { GraphQLClient } from "graphql-request";
import {
  cart_addLine,
  cart_create,
  cart_modify,
  cart_recover,
  checkout_create,
  cart_get,
  checkout_linkCustomer,
  customer_login,
  products,
  customer_reset_password,
} from "./queries";

const client = new GraphQLClient(
  `https://${process.env.SHOPIFY_DOMAIN}/api/2021-10/graphql.json`,
  {
    headers: {
      "Content-Type": "application/json",
      "x-shopify-storefront-access-token": process.env.SHOPIFY_ACCESS_TOKEN,
    },
  }
);

export const LoginUser = async (variables) =>
  await client.request(customer_login({ ...variables }));

export const ResetPassword = async (email) =>
  await client.request(customer_reset_password(email));

export const RecoverCart = async (cart_id) =>
  await client.request(cart_recover({ cart_id }));

export const CreateCart = async ({ id, quantity }) =>
  await client.request(cart_create(), {
    cartInput: {
      lines: [
        {
          quantity: quantity,
          merchandiseId: id,
        },
      ],
    },
  });

export const EditLine = async ({ cartId, line_items }) =>
  await client.request(cart_modify(), {
    cartId,
    lines: line_items,
  });

export const AddLine = async ({ cartId, items }) =>
  await client.request(cart_addLine(), {
    lines: items,
    cartId,
  });

export const GetProducts = async (quantity) =>
  await client.request(products({ quantity }));

export const GetCart = async (cartId) =>
  await client.request(cart_get({ cartId }));

export const CreateCheckout = async (line_items) =>
  await client.request(checkout_create(), { input: { lineItems: line_items } });

export const LinkCustomerToCheckout = async ({
  checkoutId,
  customerAccessToken,
}) =>
  await client.request(checkout_linkCustomer(), {
    checkoutId,
    customerAccessToken,
  });
