export const products = [
  {
    id: 1,
    fornecedor: "Lovme Cosméticos",
    minReales: 500,
    name: "Royal For Men 100 ml",
    prices: {
      price: 70,
      desconto: 38,
      revenda: 99.99,
      imposto: 1.8,
    },
    quantity: 1,
    description:
      "Royal for Men Eau de Toilette de Lonkoom é uma fragrância masculina intensa para homens que estão sempre produzindo, sempre realizando.\nÉ uma fragrância refrescante com personalidade elegante. Pura energia revigorante, majestoso como o mar, de onde vem sua inspiração, Royal tem toda realeza de um homem de espírito jovem, aventureiro que ama a liberdade e é arrojado em suas decisões.\nFougére Aromático é uma fragrância fresca, elegante para homens discretos e sofisticados. As notas de saída laranja, melão, trazem frescor ao perfume. A magia das ervas como o manjericão estimula o aroma trazendo em destaque o âmbar e musk em sua base.",
    caracteristicas:
      "Uma composição refinada de citrinos e madeira que combinam-se para onferir leveza à fragrância. A sensação de frescor e bem-estar pode ser sentida o dia todo. Possui acordes que encantam ao trazer uma inspiração amadeirada aliada a um frescor revigorante. É perfeita para deixar uma assinatura elegante e espontânea no ar. Com uma base composta exclusivamente de Vegetal o Oil Cream da Orgânica possui um maior poder de absorção, melhorando a elasticidade da pele e aumentando o poder de hidratação natural. Feito para proporcionar um tratamento exclusivo de beleza! Ele entrega uma hidratação intensa que pode ser percebida minutos depois de sua aplicação.",
    descFornecedor:
      "Com a Organica você estará entrando no mercado que mais cresce no Brasil e no mundo, que é o seguimento de cosméticos e produtos sustentáveis, nossos produtos são: Veganos; Puro Vegetal; Sem Parabenos; Não Testado em Animais; Eco Friendly; PH Equilibrado; Aromatherapy e com selo FSC. São Cosméticos conscientes pensando no meio ambiente! Para o corpo e cabelos, banho e pós banho. Conheça as condições e faça parte da corrente sustentável.",
    envio: {
      from: "Sao Paulo",
      entrega: "Brasil",
    },
    valores: "",
    variants: [
      {
        type: "Aroma",
        items: [
          "Abacate",
          "Macadâmia",
          "Creme com Avelã",
          "Cacau com Babalu",
          "Oléo de Coco com Babosa",
        ],
      },
    ],
    pagamento: [
      {
        type: "Boleto em até 3x",
        icons: true,
      },
      {
        type: "Cartão de Crédito",
        icons: true,
      },
    ],
  },
  {
    id: 2,
    fornecedor: "NATRIVE COSMÉTICOS ROUSTEN PROFISSIONA",
    minReales: 300,
    name: "Shampoo Natrive Óleo de Argan 500ml",
    prices: {
      price: 3.86,
      desconto: 2,
      revenda: 6.75,
      imposto: 1.5,
    },
    quantity: 12,
    description:
      "Poderoso hidratante. Possui propriedades antioxidante, brilho intenso, anti-frizz, alta hidratação, efeito contra pontas duplas, proteção contra raios UV, proteção térmica e melhora a elasticidade. Garante aos fios aspecto saudável, brilho, maciez, mechas disciplinadas e sem frizz.",
    caracteristicas:
      "Uma composição refinada de citrinos e madeira que combinam-se para onferir leveza à fragrância. A sensação de frescor e bem-estar pode ser sentida o dia todo. Possui acordes que encantam ao trazer uma inspiração amadeirada aliada a um frescor revigorante. É perfeita para deixar uma assinatura elegante e espontânea no ar. Com uma base composta exclusivamente de Vegetal o Oil Cream da Orgânica possui um maior poder de absorção, melhorando a elasticidade da pele e aumentando o poder de hidratação natural. Feito para proporcionar um tratamento exclusivo de beleza! Ele entrega uma hidratação intensa que pode ser percebida minutos depois de sua aplicação.",
    descFornecedor:
      "Com a Organica você estará entrando no mercado que mais cresce no Brasil e no mundo, que é o seguimento de cosméticos e produtos sustentáveis, nossos produtos são: Veganos; Puro Vegetal; Sem Parabenos; Não Testado em Animais; Eco Friendly; PH Equilibrado; Aromatherapy e com selo FSC. São Cosméticos conscientes pensando no meio ambiente! Para o corpo e cabelos, banho e pós banho. Conheça as condições e faça parte da corrente sustentável.",
    envio: {
      from: "Sao Paulo",
      entrega: "Brasil",
    },
    valores: "",
    pagamento: [
      {
        type: "Boleto em até 3x",
        icons: true,
      },
      {
        type: "Cartão de Crédito",
        icons: true,
      },
    ],
  },
  {
    id: 3,
    fornecedor: "Ubá Chocolates Artesanais",
    minReales: 350,
    name: "Barra de Chocolate 62% Cacau com Castanha do Brasil 80g",
    prices: {
      price: 13.9,
      desconto: 10,
      revenda: 21,
      imposto: 2,
    },
    quantity: 1,
    description:
      "Chocolate de sabor suave crocante, combinando os sabores do cacau e castanha do Brasil. A cada 8 unidades de barras 80g enviamos um display de apresentação.",
    caracteristicas:
      "Uma composição refinada de citrinos e madeira que combinam-se para onferir leveza à fragrância. A sensação de frescor e bem-estar pode ser sentida o dia todo. Possui acordes que encantam ao trazer uma inspiração amadeirada aliada a um frescor revigorante. É perfeita para deixar uma assinatura elegante e espontânea no ar. Com uma base composta exclusivamente de Vegetal o Oil Cream da Orgânica possui um maior poder de absorção, melhorando a elasticidade da pele e aumentando o poder de hidratação natural. Feito para proporcionar um tratamento exclusivo de beleza! Ele entrega uma hidratação intensa que pode ser percebida minutos depois de sua aplicação.",
    descFornecedor:
      "Com a Organica você estará entrando no mercado que mais cresce no Brasil e no mundo, que é o seguimento de cosméticos e produtos sustentáveis, nossos produtos são: Veganos; Puro Vegetal; Sem Parabenos; Não Testado em Animais; Eco Friendly; PH Equilibrado; Aromatherapy e com selo FSC. São Cosméticos conscientes pensando no meio ambiente! Para o corpo e cabelos, banho e pós banho. Conheça as condições e faça parte da corrente sustentável.",
    envio: {
      from: "Bahia",
      entrega: "Brasil",
    },
    valores: "Feito no Brasil",
    pagamento: [
      {
        type: "Boleto em até 3x",
        icons: true,
      },
      {
        type: "Cartão de Crédito",
        icons: true,
      },
      {
        type: "True PAY",
        icons: true,
      },
    ],
  },
];
