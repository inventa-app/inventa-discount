import { createContext, useState, useEffect } from "react";
import ModalWrapper from "@components/ModalWrapper";
import { listenEvent, triggerEvent } from "@services/utils";
import Login from "@components/Modals/Login";

export const ModalContext = createContext();

export const ModalProvider = ({ children, userAuth }) => {
  const [modal, setModal] = useState();

  const closeModal = () => {
    setModal(undefined);
  };

  const openModal = ({ name, component, props = {}, closeColor = "black" }) => {
    if (!component) throw { error: "No component assigned to modal" };
    setModal({ name, component, props, closeColor });
  };

  useEffect(() => {
    listenEvent("modal_open:login", () =>
      openModal({ name: "login", component: Login, closeColor: "white" })
    );
  }, []);

  const value = {};
  return (
    <ModalContext.Provider value={value}>
      {children}
      {modal && modal.component ? (
        <ModalWrapper
          closeColor={modal.closeColor}
          onClose={closeModal}
          name={modal.name}
        >
          <modal.component closeFunction={closeModal} {...modal.props} />
        </ModalWrapper>
      ) : (
        <></>
      )}
    </ModalContext.Provider>
  );
};
