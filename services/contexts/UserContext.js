import { LoginUser } from "@services/shopify-requests";
import { getUserAccessToken, listenEvent } from "@services/utils";
import { createContext, useState, useEffect } from "react";
import {
  AuthenticationDetails,
  CognitoUser,
  CognitoUserPool,
} from "amazon-cognito-identity-js";

export const UserContext = createContext();

export const UserProvider = ({ children, user_data }) => {
  const [userCat, setUserCat] = useState();
  const [clientWidth, setClientWidth] = useState(false);
  const [user] = useState(user_data);

  const [cartInfo, setCartInfo] = useState(
    user_data && user_data.cart_items ? JSON.parse(user_data.cart_items) : []
  );
  const getCartInfo = () => cartInfo;
  const cartCount = () =>
    cartInfo.reduce((previousValue, item) => previousValue + item.qty, 0);
  const updateQty = ({ id, qty }) => {
    const newArray = refreshUpdatesStorage({ id, qty });
    setCartInfo(newArray);
  };

  const refreshUpdatesStorage = ({ id, qty }) => {
    const cartStorage = window.localStorage.getItem("cart_user-updates");
    if (cartStorage) {
      let found = false;
      const parsed = JSON.parse(cartStorage);
      parsed.map((item) => {
        if (item.id == id) {
          found = true;
          item.qty = qty;
        }
      });
      if (!found) parsed.push({ id, qty });
      window.localStorage.setItem("cart_user-updates", JSON.stringify(parsed));
      return parsed;
    } else {
      window.localStorage.setItem(
        "cart_user-updates",
        JSON.stringify([{ id, qty }])
      );
      return [{ id, qty }];
    }
  };
  const cartInit = () => {
    listenEvent("cart:update-product", ({ id, qty }) => {
      updateQty({ id, qty });
    });
    listenEvent("cart:remove-product", ({ id }) => {
      updateQty({ id, qty: 0 });
    });
  };

  useEffect(() => {
    setClientWidth(window.innerWidth);

    window.onresize = () => {
      setClientWidth(window.innerWidth);
    };

    const cart =
      user_data && user_data.cart_items ? JSON.parse(user_data.cart_items) : [];
    if (Array.isArray(cart)) {
      setCartInfo(cart);
      cartInit();
      window.localStorage.setItem("cart_user-updates", JSON.stringify(cart));
    }
  }, []);

  const getUserCat = () => userCat.token;

  const userLogin = async (email, password) => {
    if ((email, password)) {
      const {
        customerAccessTokenCreate: { customerUserErrors, customerAccessToken },
      } = await LoginUser({ email, password });
      if (Array.isArray(customerUserErrors) && customerUserErrors.length) {
        console.warn("Login error", { ...customerUserErrors });
        return {
          ok: false,
          error: "invalid_creds",
        };
      }
      if (customerAccessToken) {
        const { accessToken, expiresAt } = customerAccessToken;
        //setUserCat({ token: accessToken, expiresAt });
        //To change
        if (typeof window.legacyLogin === "function")
          window.legacyLogin(email, password, process.env.LEGACY_SITE);

        return {
          ok: true,
        };
      }
    } else
      return {
        ok: false,
        error: "missing_creds",
      };
  };

  const sendLoginCode = (email, callback) => {
    let attempts = -1;
    const prod = !window.location.host.includes("dev");
    const poolData = prod
      ? {
          // Prod
          UserPoolId: "us-east-1_Gn9H5xdeq",
          ClientId: "c99caq12iv5462qitbr66avud",
        }
      : {
          // Dev
          UserPoolId: "us-east-2_WqeLGvUIN",
          ClientId: "6hqh9mntss3k7jcu8hd07quas",
        };
    const newUserPool = new CognitoUserPool(poolData);
    const authenticationDetails = new AuthenticationDetails({
      Username: email,
    });
    const user = new CognitoUser({
      Username: email,
      Pool: newUserPool,
    });
    user.setAuthenticationFlowType("CUSTOM_AUTH");
    user.initiateAuth(authenticationDetails, customAuth());

    function customAuth() {
      return {
        onSuccess: (result) => {
          const jwtToken = result.idToken.jwtToken;
          const corsUrl = "https://let-cors-anywhere.herokuapp.com/";
          const inventaUrl = prod
            ? "https://auth.inventa.tec.br/login/shopify"
            : "https://auth.inventa.dev.br/login/shopify";
          fetch(`${corsUrl}${inventaUrl}`, {
            method: "GET",
            headers: {
              origin: "shopify",
              Authorization: jwtToken,
            },
          })
            .then((res) => {
              return res.json();
            })
            .then((res) => {
              const redirectUrl = res.result;
              window.location.replace(redirectUrl);
            });
        },
        onFailure: (err) => {
          if (typeof callback == "function") callback({ ok: false, err });
        },
        customChallenge: (challengeParameters) => {
          attempts++;
          if (typeof callback == "function") {
            callback({
              ok: true,
              triggerFunction: function (code) {
                if (code) user.sendCustomChallengeAnswer(code, customAuth());
              },
              attempts,
            });
          }
        },
      };
    }
  };

  const value = {
    user,
    getUserCat,
    sendLoginCode,
    userLogin,
    clientWidth,
    cartInfo,
    getCartInfo,
    cartCount,
  };
  return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
};
