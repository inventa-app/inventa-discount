export const triggerEvent = (name, detail = {}) => {
  if (!name) throw { error: 'Missing "name" property' };
  else {
    const event = new CustomEvent(name, { detail });
    window.dispatchEvent(event);
  }
};

export const listenEvent = (name, callback) => {
  if (name) {
    window.addEventListener(name, (e) => {
      if (typeof callback == "function") callback(e.detail);
    });
  } else {
    throw { error: 'Missing required "name" property' };
  }
};

export const saveUserAccessToken = ({ token, expiresAt }) => {
  if (token && expiresAt)
    window.localStorage.setItem(
      "user_cat",
      JSON.stringify({ token, expiresAt })
    );
  else throw { error: "Missing required properties" };
};

export const getUserAccessToken = () => {
  const user_cat = window.localStorage.getItem("user_cat");
  if (user_cat) {
    try {
      const { token, expiresAt } = JSON.parse(user_cat);
      if (token && new Date().getTime() < new Date(expiresAt).getTime())
        return { token, expiresAt };
      else {
        window.localStorage.removeItem("user_cat");
        return null;
      }
    } catch (error) {
      window.localStorage.removeItem("user_cat");
      return null;
    }
  } else return null;
};

export const findTag = (tags, target) => {
  if (Array.isArray(tags)) {
    const result = tags.find((tag) => {
      if (tag.toLowerCase().includes(target.toLowerCase())) return true;
      else return false;
    });
    if (result) return result.split(target);
    else return null;
  } else return null;
};

export const formatPrice = (number) => {
  return new Intl.NumberFormat("pt-BR", {
    style: "currency",
    currency: "BRL",
  }).format(number);
};

/* import jwt from "jsonwebtoken";
import { JWT_SECRET_SIGN } from "@constants/index"; */
export const getAuthToken = (cookies) => {
  if (cookies && cookies["user-inventa"]) return cookies["user-inventa"];
  else return null;
};

export const shopifyImagesLoader = ({ src, width, height }) => {
  if (!width || !height)
    throw {
      error: "Provide Width && Height",
    };
  const a = src.split("/");
  const file = a[a.length - 1];
  const fileName = file.slice(0, file.lastIndexOf("."));
  const rest = file.slice(file.lastIndexOf("."), file.length);
  return src.replace(file, fileName + "_" + width + "x" + height + rest);
};

export const validEmail = (value) =>
  value && value.match(/^[\w-\.\+\u00C0-\u017F]+@([\w-]+\.)+[\w-]{2,6}$/g);

export const validPhone = (value) => value && value.match(/^\d{10,11}\b/g);

export const productInfoForUser = (product, user_data = {}) => {
  const { user } = user_data ? user_data : {};
  if (!user) {
    const { variants, ...rest } = product;
    const variants_noPrice = variants.map((variant) => {
      const { variant_price } = variant;
      const { original_price, ...vp } = variant_price;
      const newVariant_price = { ...vp };
      return { ...variant, variant_price: newVariant_price };
    });
    return { variants: variants_noPrice, ...rest };
  } else return product;
};

let static_data;
export const getStaticInformation = async () => {
  if (!static_data) {
    const data = await fetch(
      "https://inventa-mktp-assets.s3.amazonaws.com/main-menu.json"
    );
    static_data = await data.json();
  }
  return static_data;
};

let footer_static;
export const getStaticFooterInformation = async () => {
  if (!footer_static) {
    const data = await fetch(
      "https://inventa-mktp-assets.s3.amazonaws.com/footer-menu.json"
    );
    footer_static = await data.json();
  }
  return footer_static;
};
