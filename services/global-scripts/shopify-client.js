import { initGraphqlClient, LoginUser } from "@services/shopify-requests";
import { listenEvent } from "@services/utils";

function ShopifyClient(domain, access_token) {
  if (!domain || !access_token) {
    throw {
      error: "Missing information to start client",
    };
  }
  initGraphqlClient(domain, access_token);
}

export default ShopifyClient;
