import MetaTags from "@components/MetaTags";
import Layout from "@layouts/InventaDefault";
import { ModalProvider } from "@services/contexts/ModalContext";
import { UserProvider } from "@services/contexts/UserContext";
import "@styles/global/styles.css";
import Head from "next/head";

function MyApp({ Component, pageProps }) {
  const {
    user,
    notification,
    pageData = {},
    menuItems,
    footerItems,
  } = pageProps;

  return (
    <>
      <Head>
        <title>{pageData.title_main}</title>
        <MetaTags pageData={pageData} />
        {!user?.user && <script defer src="/legacy-login.js" />}
        <script defer src="/legacy-redirect.js" />
      </Head>
      <UserProvider user_data={user}>
        <ModalProvider>
          <Layout
            menuItems={menuItems}
            user={user}
            notification={notification}
            footerItems={footerItems}
          >
            <Component {...pageProps} />
          </Layout>
        </ModalProvider>
      </UserProvider>
    </>
  );
}

export default MyApp;
