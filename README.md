Aca les escribe Nehuen, en este momento ya debo estar perdido en las montañas. Pero aca les dejo toda la documentacion que necesitan para no sufrir desarrollando.
Estoy corriendo con los tiempos, asi que esto lo escribo directo en español.

## Autenticacion con usuario viniendo desde Shopify

Para autenticar un usuario que ya esta logueado en Shopify se debe usar la funcion global que deje armada en el layout theme.liquid, llamada _goToHeadless_. Recordar asegurarse que siga ahi la funcion en caso que hagan un deploy a una nueva version de Shopify.

#### How to use

Al llamar a esta funcion tenemos que pasarle, opcionalmente, un parametro que va a ser el _pathname_ al que queremos redireccionar a nuestro usuario en el headless de nuestro sitio. En caso de no darle ningun parametro se redirecciona a la ruta raiz('/') por default.
En esta primera version que estamos construyendo vamos a jugar con la pagina de detalle de producto solamente asi que lo que van a necesitar es llamarla como goToHeadless('products/:handle'), donde _:handle_ va a ser el parametro que use next.js para pedirle al backend ese producto.

#### Que hace

Esta funcion en resumen, primero redirecciona al usuario al endpoint de nuestra app proxy de autenticacion, mediante la creacion de un formulario para poder darle values dentro del mismo redirect. En nuestra app proxy hacemos una validacion de si el mail del form es el mismo del usuario que este logueado, en caso de que no este logueado no hay mail, sea el mismo mail que nos va a proveer Shopify a travez de liquid. Si todo OK, que existe usuario logueado y los mails corresponden, entonces se genera un JWT que va a tener expiracion y otras cosas. Ese token va a ser pasado a travez de otro redirect ya a nuestro headless en next.js. A traves de este ultimo redirect estamos adicionando otros parametros como mail e IP del requester. Generamos otra validacion en cuanto mail e IP, ver que coincidan para asegurarnos que no haya un tercero en discordia haciendo el request. Si todo OK entonces vamos a setear una Cookie HttpOnly en nuestro usuario. Esa cookie va a contener info de nuestro user, el token de auth provisto por nuestro proxy app, y otros datos como mail, ip.

#### Como validar autenticacion en nuestro SSR

En nuestra pagina de _/produto/:handle_ vamos a tener nuestro metodo _getServerSideProps_ el cual nos va a dar la funcionalidad de fetchear los datos necesarios antes de retornar nuestra app al usuario. _getServerSideProps_ recibe como parametro _context_ el cual nos da el contexto general del request que se esta haciendo en ese momento; entre las que vamos a encontrar las cookies del usuario. Para entrar a estas cookies vamos a usar _context.req.headers.cookie_ y estas las vamos a pasar a nuestra funcion _getAuthCookie()_ que estara en 'utils'. Esta funcion nos va a retornar:

- Si estuviera el usuario autenticado, un objeto con todos las propiedades que estemos enviando desde el redirect de Shopify la primera vez.
- Si no estuviese autenticado, o sea que no hay user logged, nos va a retornar _null_.

Entonces para saber o no si esta autenticado usaremos estos campos para hidratar nuestra aplicacion, y en caso que no, limitaremos desde el mismo servidor los datos que enviamos al front.

#### Como desloggear un usuario

En caso que desarrollen el flujo para desloggear al usuario entonces podran usar la funcion _userLogout_ que se tiene que importar desde UserContext en el front.
Esta funcion le pega a un endpoint de nuestro servidor en next.js que elimina las cookies y tambien hace el logout en Shopify legacy.

#### Como loggear a un usuario en headless

Para loguear a un usuario vamos a tener dos funciones que se encarguen de eso:

- _userLogin_ tambien importada desde UserContext, con email y password nos va a permitir loguearnos a travez del storefront API y luego generar el login tambien en Shopify Legacy a traves de Multipass. Los parametros de esta funcion (email, password).
- _sendLoginCode_ que nos va a permitir loguear al usuario mediante un codigo enviado a su mail/telefono. Esta es la misma funcionalidad ya desarrollada en Shopify. Si el codigo estuviera correcto va a loguear a nuestro usuario a travez de multipass y terminara yendo de nuevo a nuestra pagina. Es importante que el parametro 'return*to' de esta funcion lo envieemos a la pagina que corresponde en nuestro Shopify Legacy, de esta forma haremos que la misma pagina de detalle de producto en Shopify legacy haga el redirect a traves de la funcion \_goToHeadless* de forma que ya pase el proceso de autenticacion hacia nuestra app headless. Si no pasamos el parametro 'return_to' a esta funcion al iniciar entonces redirije por defecto a la misma pagina, correspondiente a inventa.shop; Ej: Si hacemos login con codigo en ssr.com/products/tetera -> inventa.shop/products/tetera. Esta funcion de login por codigo va a precisar algunas funciones adicionales para poder hacer el handle de cuando el codigo este erroneo, para ver un ejemplo pueden observar como esta hecho el handle en Fornecedores Portal. Para iniciar la funcion debemos pasar los parametros (email, callback, [return_to]).
