import Document, { Html, Head, Main, NextScript } from "next/document";
//const SHOPIFY_DOMAIN = "inventa1.myshopify.com";

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="pt">
        <Head>
          <link
            href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp&display=block"
            rel="stylesheet"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
